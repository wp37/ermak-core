<?php
	
	function draw_location_display($term_id, $step=0)
	{
		//add_action("ermak_body_script", "ermak_body_script");
		ermak_body_script($term_id);
		global $post, $Soling_Metagame_Constructor;
		//if($post->ID 	!= $Soling_Metagame_Constructor->options['location_display']) return "";
		
		if(!current_user_can("manage_options"))	
		{
			echo "<div class=smc_comment>" . __("Only Master can use this page", "smc") . "</div>";
			return ;
		}
		$location 		= SMC_Location::get_instance($term_id);
		$term_meta 		= SMC_Location::get_term_meta($term_id);
		$location_type	= get_post($term_meta['location_type']);
		
		$css			= "
		#tbl0, #tbl1, #tbl11
		{
			background:#666;
		}
		.table_podval
		{
			width:100%;
			margin:0;
		}
		.table_podval td
		{
			border:0;
			padding:20px;
		}";
		$tbl0			= get_tbl0($step, $location, $term_meta, $location_type);		
		$tbl1			= get_tbl1($step, $location, $term_meta, $location_type);		
		$tbl11			= get_tbl11($step, $location, $term_meta, $location_type);		
		$tbl20			= get_tbl20($step, $location, $term_meta, $location_type);
		require_once( SMC_REAL_PATH . "tpl/display.php" );
		
		return $table;
	}
	
	function get_tbl0($step, $location, $term_meta, $location_type)
	{
		return "
		<div class='cell_0'>
			<h1><span>". $location_type->picto . $location_type->post_title."</span> ".$location->name."</h1>".
			apply_filters("location_display_tbl0", "", $location, $term_meta, $location_type).
		"</div>";
	}
	function get_tbl1($step, $location, $term_meta, $location_type)
	{
		return "
			<div class='cell_0'>".
			apply_filters("location_display_tbl1", "", $location, $term_meta, $location_type);
		"</div>";
	}
	function get_tbl11($step, $location, $term_meta, $location_type)
	{
		return "
			<div class='cell_0'>".
			apply_filters("location_display_tbl111", "", $location, $term_meta, $location_type);
		"</div>";
	}
	function get_tbl20($step, $location, $term_meta, $location_type)
	{
		return "
			<div class='cell_0'>".
			apply_filters("location_display_tbl20", "", $location, $term_meta, $location_type);
		"</div>";
	}
	
	
	function ermak_body_script($term_id)
	{
		global	$Soling_Metagame_Constructor, $post;
		?>
			<script>
				var intv = setInterval(function()
				{
					jQuery("#wait_screen").fadeIn('slow');					
					send(['refresh_location_display', <?php echo $_GET['lid'] ?>], false);
				}, <?php echo $Soling_Metagame_Constructor->options['display_update_period']; ?>)
			</script>
		<?php
	}
	function location_display()
	{		
		$term_id		= $_GET['lid'];
		echo draw_location_display($term_id);
	}
?>