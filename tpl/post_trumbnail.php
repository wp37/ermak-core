<?php

function cyfu_publish_post($post, $imageUrl, $rotation=0)
{
	global $httml;
	$post_id		= is_numeric($post) ? $post : $post->ID;
    // First check whether Post Thumbnail is already set for this post.
    if (get_post_meta($post_id, '_thumbnail_id', true) || get_post_meta($post_id, 'skip_post_thumb', true)) 
	{
        //return;
    }
	
    // Generate thumbnail
    $thumb_id 		= cyfu_generate_post_thumb($imageUrl, $post, $rotation);
	$httml .= "<p>imageUrl = $imageUrl</p>";
	$httml .= "<p>thumb_id = $thumb_id</p>";
    // If we succeed in generating thumg, let's update post meta
    if ($thumb_id) 
	{
        update_post_meta( $post_id, '_thumbnail_id', $thumb_id );
    }
}


function cyfu_generate_post_thumb ($imageUrl, $post, $rotation=0) 
{
	//echo '<div>0</div>';
	
	// Get image from url & check if is an available image
	$post_id		= is_numeric($post) ? $post : $post->ID;
	
	// Get the file name
	$filename 		= substr($imageUrl, (strrpos($imageUrl, '/'))+1);

	//echo '<div>1 '.$imageUrl.'</div>';
    if (!(($uploads = wp_upload_dir(current_time('mysql')) ) && false === $uploads['error'])) {
        return null;
    }
	//echo '<div>2'.$filename.'</div>';
    // Generate unique file name
    $filename 		= wp_unique_filename( $uploads['path'], $filename );

    // Move the file to the uploads dir
    $new_file 		= $uploads['path'] . "/$filename";
	
	
	//echo '<div>3</div>';
    if (!ini_get('allow_url_fopen')) {
        $file_data 	= curl_get_file_contents($imageUrl);
    } else {
        $file_data 	= @file_get_contents($imageUrl);
    }
	
	//echo '<div>4'. $file_data.'</div>';
    if (!$file_data) {
        return null;
    }
	
	/*
	$file_data		= imagecreatefromjpeg($imageUrl);*/
	//$file_data		= imagerotate($file_data, 90);
	
    file_put_contents($new_file, $file_data);
	//move_uploaded_file($file_data, $new_file);
	
	if($rotation!=0)
	{
		$image 			= wp_get_image_editor(  $new_file  );
		if ( ! is_wp_error( $image ) ) 
		{
			// �������� �������� �� 90 ��������
			$image->rotate( $rotation );
			$image->save( $new_file );
		}
		else
		{
			echo "ERROR";
		}
	}
	
    // Set correct file permissions
    $stat 			= stat( dirname( $new_file ));
    $perms 			= $stat['mode'] & 0000666;
    @ chmod( $new_file, $perms );

	//echo '<div>5</div>';
    // Get the file type. Must to use it as a post thumbnail.
    $wp_filetype 	= wp_check_filetype( $filename, $mimes );

    extract( $wp_filetype );

    // No file type! No point to proceed further
    if ( ( !$type || !$ext ) && !current_user_can( 'unfiltered_upload' ) ) {
        //return null;
    }

	//echo '<div>6</div>';
    // Compute the URL
    $url 			= $uploads['url'] . "/$filename";
	
	
    // Construct the attachment array
    $attachment 	= array(
        'post_mime_type' 	=> 'image/jpeg',
        'guid' 				=> $url,
        'post_parent' 		=> null,
        'post_title' 		=> $post->post_title,
        'post_content' 		=> '',
    );

	//echo ' ' . $post_id . " " . $post->post_title . '</div>';

    $thumb_id = wp_insert_attachment($attachment, $file, $post_id);
    if ( !is_wp_error($thumb_id) ) {
        require_once(ABSPATH . '/wp-admin/includes/image.php');

        // Added fix by misthero as suggested
        wp_update_attachment_metadata( $thumb_id, wp_generate_attachment_metadata( $thumb_id, $new_file ) );
        update_attached_file( $thumb_id, $new_file );

        return $thumb_id;
    }
	
    return null;

}

/**
 * Function to fetch the contents of URL using curl in absense of allow_url_fopen.
 *
 * Copied from user comment on php.net (http://in.php.net/manual/en/function.file-get-contents.php#82255)
 */
function curl_get_file_contents($URL) {
    $c = curl_init();
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_URL, $URL);
    $contents = curl_exec($c);
    curl_close($c);

    if ($contents) {
        return $contents;
    }

    return FALSE;
}

	
?>