<script>
		jQuery(document).ready(function($)
		{
			var gr	= $(".uam_group_selection");
			if(gr.size())
			{
				gr.before( "<div id='uam-button' class='button' style='margin-top:10px;'><?php _e("Show") ?></div>" );
				gr.hide();
				$("#uam-button").click(function(evt)
				{	
					gr.toggle();
					if(gr.is(':visible'))
						$(this).text('<?php _e("Hide");?>');
					else
						$(this).text('<?php _e("Show");?>');					
				});
			}
		});
</script>
<?php
	
?>