<?php

		$posts		= array(
							array( 
									'img'=>'wp-content/uploads/2014/03/country',
									'post_title'=>'Домино "Сокращение рациональных дробей"',
									'post_content'=>"Правила просты и известны всем: Игрокам раздается по 4 карточки, остальные составляют банк (рубашкой вверх). Каждая карточка разделена на две зоны: на одной написана сократимая рациональная дробь, на другой выражение в стандартном виде.",
									'comment_count'=>36,
									'audio'=>'wp-content/plugins/Ermak/img/lia.mp3',
									'title' => 'Великая пестня'
								  ),
							array( 
									'img'=>'wp-content/uploads/2014/03/country',
									'post_title'=>"<a href='#'>Загадка</a> № 8 «БМД»",
									'post_content'=>"Захват произвели: <a href='#'>Садов Владислав</a>, Сирожева Кристина, Романова Анастасия, Шангареев Артем",
									'comment_count'=>12,
									'slides'=>array(
														'wp-content/plugins/Ermak/img/travel/01.jpg',
														'wp-content/plugins/Ermak/img/travel/02.jpg',
														'wp-content/plugins/Ermak/img/travel/03.jpg',
														'wp-content/plugins/Ermak/img/travel/04.jpg',
														'wp-content/plugins/Ermak/img/photography/005.jpg',
														'wp-content/plugins/Ermak/img/photography/006.jpg',
														'wp-content/plugins/Ermak/img/photography/007.jpg',
													)
								  ),
							array( 
									'img'=>'wp-content/uploads/2014/03/slides334',
									'post_title'=>"Трусы!!",
									'post_content'=>"Сильнейшие всех планет не проявили желания участвовать в турнире, <a href='#'>ИСПУГАЛИСЬ поражения</a> и позора, я маг, Асташкин Сергей, объявляю их всех, кто не откликнулся, проигравшими.. Я — Самый великий воин во вселенной! Маг — Мироборец.",
									'comment_count'=>3,
									'video'=> 'IuGyBXKBWFY'
								),
							array( 
									'img'=>'wp-content/uploads/2014/03/pl1',
									'post_title'=>"Официальное заявление Нибиру",
									'post_content'=>"Совет Анунаков с планеты Нибиру официально заявляет, что не станет занимать ничьей стороны в разгорающейся войне.
Анунаки намерены сохранять нейтралитет и занять позицию наблюдателей, не вмешиваться в дела остальных Планет.
Однако, в целях самозащиты Совет принял решение о расконсервации Боевого Космического Крейсера и установке вокруг планеты силового щита.
Мы не собираемся ни на кого нападать, но, согласно второму принципу анунаков («Выживает только та цивилизация которая умеет себя защищать»), мы готовы защитить себя и свои территории.

Также анунаки готовы оказывать гуманитарную помощь пострадавшим планетам.",
									'comment_count'=>6,
									'slides'=>array(
														'wp-content/plugins/Ermak/img/travel/05.jpg',
														'wp-content/plugins/Ermak/img/travel/06.jpg',
														'wp-content/plugins/Ermak/img/travel/07.jpg',
														'wp-content/plugins/Ermak/img/travel/08.jpg',
													)
								  ),
							array( 
									'img'=>'wp-content/uploads/2014/03/pl3',
									'post_title'=>"ВАЖНО: ОБРАЩЕНИЕ МАСТЕРОВ!!!",
									'post_content'=>"ДЛЯ НАС ОЧЕНЬ ВАЖНО получить обратную от вас ОБРАТНУЮ СВЯЗЬ! НУ прям ОЧЕНЬ!!! Поэтому просьба:

К ДЕРЖАТЕЛЯМ: кратко написать мне на kisulya87@gmail.com, что понравилось и что не понравилось в игре (плюсы и минусы) — можно совсем кратко, ну ПОЖАЛУЙСТА!;)

К ИГРОКАМ: мы ОЧЕНЬ-ОЧЕНЬ-ОЧЕНЬ хотим узнать, как для вас прошла игра: кем вы были, что делали и к чем пришли. Пожалуйста, выкладывайте ваши отчеты сюда в комментариях или в любое другое место со ссылкой здесь!!! И ЕЩЕ: очень важно услышать ваше мнение об игре — интересно-неинтересно, почему и т.п.! Просьба писать ваши отзывы здесь или мне в контакте! И передайте товарищам, не посещающим сайт об этой просьбе.",
									'comment_count'=>0,
									'slides'=>array(
														'wp-content/plugins/Ermak/img/travel/09.jpg',
														'wp-content/plugins/Ermak/img/travel/10.jpg',
														'wp-content/plugins/Ermak/img/travel/11.jpg',
														'wp-content/plugins/Ermak/img/travel/12.jpg',
													)
								  ),
							array( 
									'img'=>'wp-content/uploads/2014/03/slides70',
									'post_title'=>"Ответ Анунаков",
									'post_content'=>"Ответ Владиславу Грачеву.

Ты пожелал апокалипсиса. Он уже начинается. Теперь ты — Ной нового времени. Можешь начинать спасать Мир и строить ковчег.

С любовью, твои Анунаки:)

All as you wish!",
									'comment_count'=>294,
									'slides'=>array(
														'wp-content/plugins/Ermak/img/travel/15.jpg',
														'wp-content/plugins/Ermak/img/travel/16.jpg',
														'wp-content/plugins/Ermak/img/travel/17.jpg',
														'wp-content/plugins/Ermak/img/travel/18.jpg',
														'wp-content/plugins/Ermak/img/travel/19.jpg',
														'wp-content/plugins/Ermak/img/photography/015.jpg',
													)
								  ),
							array( 
									'img'=>'wp-content/uploads/2014/03/slides69',
									'post_title'=>"Конец мирного времени: Глава 3",
									'post_content'=>"Я маг, Асташкин Сергей с планеты Метида нападаю на повинцию 3 планеты Барнарда.",
									'comment_count'=>0,
									'slides'=>array(
														'wp-content/plugins/Ermak/img/travel/11.jpg',
														'wp-content/plugins/Ermak/img/travel/12.jpg',
														'wp-content/plugins/Ermak/img/travel/13.jpg',
														'wp-content/plugins/Ermak/img/travel/14.jpg',
													)
								  )							
						);
	
	global $smc_height;
	
	
	
	$html	.='<style>
			.ss 
			{	
				width:100%;
				height:100%; 
				background-color:#FFF; 
				background-size:cover; 
				font-size:72px;
				color:#FFF; 
				text-align:center; 
			}
			.sub-slider
			{
				position: relative;
				top: 20px; 
				left: 60px; 
				width: 420px; 
				height: '.($smc_height - 70).'px; 
				overflow: hidden; 
				border:2px solid #FFF;
				-webkit-transition: all 700ms ease-out;
				-moz-transition: all 700ms ease-out;
				-ms-transition: all 700ms ease-out;
				-o-transition: all 700ms ease-out;
				transition: all 700ms ease-out;
				-webkit-box-shadow: 6px 6px 14px 0 rgba(0,0,0,0.5);
				box-shadow: 6px 6px 14px 0 rgba(0,0,0,0.5);
			}
			.jssorn01 div, .jssorn01 div:hover, .jssorn01 .av
			{
				filter: alpha(opacity=70);
				opacity: .7;
				overflow:hidden;
				cursor: pointer;
				border: #DDD 1px solid;
			}
			.jssorn01 div { background-color: #000; }
			.jssorn01 div:hover, .jssorn01 .av:hover { background-color: #d3d3d3; }
			.jssorn01 .av { background-color: '.$this->get_iface_color().'; }
			.jssorn01 .dn, .jssorn01 .dn:hover { background-color: #555555; }

			/* jssor slider direction navigator skin 02 css */
			/*
			.jssord02l              (normal)
			.jssord02r              (normal)
			.jssord02l:hover        (normal mouseover)
			.jssord02r:hover        (normal mouseover)
			.jssord02ldn            (mousedown)
			.jssord02rdn            (mousedown)
			*/
			.jssord02l, .jssord02r, .jssord02ldn, .jssord02rdn
			{
				position: absolute;
				cursor: pointer;
				display: block;
				background: url(wp-content/plugins/Ermak/img/d03.png) no-repeat;
				overflow:hidden;
			}
			.jssord02l { background-position: -3px -33px; }
			.jssord02r { background-position: -63px -33px; }
			.jssord02l:hover { background-position: -123px -33px; }
			.jssord02r:hover { background-position: -183px -33px; }
			.jssord02ldn { background-position: -243px -33px; }
			.jssord02rdn { background-position: -303px -33px; }
			.lp-play-button
			{
				position:absolute;
				bottom:40%;
				left:40%;
				padding:12px 12px 4px 12px;
				background-color:'.$this->get_iface_color().'; 
				font-size:80%;
				line-height:110%;
				align:center;
				color:white;
			}
		</style>
    
    <!-- Jssor Slider Begin -->
    <!-- You can move inline styles (except ) to css file or css block. -->
    <div class="lp-slider-cont" id="slider_container3" style="position: relative; top: 30px; left: 0; width: '.$params[5].'px;   height: '. ($smc_height - 30) .'px; background:transparent;">
		
        <!-- Loading Screen >
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block; background-color: transparent; top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(wp-content/plugins/Ermak/img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
        </div-->

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: '. ($params[5] - 100).'px; height: '. ($smc_height - 30) .'px; overflow: hidden;">';
	$i = 0;
	
	/*
	if(count($posts) == 0)
	{
		$posts	= array( 
							'img'=>'wp-content/plugins/Ermak/img/lp_location_default.jpg',
							'post_title'=>"Трусы!!",
							'post_content'=>"Сильнейшие всех планет не проявили желания участвовать в турнире, <a href='#'>ИСПУГАЛИСЬ поражения</a> и позора, я маг, Асташкин Сергей, объявляю их всех, кто не откликнулся, проигравшими.. Я — Самый великий воин во вселенной! Маг — Мироборец.",
							'comment_count'=>3,
							'video'=> 'IuGyBXKBWFY'
						);
	}
	*/
	foreach($posts as $post)
	{
        //$html	.=    '<div class="jssor-bck" style="background-image:url('.$post['img'].'.jpg);">
        $html	.=    '<div class="jssor-bck" style="background-color:' . $this->options['fills'][$i % count($this->options['fills'])][0] . '; ">
							<div class="lp-comment-button-cont">'.
								//$this->options['fills'][count($this->options['fills']) % $i][1].
								'<a class="lp-comment-button hint  hint--right" data-hint="'.$post['comment_count']. __(" comments to this post", "smc").'">'.
									$post['comment_count'].
								'</a>
								<div class="lp-comments-count">'.
									
								'</div>
							</div>
							<div class="jssor-featured-text">
								<div class="jssor-featured-title">'.
									$post['post_title'].'
								</div>
								<div class="jssor-featured-content">'.
									$post['post_content'].'
								</div>
							</div>
							
							<div id="slider'.$i.'_container" class="sub-slider" style="">';
		if(isset($post['slides'])){
			$html	.=    	'
								<!-- Slides Container -->
								<div f="slide" u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 420px; height: '.($smc_height - 70).'px; overflow: hidden;">
								<div class="ss" style="background-image:url(0.png); background-color:transparent;"></div>';
			foreach($post['slides'] as $slide)
			{
				$html	.=    	'	<div class="ss" style="background-image:url('.$slide.');"></div>';
			}
			$html		.=    	'	<div class="ss" style="background-image:url(0.png); background-color:transparent;"></div>
								</div>
								<!-- Navigator Skin Begin -->
								
								<!-- jssor slider navigator skin 01 -->
								<!-- navigator container -->
								<div u="navigator" class="jssorn01" style="position: absolute; bottom: 16px; right: 10px;">
									<!-- navigator item prototype -->
									<div u="prototype" style="POSITION: absolute; WIDTH: 12px; HEIGHT: 12px;"></div>
								</div>
								<!-- Navigator Skin End -->
								
								<!-- Direction Navigator Skin Begin -->
								
								<!-- Arrow Left -->
								<span u="arrowleft" class="jssord02l" style="width: 55px; height: 55px; top: '.($smc_height/2- 55).'px; left: 8px;">
								</span>
								<!-- Arrow Right -->
								<span u="arrowright" class="jssord02r" style="width: 55px; height: 55px; top: '.($smc_height/2- 55).'px; right: 8px">
								</span>
								<!-- Direction Navigator Skin End -->';
		}
		else if(isset($post['video']))
		{
			$html		.=		'
								<!-- Slides Container -->
								<div id="slide-cont-'.$i.'" f="video" u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width:420px; height:'.($smc_height - 80).'px; overflow: hidden;">
								';
			//$html		.=    	'	<div class="ss" style="background-image:url(0.jpg); background-color:transparent;"></div>';
			$html		.=    	'	<div  class="ss" style="background-image:url(wp-content/plugins/Ermak/img/photography/015.jpg);width:100%; height:100%;">
										
										<div id="video-player-'.$i.'"  style="dispaly:none;">
											<video width="640" height="420" id="player'.$i.'" preload="none">
												<source type="video/youtube" src="http://www.youtube.com/embed/'.$post['video'].'?rel=0&wmode=transparent" />
											</video>
										</div>
										<div id="play-button-'.$i.'" class="lp-play-button">
											<a href="javascript:void(0);" onclick="video_click(' .$i. ', ' . "'" .$post['video']. "'" .');">
												<i class="fa fa-play-circle-o"></i> 
											</a>
										</div>
									</div>';
			$html		.=    	'	<div class="ss" style="background-image:url(0.jpg); background-color:transparent;"></div>';
			$html		.=    	'</div>';
		}
		else if(isset($post['audio']))
		{
			$html		.='<div id="lp-audio-player" src="'.$post['audio'].'" title="'.$post['title'].'" style="position:absolute; bottom:0px; left:10px;"></div>';
		}
		$html	.=    			'
							</div>
							
							<div u="thumb">
								<img class="i" src="'.$post['img'].'-75x75.jpg" />								
							</div>
						</div>';
						$i++;
    }       
       $html	.=     '</div>
        
        <!-- ThumbnailNavigator Skin Begin -->
        <div u="thumbnavigator" class="jssort11" style="position: absolute; width: 200px; height: '. ( $smc_height - 30 ) .'px; left:'. ( $params[5] - 10 ).'px; top:10px;">
            <!-- Thumbnail Item Skin Begin -->
            <div u="slides" style="cursor: move;">
                <div u="prototype" class="p" style="position: absolute; width: 380px; height: 69px; top: 0; left: 0;">
                    <thumbnailtemplate style=" width: 100%; height: 100%; border: none;position:absolute; top: 0; left: 0;"></thumbnailtemplate>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
        <!-- ThumbnailNavigator Skin End -->
    </div>
    <!-- Jssor Slider End -->';
	
	?>