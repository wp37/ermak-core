<?php
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	// Пошла обработка запроса
	global $Intruction_Menager;
	
	if (isset($_POST['save'])) 
	{			
		$this->options['admin_optimyze']			= isset($_POST['demo_box_1']) 	? 1 : 0;
		$this->options['hide_panel']				= isset($_POST['demo_box_2']) 	? 1 : 0;
		$this->options['use_exec']					= isset($_POST['demo_box_3']) 	? 1 : 0;
		$this->options['use_ermak_ico']				= $_POST['use_ermak_ico'] == "on";
		$this->options['fx_index']					= stripslashes($_POST['radiog_lite']);
		update_option(SMC_ID, $this->options);	
		
		if($this->options['admin_optimyze'])
		{
			$this->assistants->remove_menus();
		}
		echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><b>'.__('Settings saved.').'</b></p></div>';
	}
	if(isset($_POST['save_settings']))
	{
		$this->options['default_featured_title']	= stripslashes($_POST['featured_title']);
		$this->options['default_featured_text']		= stripslashes($_POST['featured_text']);
		update_option(SMC_ID, $this->options);
	}
	if(isset($_POST['reset']))
	{
		if($_POST['is_reset']==1)
		{
			$this->clear_all();
			echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><b>'.__('Deleting all Locations, Location Types, Direct Messages and Masters.','smc').'</b></p></div>';
		}
	}
	if($_POST['create_instructions'])
	{		
		$Intruction_Menager->init_Instructions();		
	}
	if($_POST['create_instruction_menu'])
	{		
		$Intruction_Menager->add_instruction_menu_items();		
	}
	if($_POST['delete_instruction_menu'])
	{
		$Intruction_Menager->remove_instruction_menu_iems();	
		echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><b>'.__('Instructions Menu deleted successfull.','smc').'</b></p></div>';
	}
	if($_POST['save_special_pages'])
	{
		$this->options['location_page_id']			= $_POST['location_type'];
		$this->options['masters_page_id']			= $_POST['masters'];
		$this->options['prsonal_page_id']			= $_POST['dm'];
		$this->options['about_the_project_page_id']	= $_POST['about_the_project_page_id'];
		$this->options['my_log_page_id']			= $_POST['my_log_page_id'];
		$this->options['rules_page_id']				= $_POST['rules_page_id'];
		$this->options['location_display']			= $_POST['location_display'];
		$this->options['personal_smartphone']		= $_POST['personal_smartphone'];
		do_action("smc_admin_sinc_special_pages");
		update_option(SMC_ID, $this->options);
	}
	if($_POST['save_dys'])
	{
		update_option('dys_project_name', 			$_POST['dys_project_name']);
	}
	
	if($_POST['ssv'])
	{
		/*
		ob_start();
		print_r( SMC_Location::create_taxonomy_custom_meta(4,  array( "location_type_id"=>8, "hiding_type"=>2 ) ) );
		$ssv	= ob_get_contents();
		ob_end_clean(); 
		*/
		SMC_Location::add_properties( array( "count_cells", "currency_type" ), "BIGINT(20) UNSIGNED ", 0 );
	}
	
	$posts		= get_posts(array('numberposts' => 500,'post_type'=>"page", 'orderby'=>"ID", 'order'=>'ASC'));
	$project	= $this->options['project_name'];
	$dys_project_name	= get_option('dys_project_name');
	// Внешний вид формы
	?>
	</pre>		
		<style>
			.h
			{
				border-bottom:1px dotted #AAA;
				padding-bottom:15px;
				margin-bottom:15px;
			}
		</style>		
		<form method="post">
		<div style="margin-bottom:10px;">
			<div class="smc_title_main">
				<h2><?php _e("Metagame settings","smc");?></h2>	
			</div>		
			<div class="smc_title_main" style="padding-top:50px;">
				<span style="margin:25px 0;">
					<?php _e("To our friend who is Ermak Timofeevitch Sergeev's father.", "smc") ?>
				</span><br>
				<i> 
					<?php _e("Ermak team", "smc") ?>
				</i>
			</div>
		</div>
	<?php
	$html_0	= "<div><img src='" . SMC_URLPATH . "img/ermak1.jpg'></div>";//"<div><textarea style='width:80%; height:300px;'>" .$ssv . '</textarea></div><input type="submit" name="ssv" value="Save"/>';
	$html	= '					
				<div class="smc_form" style="display:'. ( $project != "" ? "none" : "block" ).'">		
					<h3>' . __("Project registration","smc"). '</h3>
					<div class="smc-description">'.
						__("Project Metaversitet created for accounting and the development of individual educational trajectories of the participants in the course of business and gaming training.", "smc"). "<br>".
						__("Register your game on Metaversitete allow your players to collect educational outcomes and automatic assessment to analyze and increase their competence.", "smc").
					'</div>
					
					<div id="delete_project" style="float:left;margin-right:2px;;margin-bottom:2px;">
						<a class="button-primary smc-pad-button" id="update_project">
							<span class="fa-stack smc-green">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa fa-bookmark fa-stack-1x fa-inverse"></i>
							</span>' . __('about Metaversitet', "smc") .
						'</a>
					</div>
					<div id="registrate">
						<a class="button-primary smc-pad-button" id="update_project">
							<span class="fa-stack smc-red ">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa fa-user fa-stack-1x fa-inverse"></i>
							</span>' . __('Registrate metaversity.ru project', "smc").
						'</a>
					</div>
					<div>							
						<!-- DYS -->'.			
						settings_fields("dys-inter-login-settings-group") .
						do_settings_sections('dys-inter-login-settings-group') .
						'<BR>
						<label for="dys_project_name">' . __("Title of Metaversity Project", "smc") . '<label><BR>
						<input type="text" name="dys_project_name" value="' . esc_attr($dys_project_name) .'" /><BR>'.
						($dys_project_name ? '<div>Вставьте <a href="http://' . esc_attr($dys_project_name) . '.metaversity.ru/admin/integration/endpoint/">здесь</a> следующий адрес <input type="text" style="width: 600px;" readonly="readonly" value="' . SMC_URLPATH . 'auth/index.php"/></div>' : "" ) .
						'<div class="submit">
							<input type="submit" class="button-primary" name="save_dys" id="save_dys" value="' . __("Save") . '"/>	
						</div>							
						<!-- DYS end -->					
					</div>
				</div>
				<div class="smc_form" style="display: ' . ( $project=="" ? "none" : 'block' ) . '">		
					<h3> Project <span style="font-weight:bold; color:red;">' . $project . '</span></h3>
					<div class="smc-description" style="margin-bottom:5px;">'.
						sprintf(__("This metagame project register on Metaversity portal. Metaversiry's users can register by click in %s this page %s", "smc"), "<a href='http://" . $project . ".metaversity.ru/' target='_blank'>", "</a>" ).
					'</div>
					<div style="float:left;margin-right:2px;">
						<a href="#" class="button" id="update_project">' . __('update', "smc") . '</a>
					</div>
					<div style="float:left;margin-right:2px;">
						<a class="button" id="udeleted_project">' . __('delete project', "smc") . '</a>
					</div>
					<div>
						<a href="/wp-admin/admin.php?page=metaversitet_sub_menu" class="button" id="about_project">' . __('about Metaversitet', "smc") . '</a>
					</div>
				</div>
				<div class="smc_form" >
					<h3>' . __("Unique hash of this installation", "smc") . '</h3>
					<div class="smc_selected_form">'.
						get_option(SMC_HASH).					
				'	</div>
				</div>
				';
		$html_1		= '
					<div class="smc_form">							
						<h3>' . __("System ettings","smc") . '</h3>	
						<input name="demo_box_1" id="demo_box_1" class="css-checkbox" type="checkbox"  '. checked($this->options['admin_optimyze'], true, false) . ' />
						<label for="demo_box_1" name="demo_lbl_1" class="css-label">' . __("Optimyze Admin Menu","smc") . '</label>
						<BR>
						<input name="demo_box_2" id="demo_box_2" class="css-checkbox" type="checkbox" ' . checked($this->options['hide_panel'], true, false). ' />
						<label for="demo_box_2" name="demo_lbl_2" class="css-label">' . __("Remove Admin Menu for contributor", "smc") . '</label>
						<!--BR>
						<input name="demo_box_3" id="demo_box_3" class="css-checkbox" type="checkbox" '. checked($this->options['use_exec'], true, false) . '/>
						<label for="demo_box_3" name="demo_lbl_3" class="css-label">'. 
							__("Use ", "smc"). ' [exec][/exec]' . __(" shotcode for Administrator can post php-scripts", "smc") . 
						'</label-->
						<BR>
						<input name="use_ermak_ico" id="use_ermak_ico" class="css-checkbox" type="checkbox" ' . checked($this->options['use_ermak_ico'], true, false) .'/>
						<label for="use_ermak_ico" name="use_ermak_ico" class="css-label">' . __("Use Ermak favicon", "smc") . '</label>
						<!--p></p>
						<h3>' . __("FX for index page","smc") .'</h3>
						<p class="smc-description">'.
							__("Use our staffes for index page if it allowed your theme. More - instruction how used this fitches in shortcode. ", "smc").
						'</p>
						<table>
							<tr>
								<td>
									<input type="radio" name="radiog_lite" id="radio1" class="css-checkbox"  value=0 ' . checked($this->options['fx_index'], false, false) . '/> 
									<label for="radio1" class="css-label">'.__("No FX","smc").'</label>
								</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="radiog_lite" id="radio2" class="css-checkbox" value=1 ' . checked($this->options['fx_index'], true, false) . '/> 
									<label for="radio2" class="css-label">' . __("Main Metagame Panel","smc") . '</label>
								</td>
							</tr>
						</table-->
						<p></p>
						<div class="submit">
							<input name="save" type="submit" class="button-primary" value="' . __('Save Draft') . '" />
						</div>
					</div>
					
					
					<div class="smc_form">
						<h3>' . __("Reset all settings.", "smc") . '</h3>
						<div class="smc-description">'.
							__("Your click deleted all Location, Location Types and other your settings of Metagame.", "smc") .
						'</div>
						<div>
							<input type="radio" name="is_reset" id="is_reset1" class="css-checkbox" value="1"/> 
							<label for="is_reset1" class="css-label">' . __("Yes") . '</label>
						</div>
						<div>
							<input type="radio" name="is_reset" id="is_reset2" class="css-checkbox" value="0" checked="checked"/> 
							<label for="is_reset2" class="css-label">' . __("No") . '</label>
						</div>
						<div class="submit ">
							<input name="reset" type="submit" class="button-primary smc-pad-button" value="' . __("Reset", "smc") . '" />
						</div>
					</div>
					';
					
		$html_1		.= '
					<div class="smc_form">	
						<div class="h" style="position:relative; display:inline-block; float:left; width:240px;">
							<h4>' . __("Instructions Menu", "smc") .'</h4>
							<input name="create_instruction_menu" type="submit" class="button" value="'. __("Create Instrution Menu", "smc").'" style="display:'. ( $this->options['instruction_menu_enabled'] ? "none"	: "block") . ';"/>
							<input name="delete_instruction_menu" type="submit" class="button" value="'. __("Delete Instrution Menu", "smc").'" style="display:' .( $this->options['instruction_menu_enabled'] ? "block": "none"  ). ';"/>
						</div>
						<div class="h"  style="position:relative; display:inline-block; width:240px; margin-bottom:19px;">
							<h4>' . __("Create Instructions", "smc") . '</h4>
							<input name="create_instructions" type="submit" class="button" value="'. __("Create Instructions", "smc"). '"/>
						</div>
						<div>
							<label for="featured_title">'. 
								__("Default Featured Title", "smc") . 
							'</label><br>
							<input name="featured_title" value="' . $this->options['default_featured_title'] . '" style="width:100%;"/>						
						</div>
						<div>
							<label for="featured_text">' . 
								__("Default Featured Text", "smc") . 
							'</label><br>
							<textarea name="featured_text" style="width:100%; height:100px;">' .
								$this->options['default_featured_text'] .
							'</textarea>					
						</div>
						<div class="submit">
							<input name="save_settings" type="submit" class="button-primary" value="' . __('Save Draft') . '" />
						</div>
					</div>';
		$html_2		= '
					<div class="smc_form">
						<div class="absaz">'.
							__("To work correctly need ","smc") .
							'<ul>
								<li>'.
									( !is_plugin_active('user-access-manager/user-access-manager.php')  ?
									'<a href="http://wordpress.org/plugins/user-access-manager/" target="_blank">User Access Manager</a>' :
									'<b>User Access Manager</b>' ) .
									'<br><div class="smc-description">'. __("you can manage the access to your posts, pages and files", "smc") . '</div>
								</li>
								<li>'.
									( !is_plugin_active('add-local-avatar/avatars.php') ?
									'<a href="http://wordpress.org/plugins/add-local-avatar/" target="_blank">Add Local Avatar</a> ' :
									'<b>Add Local Avatar</b>' ).
									'<br><div class="smc-description">' . __("This plugin adds the ability to manage local avatars.", "smc") . '</div>							
								</li>
								<li>' .
									( !is_plugin_active('taxonomy-images/taxonomy-images.php') ?
									'<a href="http://wordpress.org/plugins/rus-to-lat-advanced/" target="_blank">rus to lat advanced 1.0</a>' : 
									'<b>rus to lat advanced 1.0</b>' ) .
									'<br><div class="smc-description">' . __("Производит транслитерацию загружаемых файлов и постоянных ссылок, создаваемых из заголовков страниц и записей, имеющих русские символы в названии.") .'</div>							
								</li>
								<li>'.
									( !is_plugin_active('taxonomy-images/taxonomy-images.php') ? 
									'<a href="http://wordpress.org/plugins/taxonomy-images/" target="_blank">Taxonomy Images</a> ' :
									'<b>Taxonomy Images</b>' ) .
									'<br><div class="smc-description">'. __("Associate images from your media library to categories, tags and custom taxonomies.", "smc") . '</div>							
								</li>	
							</ul>
						</div>					
					</div>
					';
		
		$html_3		= '
					<div class="smc_form">
							<div class="smc-description">'.
								__("Special functionally pages used by Metageme's elements. If administrator cleared one of these, functional of plugin may have errors. Create new page and send shortcode, whitten on label.", "smc").
							'</div>
							<div class="absaz">
								<ul>
									<li>
										<label for="about_the_project_page_id">' . __("About the project", "smc") . '</label><BR>
										<div class="styled-select state rounded w400">
											<select  name="about_the_project_page_id" id="about_the_project_page_id" class="chosen-select">
												<option  value="-1">---</option>';
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['about_the_project_page_id'], false);
				$html_3			.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
			
			$html_3			.= '			</select>
										</div>
									</li>
									<li>
										<label for="rules_page_id">' . __("Rules and rights", "smc") . '</label><BR>
										<div class="styled-select state rounded w400">
										<select  name="rules_page_id" id="rules_page_id" class="chosen-select">
											<option  value="-1">---</option>';
											
			foreach($posts as $post)
			{
				$selected		=  selected($post->ID, $this->options['rules_page_id'], false);
				$html_3			.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
											
											
			$html_3			.= '		</select>
										</div>
									</li>
									<li>
										<label for="location_type">' . __("My Locations", "smc") . ' shortcode: [smc_locations_page] </label><BR>
										<div class="styled-select state rounded w400">
										<select  name="location_type" id="location_type" class="chosen-select">
											<option  value="-1">---</option>';
																							
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['location_page_id'], false);
				$html_3			.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}										
			$html_3			.= '		</select>
										</div>
									</li>
									<li>
										<label for="dm">' . __("Direct Messages", "smc") . ' shortcode: [smc_personal_page] </label><BR>
										<div class="styled-select state rounded w400">
										<select  name="dm" id="dm" class="chosen-select">
											<option  value="-1">---</option>';
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['prsonal_page_id'], false);
				$html_3			.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
			$html_3			.= '		</select>
										</div>
									</li>
									<li>
										<label for="masters">' . __("Masters", "smc"). ' shortcode: [smc_masters_page] </label><BR>
										<div class="styled-select state rounded w400">
										<select  name="masters" id="masters" class="chosen-select">
											<option  value="-1">---</option>';
											
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['masters_page_id'], false);
				$html_3			.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
			$html_3			.= '		</select>
										</div>
									</li>
									<li>
										<label for="my_log_page_id">' . __("Log page", "smc") . 'shortcode: [my_log_page] </label><BR>
										<div class="styled-select state rounded w400">
										<select  name="my_log_page_id" id="my_log_page_id" class="chosen-select">
											<option  value="-1">---</option>';
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['my_log_page_id'], false);
				$html_3			.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
			$html_3			.= '		</select>
										</div>
									</li>
									<li>
										<label for="personal_smartphone">' . __("personal smartphone", "smc") . 'shortcode: [personal_smartphone] </label><BR>
										<div class="styled-select state rounded w400">
										<select  name="personal_smartphone" id="personal_smartphone" class="chosen-select">
											<option  value="-1">---</option>';
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['personal_smartphone'], false);
				$html_3			.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
			$html_3			.= '		</select>
										</div>
									</li>
									<li>
										<label for="location_display">' . __("Location display", "smc") . 'shortcode: [my_log_page] </label><BR>
										<div class="styled-select state rounded w400">
										<select  name="location_display" id="location_display" class="chosen-select">
											<option  value="-1">---</option>';
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['location_display'], false);
				$html_3			.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
			
			$html_3			.= '		</select>
										</div>
									</li>'.
									apply_filters("smc_admin_special_pages_choosee", "").
								'</ul>
							</div>
							<div class="submit">
								<input name="save_special_pages" type="submit" class="button-primary" value="' . __('Save Draft') . '" />
							</div>
						</div>
					';
					
		require_once(SMC_REAL_PATH.'class/SMC_Object_type.php');
		$SMC_Object_Type		= new SMC_Object_Type;
		$d						= '<h2>SMC_Object</h2>';
		ob_start();
		print_r ($SMC_Object_Type->get_object(61, 'location_type'));
		$d						.= '<div  class="smc_form"><textarea style="width:100%; min-height:600px;">'.ob_get_clean(). "</textarea></div>";
		
		$arr					= apply_filters("smc_setting_page", 
		array(
			array("title" => __("About"), 					"slide" => $html_0 ),			
			//array("title" => __("Registration", 'smc'), 	"slide" => $html   ),			
			array("title" => __("Actions"), 				"slide" => $html_1 ),		
			array("title" => __("Moduls","smc") , 			"slide" => $html_2 ),				
			array("title" => __("Special Pages","smc"), 	"slide" => $html_3 ),	
			//array("title" => "?", 	"slide" => $d ),	
			), 'main'														
		);
		echo Assistants::get_switcher($arr);
		echo wp_nonce_field( basename( __FILE__ ), 'mg_1', true, false );
		?>
		</form>
		 <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
		<pre>
		<script type="text/javascript">
			//set_chosen(".chosen-select");
		</script>
		<?php
		//echo "text:<BR>";
		//print_r(get_option(SMC_ID)->head);

?>