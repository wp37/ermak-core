<?php	
	
	//function get_slide_show($options, $color, $params)
	{
		global $smc_height;	
		global $post;
		global $Soling_Metagame_Constructor;
		$options	= $this->options;
		$color		= $this->get_iface_color();
		$excat		= $options['exclude_cats'];		
		$ex			= is_array($excat) ? $excat : explode(",", $excat);
		wp_reset_postdata();
		$args	= array(
							'numberposts'     		=> $options['courusel_slides'],
							'offset'         		=> 0,
							'orderby'         		=> 'post_date',
							'order'           		=> 'DESC',
							
							'include'         		=> '',
							'category__not_in'		=> $ex,
							'meta_query' 	  		=> array(
																'relation' => 'OR',
																array(
																	'key' => 'youtube_id',
																	'value' => '--',
																	'compare' => 'NOT LIKE'
																),
																array(
																	'key' => 'slides',
																	'value' => '--',
																	'compare' => 'NOT LIKE'
																)
															),
							/**/
							'post_type'       		=> array('page','post', ERMAK_NEWS_TYPE),
							'post_mime_type'  		=> '',
							'post_parent'     		=> '',
							
							'post_status'     		=> 'publish'
			
						);
		insertLog("slide_show", $args);	
		$slidess	= get_posts($args);
		insertLog("slide_show",($slidess) );
		//$slidess		= array();		
		if(!isset($slidess) && count($slidess) > 0 )
		{
			$html	.='<style>
			.ss 
			{	
				width:100%;
				height:100%; 
				background-color:#FFF; 
				background-size:cover; 
				font-size:72px;
				color:#FFF; 
				text-align:center; 
			}
			.sub-slider
			{
				position: relative;
				top: 20px; 
				left: 60px; 
				width: 420px; 
				height: '.($smc_height - 70).'px; 
				overflow: hidden; 
				border:2px solid #FFF;
				-webkit-transition: all 700ms ease-out;
				-moz-transition: all 700ms ease-out;
				-ms-transition: all 700ms ease-out;
				-o-transition: all 700ms ease-out;
				transition: all 700ms ease-out;
				-webkit-box-shadow: 6px 6px 14px 0 rgba(0,0,0,0.5);
				box-shadow: 6px 6px 14px 0 rgba(0,0,0,0.5);
			}
			.jssorn01 div, .jssorn01 div:hover, .jssorn01 .av
			{
				filter: alpha(opacity=70);
				opacity: .7;
				overflow:hidden;
				cursor: pointer;
				border: #DDD 1px solid;
			}
			.jssorn01 div { background-color: #000; }
			.jssorn01 div:hover, .jssorn01 .av:hover { background-color: #d3d3d3; }
			.jssorn01 .av { background-color: '.$this->get_iface_color().'; }
			.jssorn01 .dn, .jssorn01 .dn:hover { background-color: #555555; }

			/* jssor slider direction navigator skin 02 css */
			/*
			.jssord02l              (normal)
			.jssord02r              (normal)
			.jssord02l:hover        (normal mouseover)
			.jssord02r:hover        (normal mouseover)
			.jssord02ldn            (mousedown)
			.jssord02rdn            (mousedown)
			*/
			.jssord02l, .jssord02r, .jssord02ldn, .jssord02rdn
			{
				position: absolute;
				cursor: pointer;
				display: block;
				background: url(wp-content/plugins/Ermak/img/d03.png) no-repeat;
				
				overflow:hidden;
			}
			.jssord02l { background-position: -3px -33px; }
			.jssord02r { background-position: -63px -33px; }
			.jssord02l:hover { background-position: -123px -33px; }
			.jssord02r:hover { background-position: -183px -33px; }
			.jssord02ldn { background-position: -243px -33px; }
			.jssord02rdn { background-position: -303px -33px; }
			.lp-play-button
			{
				position:absolute;
				bottom:10px;
				left:40%;
				padding:8px 8px 0px 8px;
				background-color:'.$color.'; 
				font-size:80%;
				line-height:110%;
				align:center;
				color:white;
				-webkit-border-radius: 50px;
				-moz-border-radius: 50px;
				border-radius: 50px;
			}
			.lp-play-button a
			{
				color:white;
			}
			.jssor-more-than
			{
				padding: 5px 10px;
				color:#888;
				border:1px solid #888;
				margin-top:30px;
				margin-left:30px;
				margin-right:20px;
				font-family:Open Sans, Arial, sans-serif;
				font-size:0.7em;
				text-align:left;
				text-transform:uppercase;
			}
			.jssor-more-than:hover, .jssor-more-than a:hover
			{
				background-color:'.$color.';
				color:#FFF;
				border:1px solid #FFF;
			}
			.jssor-more-than:active
			{
				background-color:#FFF;
				color:'.$color.';
			}
			
			 .jssord21l, .jssord21r, .jssord21ldn, .jssord21rdn
            {
            	position: absolute;
            	cursor: pointer;
            	display: block;
                background: url(wp-content/plugins/Ermak/img/d21.png) center center no-repeat;
				#background-color:'.$color.';
				padding:10px 0;
                overflow: hidden;
            }
            .jssord21l { background-position: -3px -23px; }
            .jssord21r { background-position: -63px -23px; }
            .jssord21l:hover { background-position: -123px -23px; }
            .jssord21r:hover { background-position: -183px -23px; }
            .jssord21ldn { background-position: -243px -23px; }
            .jssord21rdn { background-position: -303px -23px; }			
			
		</style>
			
			<!-- Jssor Slider Begin -->			
			<div class="lp-slider-cont" id="slider_container_corousel" style="position: relative; top: 30px; left: 0; width: '.$params[15].'px;   height: '. ($smc_height - 30) .'px;">
			
				

				<!-- Slides Container -->
				<div u="slides" style="cursor: move; position: absolute; left: 0; top: 0; width: '. ($params[15] - 50).'px; height: '. ($smc_height - 30) .'px; overflow: hidden; ">';
			$i = 0;
			foreach($slidess as $post)
			{	
				/*
				$attachment_image = get_children( array(
															'numberposts' => 1,
															'post_mime_type' => 'image',
															'post_parent' => $post->ID,
															'post_type' => 'attachment'
														) );
				$attachment_image1 = array_shift($attachment_image);
				$img		= wp_get_attachment_url( $attachment_image1->ID );
				*/
				$img_id		= get_post_thumbnail_id();
				if(!$this->options['show_carusel_bckgnd'])
					$img	= wp_get_attachment_url($img_id);
				$html		.= '	<div class="jssor-bck" style="background-image:url('.$img.'); background-color:'. $options['fills'][$i % count($options['fills'])][0].';">';
				/*----------------------*/
								$html	.=    '		<div class="lp-comment-button-cont">
										<a class="lp-comment-button hint  hint--right" data-hint="'.$post->comment_count. __(" comments to this post", "smc").'">'.
											$post->comment_count.
										'</a>
										<div class="lp-comments-count">'.
											
										'</div>
									</div>
									<div class="jssor-featured">									
										<div class="jssor-featured-cat"> ';
											$cats	= get_the_category( $post->ID );
											if(count($cats))
											{
												$html .= '<span><i class="fa fa-folder-open"></i> ';
												
												foreach($cats as $cat)
												{
													$html .= ' <a href="/?cat='.$cat->cat_ID .'" class="jssor-featured-cat-a">' . $cat->cat_name . '</a>';
													
												}
												$html .= '</span>';//$html .=  the_category(', ');
											}
											$locs	= get_the_terms( $post->ID, SMC_LOCATION_NAME);
											if(count($locs))
											{
											
												$html .= '<span> ';												
												foreach($locs as $loc)
												{
													$location	= SMC_Location::get_instance($loc->term_id);
													$cur_lt		= $Soling_Metagame_Constructor->get_location_type($loc->term_id);
													$html 		.= " <span class='lp_featured_location'>" . $cur_lt->picto ." ". $cur_lt->post_title .'</span> <a href="'. SMC_Location::get_term_link($location->term_id).'" class="jssor-featured-cat-a">' . $location->name . '</a> ';
													
												}
												$html .= '</span>';//$html .=  the_category(', ');
											}
									$html	.=   '</div>
												
										<div class="jssor-featured-text"><div class="jssor-featured-title">'.
												$post->post_title.'
											</div>
											<div class="jssor-featured-content">'.
												wp_trim_words($post->post_content).'
											</div>
											<span>
												<a href="'.get_permalink($post->ID).'" class="jssor-more-than">'.
													__("Read more", "smc").
											'	</a>
											</span>
										</div>										
									</div>';
								
				$html					.= '	<div id="slider'.$i.'_container" class="sub-slider" style="">';	
				$slides					= get_post_meta($post->ID, "slides", true);
				
				$video					= get_post_meta($post->ID, "youtube_id", true);
				$audio					= get_post_meta($post->ID, "audio", true);
							
				if($video != "")
				{
					
					
					$html		.=		'
										<!-- Slides Container -->
										<div id="slide-cont-'.$i.'" f="video" u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width:420px; height:'.($smc_height - 70).'px; overflow: hidden;">
										'; 
					//$html		.=    	'	<div class="ss" style="background-image:url(0.jpg); background-color:transparent;"></div>';
					$html		.=    	'	<div  class="ss" style="background-image:url(http://img.youtube.com/vi/' . $video . '/0.jpg); background-size:cover; width:100%; height:100%; background-position:center;">
												
												<div id="video-player-'.$i.'"  style="dispaly:none;">
													<video width="'.(($smc_height - 70)*4/3).'" height="'.($smc_height - 70).'" id="player'.$i.'" preload="none">
														<source type="video/youtube" src="http://www.youtube.com/embed/'.$video.'?rel=0&wmode=transparent" />
													</video>
												</div>
												<div id="play-button-'.$i.'" class="lp-play-button">
													<a href="javascript:void(0);" onclick="video_click(' .$i. ', ' . "'" .$video. "'" .');">
														<i class="fa fa-play-circle-o"></i> 
													</a>
												</div>
											</div>';
					$html		.=    	'	<div class="ss" style="background-image:url(0.jpg); background-color:transparent;"></div>';
					$html		.=    	'</div>';
					
				}/*
				else if($slides[0] != "")
				{
					
					$html	.=    	'
										<!-- Slides Container -->
										<div f="slide" u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 420px; height: '.($smc_height - 70).'px; overflow: hidden;">';
					foreach($slides as $slide)
					{
						$html	.=    	'	<div class="ss" style="background-image:url('.$slide.');"></div>';
					}
					$html		.=    	'
										</div>';
										<!-- Navigator Skin Begin -->
										
										<!-- jssor slider navigator skin 01 -->
										<!-- navigator container -->
										<div u="navigator" class="jssorn01" style="position: absolute; bottom: 16px; right: 16px;">
											<!-- navigator item prototype -->
											<div u="prototype" style="POSITION: absolute; WIDTH: 12px; HEIGHT: 12px;"></div>
										</div>
										<!-- Navigator Skin End -->
										
										<!-- Direction Navigator Skin Begin -->
										
										<!-- Arrow Left -->
										<span u="arrowleft" class="jssord02l" style="width: 55px; height: 55px; top: '. ($smc_height/2 - 55).'px; left: 8px;">
										</span>
										<!-- Arrow Right -->
										<span u="arrowright" class="jssord02r" style="width: 55px; height: 55px; top: '.($smc_height/2 - 55).'px; right: 8px">
										</span>
										<!-- Direction Navigator Skin End -->';
					
				}*/
				else if($audio != "")
				{
					$html		.='<div id="lp-audio-player" src="'.$audio.'" title="'.$post->post_title.'" style="position:absolute; bottom:0px; left:10px;"></div>';
				} 
					
				$html	.=    			'
									</div>									
								</div>';
				//$html	.= '	</div>';
				$i++;
				
			} 
				
			$html		.= '</div><!-- Slides Container -->';						
			$html		.= '	
								<!-- Navigator Skin Begin -->
										
								<!-- jssor slider navigator skin 01 -->
								<!-- navigator container -->
								<div u="navigator" class="jssorn01" style="position: absolute; bottom: 12px; right: 0">
									<!-- navigator item prototype -->
									<div u="prototype" style="POSITION: absolute; WIDTH: 14px; HEIGHT: 14px;"></div>
								</div>
								<!-- Navigator Skin End -->
								
								<!-- Direction Navigator Skin Begin -->
								
								<!-- Arrow Left -->
								<span u="arrowleft00" class="jssord21l" style="width: 55px; height: 55px; top: '. ($smc_height/2 - 55).'px; left: 3px;">
								</span>
								<!-- Arrow Right -->
								<span u="arrowright00" class="jssord21r" style="width: 55px; height: 55px; top: '.($smc_height/2 - 55).'px; right: 3px">
								</span>
								<!-- Direction Navigator Skin End -->
							</div><!-- Jssor Slider End -->';				
			
		}
		else
		{
			
			$html		= '<div style="position: relative; top: 30px; left: 0; width: '.$params[5].'px;   height: '. ($smc_height - 30) .'px; background-color:'.
			$options['fills'][rand(0,count($options['fills']))][0].'; background-image:url('.SMC_URLPATH.'img/lp_location_default.jpg); background-size:cover;">
				<div class="jssor-featured-text">
					<div class="jssor-featured-title">'.
						$options['default_featured_title'].'
					</div>
					<div class="jssor-featured-content">'.
						$options['default_featured_text'].'
					</div>
				</div>
			</div>';
			
		} 
		
		//return $html;
	}
?>