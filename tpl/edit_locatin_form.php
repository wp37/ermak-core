<?php
		global $smc_height, $location_hide_types, $location_map_types, $Soling_Metagame_Constructor, $map_behoviors;
		$t_id 			= $term->term_id;	
		$term			= SMC_Location::get_instance($t_id);		
		$term_meta 		= SMC_Location::get_term_meta( $t_id );//get_option( "taxonomy_$t_id" ); 
		$map_data		= get_option( "taxonomy_map_$t_id");
		/*
		ob_start();
		print_r($term_meta);
		$var	= ob_get_contents();
		ob_end_clean();   
		print_r ("<tr class='form-field'><th scope='row' valign='top'><label>Location data</label></th></th><td><textarea style='height:200px;'>".$var."</textarea></td></tr>");
		*/
		//echo '<BR>uam_group: '.get_option("uam_group_$t_id")."<br>";
		//echo '<BR>taxonomy_fcell: <BR>';
		//var_dump(get_option("taxonomy_fcell_$t_id"));
		$ltype_id	= $term_meta['location_type'];
		$map_behavior	= (int)get_post_meta($ltype_id, 'map_behavior', true);
		
		?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="term_meta[map_type]"><?php _e('map type', "smc"); ?></label></th>
			</th>
			<td>
				<select  name="term_meta[map_type]" id="map_type" class='chosen-select'>
					<?php
						for($i=0; $i < count($location_map_types); $i++)
							echo '<option value='.$i.' '. selected($term_meta['map_type'], $i).'>'.__($location_map_types[$i], "smc").'</option>'
					?>						
				</select>
				<div class="form-field" id="edit_map"  style="display:<?php echo $term_meta['map_type'] ==1 ? "block": "none"?>;" src="<?php echo SMC_URLPATH.'svg-edit-2.6/svg-editor.html' ?>">
					<div id="edit-map-button" class="button" style="margin-top:10px!important;display:<?php echo (selected($term_meta['map_type'], 1)) ? "display" : "none" ?>;">
						<?php _e("Edit map", "smc"); ?>
					</div>
					<?php echo '<BR>' . $map_data; ?>	
				</div>	
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="term_meta[location_type]"><?php _e('location_type', "smc"); ?></label></th>
			<td>
				<select  name="term_meta[location_type]" class='chosen-select'>
					<?php
						$posts		= get_posts(array('numberposts' => 500,'post_type'=>"location_type", 'orderby'=>"ID", 'order'=>'ASC'));
						foreach($posts as $post)
						{
							$sel			= selected($ltype_id , $post->ID);
							echo "<option  behavior='". get_post_meta($post->ID, "map_behavior", true) ."' $sel value='$post->ID'>".$post->post_title."</option>";
						}					
					?>
				</select>				
			</td>
		</tr>
		<?php 
		$behavior	= "<h1>". $map_behoviors[$map_behavior] ."</h1>";
		if($map_behavior == DOT_MAP_BEHAVIOR)
		{
		?>
			<tr class="form-field">
				<th scope="row" valign="top">
					<label for="term_meta[x_pos]"><?php _e('Position in Map', "smc"); ?></label></th>
				<td>
					<div id="edit-position" class="button" style="margin-top:10px!important;">
						<?php _e("Check position", "smc"); ?>
					</div>
					<?php echo $behavior; ?>
					<div id="map-position" style="display:block; overflow:hidden;position:relative; width:100%; height:400px;">
						<div id="parent_svg_map">
							
							<?php 
								$parent_map		=  get_option( "taxonomy_map_".wp_get_term_taxonomy_parent_id( $term->parent, "location" ) );
								echo $t_id 		== 0 ? $Soling_Metagame_Constructor->get_main_map_svg_data() : $parent_map == 0 ? $Soling_Metagame_Constructor->get_main_map_svg_data() : $parent_map; 
							?>
						</div>
						<?php echo apply_filters("smc_edit_location_main_map", "", $t_id, $term_meta); ?>
						
						<div  class="dot_location" style="display:block; top:<?php echo $term_meta['y_pos'] - 4; ?>px; left:<?php echo $term_meta['x_pos'] - 4;?>px;">
						</div>
						<div id="vertical_dot_position" style="position:absolute; top:0px; left:-2px; height:100%; width:0; border-left:1px solid rgba(255, 255, 255, 0.5); border-right:1px solid rgba(0, 0, 0, 0.25);">
							
						</div>
						<div id="horisontal_dot_position" style="position:absolute; top:-2px; left:0px; width:100%; height:0; border-top:1px solid rgba(255, 255, 255, 0.5); border-bottom:1px solid rgba(0, 0, 0, 0.25);">
							
						</div>
						<div style="position:absolute; bottom:10px; right:10px;">
							<input name="term_meta[x_pos]" id="x_pos" type="number" step="1" value="<?php echo (int)$term_meta['x_pos']?>" style="width:100px;"/> <label for="x_pos">x</label>
							<input name="term_meta[y_pos]" id="y_pos" type="number" step="1" value="<?php echo (int)$term_meta['y_pos']?>" style="width:100px;"/> <label for="x_pos">y</label>
						</div>
					</div>
				</td>
			</tr>
		<?php } ?>
		<tr class="form-field">
			<th scope="row" valign="top">
			<label for="term_meta[creator]"> <?php _e("Location creator", "smc");  ?></label></th>
			<td>
				<?php
				wp_dropdown_users(
																		array(
																				'show_option_none'  => "---",
																				'echo'				=> true,
																				'show'				=> 'display_name',
																				'name'				=> 'term_meta[creator]',
																				'multi'             => true,
																				'class'				=> 'chosen-select',
																				'selected'			=> $term_meta ['creator']
																			  )
																	  );
				?>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
			<label for="term_meta[owner1]"> <?php _e("Location owners", "smc");  ?></label></th>
			<td>
				<?php
				wp_dropdown_users(
																		array(
																				'show_option_none'  => "---",
																				'echo'				=> true,
																				'show'				=> 'display_name',
																				'name'				=> 'term_meta[owner1]',
																				'class'				=> 'chosen-select',
																				'multi'             => true,
																				'selected'			=> $term_meta ['owner1']
																			  )
																	  );
				echo "<p></p>";
				wp_dropdown_users(
																		array(
																				'show_option_none'  => "---",
																				'echo'				=> true,
																				'show'				=> 'display_name',
																				'name'				=> 'term_meta[owner2]',
																				'class'				=> 'chosen-select',
																				'multi'             => true,
																				'selected'			=> $term_meta ['owner2']
																			  )
																	  );
				echo "<p></p>";
				wp_dropdown_users(
																		array(
																				'show_option_none'  => "---",
																				'echo'				=> true,
																				'show'				=> 'display_name',
																				'name'				=> 'term_meta[owner3]',
																				'class'				=> 'chosen-select',
																				'multi'             => true,
																				'selected'			=> $term_meta ['owner3']
																			  )
																	  );
				echo "<p></p>";
				?>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
			<label for="term_meta[hiding_type]"><?php _e("Hiding Type", "smc"); ?></label>
			<td>
				<select  name="term_meta[hiding_type]" id="term_meta[hiding_type]" class='chosen-select'>
					<option <?php selected($term_meta ['hiding_type'],0); ?> value='0'><?php echo __($location_hide_types[0], "smc") ?></option>
					<option <?php selected($term_meta ['hiding_type'],1); ?> value='1'><?php echo __($location_hide_types[1], "smc") ?></option>
					<option <?php selected($term_meta ['hiding_type'],2); ?> value='2'><?php echo __($location_hide_types[2], "smc") ?></option>
				</select>
				
			</td>
		</tr>
		
		<script>
			
			set_chosen(".chosen-select", {max_selected_options: 5});
			jQuery(function($)
			{
				$("#parent_svg_map svg").attr("width", $("#parent_svg_map").width());
				$("#parent_svg_map svg").attr("height", <?php echo $smc_height ?>);
				$("#map-position").height(<?php echo $smc_height ?>);
				$("#parent_svg_map").height(<?php echo $smc_height ?>);
				var change_dot_position	= false;
				$( "#map_type" ).change(function() {
					if($(this).val() ==1)
						$("#edit_map").show("fast");
					else
						$("#edit_map").hide("fast");
				});
				$("#edit-position").click(function(e)
				{					
					$("#map-position").toggle("slow");
				});
				$("#map-position").mouseover(function(e)
				{
					change_dot_position = true;
				});
				$("#map-position").mouseout(function(e)
				{
					change_dot_position = false;
				});
				$("#map-position").mousemove(function(event )
				{
					if(!change_dot_position)	return;
					$("#vertical_dot_position").css({
														top:0,
														left:event.pageX - $(this).offset().left
													});
					$("#horisontal_dot_position").css({
														left:0,
														top:event.pageY - $(this).offset().top
													});
				});
				$("#map-position").click(function(event )
				{
					var left	= event.pageX - $(this).position().left;
					var top		= event.pageY - $(this).position().top;
					var left1	= event.pageX - $(this).offset().left;
					var top1	= event.pageY - $(this).offset().top;
					$("#x_pos").val( Math.floor( left1 ) );
					$("#y_pos").val( Math.floor(  top1 ) );
					$(".dot_location").css({top:top1 - 4, left:left1-4, display:"block"}); 
				})
			});
		</script>
		<?php
		?>
	