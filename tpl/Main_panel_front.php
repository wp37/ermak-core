<?php
		global $UAM;
		global $smc_height;
		global $wp_query;
		global $currenting_url;
		global $smc_main_tor_buttons;
		global $user_iface_color;
		global $is_smc_login_widget;
		$currenting_url		= Assistants::get_current_URL();
		$op					= get_option(SMC_ID);
		if(is_tax('location'))
		{
			$cur_term		= get_term_by('name', single_term_title("",0), 'location');
			$tax_id 		= $cur_term->term_id;
		}
		else
		{
			$tax_id			= 0;
		}
		if(update_user_meta(
													get_current_user_id(), 
													"metagame_panel", 
													array("is_open"=>0, "cur_location"=>$tax_id),
													false
											  ))
		{
			$lp_userdata		= get_user_meta(get_current_user_id(), "metagame_panel");
		}
		else
		{
			
		}
		
		$bar_h		= 0;//is_admin_bar_showing() ? 32 : 0 ;
		
		if(count($smc_main_tor_buttons)>1)
		{
			$svg_tor_data	= array();
			$svg_tor_data[]	= "M-2.7 -93 L-2.7 -38.95 Q-17.6 -38.1 -28.45 -27.35 -31.3 -24.5 -33.35 -21.25 L-80.4 -48.5 Q-74.75 -57.5 -66.8 -65.6 L-62.15 -69.85 Q-46 -84.2 -26.05 -89.65 -14.9 -92.8 -2.7 -93";
			$svg_tor_data[]	= "M66.55 -65.6 Q71.65 -60.45 75.85 -55 L79.55 -49.75 80.45 -48.35 33.6 -21.2 Q31.25 -24.4 28.35 -27.35 17.75 -37.95 3 -38.85 L3 -93 Q15.1 -92.65 26.15 -89.65 48.7 -83.4 66.55 -65.6";
			$svg_tor_data[]	= "M36.3 -16.2 L83.4 -43.45 Q94.25 -23.35 94.25 1.15 94.25 25.9 83.2 46.05 L36.3 18.85 Q40.25 10.9 40.25 1.15 40.25 -8.4 36.3 -16.2";
			$svg_tor_data[]	= "M3 41.35 Q17.75 40.35 28.35 29.55 31.25 26.85 33.25 23.85 L80.35 50.95 Q74.6 59.8 66.55 67.7 40.1 94.35 3 95.45 L3 41.35";
			$svg_tor_data[]	= "M-28.45 29.55 Q-17.6 40.35 -2.7 41.35 L-2.7 95.45 Q-40 94.5 -66.8 67.7 -74.55 59.8 -80.2 51.15 L-33.25 23.85 Q-31.2 26.85 -28.45 29.55";
			$svg_tor_data[]	= "M-89.05 -30.8 Q-86.7 -37.35 -83.3 -43.55 L-36.25 -16.35 Q-40.2 -8.5 -40.2 1.15 -40.2 10.95 -36.05 19.05 L-83.15 46.1 Q-94.3 26 -94.3 1.15 -94.3 -15.9 -89.05 -30.8";
			
			$picto_coord	= array();
			$picto_coord[]	= array('x'=> 155, 	'y' =>104);
			$picto_coord[]	= array('x'=> 155, 	'y' =>64);
			$picto_coord[]	= array('x'=> 93, 	'y' =>50);
			$picto_coord[]	= array('x'=> 40, 	'y' =>104);
			$picto_coord[]	= array('x'=> 36, 	'y' =>124);
			$picto_coord[]	= array('x'=> 93, 	'y' =>155);
		}
		if($op['no_fixed_and_no_closed']){
		?>
			<div id="lp_mg_panel" class="lp-wrapper-relative lp-wrap-open" style="height:<?php print_r( $smc_height + 35 - $op['nav_menu_height_klapan']); ?>px!important; line-height: 1;">
		<?php }else{ ?>
			<div class="lp-wrapper-relative" style="">
			
			</div>
			<div id='lp-wrapper' class='lp-wrap' style="top:<?php echo $bar_h?>px;">
		<?php } ?>
			<div id="lp-main" class='lp <?php if($op['no_fixed_and_no_closed']) echo " lp-open";?>' style="height:<?php print_r( $smc_height); ?>px;">
				<div id="lp-content" class="lp-content lp-white lp-wrap-open" style="background:<?php echo $this->options['main_map_color']; ?>;">
					<div style='position:absolute; top:<?php echo ($smc_height/2-25); ?>px; left:100px; color:white; font-size:30px!important; width:100%;'><?php echo Assistants::get_wait_form();?></div>				
				</div>
				<div  class="lp-navi" id="lp-navi">
					<div style="color:#000!important;display:block;">
						
					</div>
				
					<div id="lp_navi" class="lp-navi-container" style="">
						<?php if($op['use_location_menu']) {?>
							<div class="lp-button" >
								<a  href="javascript:void(0);" onclick="open_metagame_panel('locations');" id='locations' class='trapezium'>
									<?php _e("Map", "smc"); ?>
								</a>
							</div >
						<?php };
						if($op['use_courusel']) {?>
							<div class="lp-button" >
								<a  href="javascript:void(0);" onclick="open_metagame_panel('courusel');" id='courusel' class='trapezium'>
									<?php _e("Breaking News", "smc"); ?>
								</a>
							</div >
						<?php };
						if($op['account_panel']) {?>
							<div class="lp-button" >
								<a  href="javascript:void(0);" onclick="open_metagame_panel('courusel');" id='account' class='trapezium'>
									<?php echo is_user_logged_in() ? wp_get_current_user()->display_name : __("Enter", "smc"). ' | ' .__("Register", "smc"); ?>
								</a>
							</div >
						<?php  } 
							
						?>
					</div>
					<div class="lp-line lp-external-background"> </div>		
					<div id='server_message'></div>
				</div>	
				<div id="lp_navi_compact" style="top:<?php echo ($smc_height) ?>px;">
					<?php if($op['use_location_menu']) {?>
						<div class="lp-button_comp lp-open-select-button" bid="locations" n="0">
							<?php _e("Map", "smc"); ?>
						</div >
					<?php };
					if($op['use_courusel']) {?>
						<div class="lp-button_comp lp-open-select-button" bid="courusel" n="1">
							<?php _e("Breaking News", "smc"); ?>
						</div >
					<?php };
					if($op['account_panel']) {?>
						<div class="lp-button_comp lp-open-select-button" bid="courusel" n="2">
							<?php echo is_user_logged_in() ? wp_get_current_user()->display_name : __("Enter", "smc"). ' | ' .__("Register", "smc"); ?>
						</div >
					<?php  } ?>
					<div id="cur_compact_menu"></div>
				</div>								
				<!--div style="position:absolute; left:0px; top: <?php print_r($smc_height - 60);?>px; z-index:1002;width:200px; height:95px; overflow:hidden;">
					<div id="colorPicker">
						<a class="color" style="background-color:<?php echo $this->get_iface_color();?>"><div class="colorInner"></div></a>
						<div style="">
							<div class="track"></div>
						</div>
						<ul class="dropdown"><li></li></ul>
						<input type="hidden" class="colorInput"/>
					</div>
				</div-->
				<?php 
				if (!$is_smc_login_widget)
				{?>					
				
					<div id="lp_navi_logged_cont" class=" hint hint--left smc-alert"  target_name="options_dialog" width="auto" data-hint="<?php _e(!is_user_logged_in() ? 'Login' : 'Personal actions', 'smc'); ?>" style="top:<?php echo ($smc_height - 2);?>px; ">
						<div id="lp_navi_logged" class="lp_avatar_button lp-border-color" >
							<?php 
							if(is_user_logged_in())
							{ ?>					
								<div id="lp_navi_avatar" class="">
									<?php echo get_avatar( get_current_user_id() , 40); ?>
								</div>
							<?php 
							
							} 
							else 
							{ ?>	
								<div id="lp_login_avatar" class="smc-alert" target_name="login_form" style="display:block; position:absolute; top:0; left:0; width:40px; background:green;">
									<center class="fix_box_size" style="position:absolute; top:12px; width:40px; padding:0; margin:0; line-height: 1.5;">				
										<i class="fa fa-user"></i>						
									</center>
								</div>
							<?php 
							} ?>
						</div>
						<div>
							<?php echo apply_filters("smc_navi_after_avatar", ""); ?>
						</div>
					</div>
					<div id="options_dialog" class="lp-hide" style="">
						<center>
							<div style="width:250px;">
								<?php 
								echo "<div style='display:block;position:relative;'><h2>".__("Personal actions", "smc")."</h2></div>";
								$wid		= new SMC_Login_Picto;
								$wid->widget(array(), array("is_illustrated" => true));
								?>
							</div>
						</center>
					</div>
				<?php } 				
				
				?>
				<div  id="refresher" class="lp-content lp-wrap-open">
					<!--div class= 'lp-full lp-transparent'>
						
					</div>
					<div id="cyrcler" class='lp-refresh111 lp-unvisible'>
						<i class="fa fa-refresh fa-spin"><i>
					</div-->
				</div>
				<div  id="llp-modal" class="lp-wrap-open lp-full lp-hide lp-absolute">
					
				</div>
				
			</div>
			<div style="position:absolute; top:<?php print_r( $smc_height + 25 + $bar_h); ?>px; width:100%; border:none; padding:0; margin:0; pointer-events:none; box-sizing:border-box;">
				<img style="width:100%;height:auto; pointer-events:none;"  src="<?php echo SMC_URLPATH . "img/Shadow-18.png"?>">
			</div>
			
			<?php
			if(count($smc_main_tor_buttons)>1) {
			?>
			<div id="pnl" style="position:absolute; top:<?php echo $smc_height?>px; left:0; z-index:99999; display:block;">
				<?php if($this->options['show_wheel_panel']){?>
				<div style="position:absolute; top:-86px; left:100px; z-index:1; display:block;">
					<svg id="svg_pnl" style="pointer-events:painted;" width="189px" height="189px" viewBox="-94.5 -93 189 189" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							<g id="pnl_buttons">
							<?php
								for($i=0; $i<6;$i++)
								{
									if($i==0)
									{
										$fill	= 0.5;
										$clr	= "#000";
										$stroke	= 0.5;
										$tp		= 'type="option_button" ';
										$ico	= $smc_main_tor_buttons[$i]['ico'];
									}
									else if($smc_main_tor_buttons[$i])
									{
										$stroke	= 0.5;
										$fill	= 0.5;
										$clr	= 0;
										$tp		= 'type="option_button" ';
										$ico	= $smc_main_tor_buttons[$i]['ico'];
									}
									else
									{
										$stroke	= 0.1;
										$fill	= 0.25;
										$clr	= 0;
										$tp		= 'type="option_button" ';
										$ico	= '';
									}
									$ico_x		= $picto_coord[$i]['x']-10;//*.75 ;//-162;
									$ico_y		= $picto_coord[$i]['y']+40;//*.75 + 0 + 0;//-162;
									$ico_targ	= $smc_main_tor_buttons[$i]['targ'];
									$comm		= $smc_main_tor_buttons[$i]['comm'];
									echo '<path comm="'.$comm.'" i="'.$i.'" id="pnl_part'.$i. '" ico="'.$ico.'" ico_targ="'.$ico_targ.'" ico_x="'.$ico_x.'" ico_y="'.$ico_y.'" '.$tp.' stroke-opacity="'.$stroke.'" fill-opacity="'.$fill.'" stroke="#FFF" fill="'.$clr.'" d="'.$svg_tor_data[$i].'"/>';
								}
							
							?>
							</g>
							<g>
								<path id="pnl_center" hint="<?php _e('Statistics','smc'); ?>" height="49" stroke="none" fill="<?php echo $this->get_iface_color();?>" d="M17.75 1.2 Q17.75 4.95 16.45 8.2 L14.15 12 12.55 13.8 Q8.25 18.15 2.4 18.85 L0 19 -2.05 18.85 Q-8.2 18.25 -12.65 13.8 L-14.1 12.1 Q-15.5 10.35 -16.35 8.35 -17.8 5.05 -17.8 1.2 L-17.75 0.15 17.75 0.15 17.75 1.2"/>
							</g>
						</svg>
					</div>
					<?php } ?>
					<div id="zoom_slider_cont">
						<div id='zoommer' class="lp_zoom_pnl_btn">
							<i class='fa fa-search'></i>
						</div>
						<input id="zoom_slider"/>
					</div>
			</div>	
			<?php }?>
			
			
			<?php if(current_user_can("manage_options")){ ?>
			<!--div style="position:fixed; top:0; left:0;z-index:9999999999999;" class="fix_box_size" style="display:block!important;">
				<a class="black_button" style="" href="<?php echo get_admin_url();?>"><i class="fa fa-cogs"></i> <?php _e("administarator's console", "smc");?></a>
			</div-->
			<?php  } ?>
			<div id='right_pnl'>				
				<div id="arrow_dn" class="lp-external-color">
					<i class="fa fa-arrow-down"></i>
				</div>
			</div>
		</div>
		<?php 
		
		$show_masters	= $this->options['show_masters'] ? '<div>
					<a data-hint="'.__("Masters","smc").'" href="?page_id='.$this->options['masters_page_id'].'" class="quick-menu-item fix_box_size hint hint--right">
						<i class="fa fa-users fa-lg"></i>
					</a>
				</div>' : "";
		$show_news		= $this->options['show_news'] ? '<div>
					<a data-hint="' . __("News") . '" href="?cat='.$this->options['quick_menu_cat_id'].'" class="quick-menu-item fix_box_size hint hint--right">
						<i class="fa fa-coffee fa-lg"></i>
					</a>
				</div>' : "";
		if($this->options['show_quick_menu'])
			echo apply_filters("smc_quick_menu",
			'<div id="quick-menu" class="quick-menu">
				<div>
					<a id="back-to-top" href="#" class="quick-menu-item fix_box_size hint hint--right" data-hint="'.__("Up", "smc").'">
						<i class="fa fa-chevron-up fa-lg"></i>
					</a>
				</div>
				<div>
					<a data-hint="'.__("Home").'" href="/" class="quick-menu-item fix_box_size hint hint--right">
						<i class="fa fa-home fa-lg"></i>
					</a>
				</div>'.
				$show_masters .
				$show_news .
			'</div>');
		?>
		
		<div style="padding-left:400px; color:#FFF;display:none;"> <?php  print_r("----".$_COOKIE['cur_location']); ?></div>
		
		<script type="text/javascript">
				var design				= new Array();
				design['lines']			= <?php echo (int)$this->options['design_lines'] ?>;
				design['page_to_panel']	= <?php echo (int)$this->options['page_to_panel'] ?>;
				var ermakurl			= '<?php echo SMC_URLPATH; ?>';
				var width;
				var panel_cont			= '<?php echo $this->options['mail_panel_cont'] == "" ? "body" : $this->options['mail_panel_cont']; ?>';//("#contentwrap");//document.body;//$
				var current_pnl_button 	= 0;
				var location_change_info= <?php echo $this->options['location_change_info'];?>;
				var playerGroup 		= new easyPlayerManager();//mp3-player
				var user_iface_color	= '<?php echo ( $this->get_iface_color() ); ?>';
				smc						= <?php print_r( $smc_height ); ?>;
				is_user_logged_in		= <?php echo (int)is_user_logged_in()?>;
				is_admin				= <?php echo current_user_can("manage_options") ? 1 : 0; ?>;
				cur_location_id			= <?php print_r( (int)$tax_id ); ?>;
				
				(function($)
				{
					
					$( "body" ).fadeIn('slow');
					width					= $(panel_cont).width();
					$(panel_cont).prepend("<div id='lp_main_panel_cont' style=' height:" + (smc + 35) + "px; '></div>");
					$("#lp_main_panel_cont").prepend($("#lp_mg_panel"));
					var height				= 189;
					
					/**/
				})(jQuery);
				<?php if($this->options['show_wheel_panel']){?>
				//=== panel's round buttoms circle ======
				(function($)
				{
					var degr, dur, scl, ico, ico_x, ico_y, ico_targ, comm, comm1, bt_clr, cur;
					var is_pnl_hidden	= false;
					
					var pnl_buttons_left	= $("#pnl_buttons").position().left;
					var toogled_buttons = function(e)
					{
						cur		= $(e.target).attr('i');
						comm1	= $(e.target).attr('comm');
						$("[type=option_button]").each(function(index, domElement)
						{
							if(!is_pnl_hidden)
							{
								degr		= 60;
								dur			= 500;
								scl			= -0.012;
								$(this).fadeIn("fast");
								if($(this).attr("ico"))
								{
									bt_clr		= index == current_pnl_button ? "color:"+user_iface_color : "";
									if($(this).attr("comm"))	comm	= "comm='"+$(this).attr("comm")+"' ";
									ico_x		= -$(this).attr("ico_x");
									ico_y		= -$(this).attr("ico_y");
									ico_targ	= $(this).attr("ico_targ");
									ico			= "<div "+ comm + " i='"+index+"' type='option_button' id='ico_"+index+"' class='pnl_button_ico hint hint--top' data-hint='"+ico_targ+"' style='top:"+ico_x+"px; left:"+ico_y+"px; "+bt_clr+";'>"+$(this).attr("ico")+"</div>";
									$("#pnl").append(ico);
								}
							}
							else
							{
								degr		= -60;
								dur			= 100;
								scl			= 0.012;
								$(this).hide();
								$("#ico_"+index).detach();
							}
							
						});
						alert(smc);
						if(cur)				current_pnl_button = cur;
						if(comm1)			send([comm1, cur_location_id, smc]);
						$("#pnl_buttons").animate({  borderSpacing: degr }, {
							step: function(now,fx) {
							  var sc	= (scl*now-0.25);
							  var rot	= -180;//now;
							  $(this).css('-webkit-transform','rotate('+rot+'deg) scale('+sc+')'); 
							  $(this).css('-moz-transform','rotate('+rot+'deg) scale('+sc+')');
							  $(this).css('transform','rotate('+rot+'deg) scale('+sc+')');
							},
							duration: dur,
							easing: 'swing'
						});
						var lft = is_pnl_hidden ? -1000 : pnl_buttons_left; 
						$("#pnl_buttons").position(function(i,val){
								alert(val.top);
								return {top:val.top, left:lft};
							});
						is_pnl_hidden = !is_pnl_hidden;
						
					}
					$("#pnl").fadeIn("slow");
					$("[type=option_button]").hide();
					$("[type=option_button]").css({'pointer-events':'none'});
					$("#pnl_center").css("cursor", "pointer");
					$("[type=option_button]").css("cursor", "pointer");
					$("#pnl_center").live({"click": toogled_buttons});
					$("[type=option_button]").live({"click": toogled_buttons});
					
					
				})(jQuery);
				//== end circle panel ================================
				<?php } ?>
				<?php 
				$hhhh			= "";
				if($op['use_location_menu']) 
				{
					$hhhh		.= "open_metagame_panel('locations', cur_location_id);";
				} 
				else if($op['use_courusel']) 
				{
					$hhhh		.= "open_metagame_panel('courusel', cur_location_id);";
				} 
				else if($op['account_panel']) 
				{
					$hhhh		.= "open_metagame_panel('account', cur_location_id);";
				}
				echo apply_filters("smc_load_panel_screen", $hhhh, $currenting_url);
				?>
				
			</script>
			
		<?php
		echo Direct_Message_Menager::owner_location_dm_form($owner_id);
?>