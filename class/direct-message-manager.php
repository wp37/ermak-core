<?php
	
	$Direct_mesage_autor_types			= array(
												"System",
												"Administrator",
												"Master",
												"All Players",
												"My assistant",
											);
	$Direct_message_avatars				= array(
												SMC_URLPATH."/img/System.png",
												SMC_URLPATH."/img/System.png",
												SMC_URLPATH."/img/master.png",
												SMC_URLPATH."/img/Administrator.png",
												SMC_URLPATH."/img/assistant.png",
												
											);
	class Direct_Message_Menager{
		
		function __construct()
		{
			
		
			//register direct message type
			add_action( 'init', 					array($this, 'add_direct_message_type'), 13 );
			add_filter( 'post_updated_messages', 	array($this, 'direct_message_messages') );
			
			// мета-поля в редактор
			add_action('admin_menu',				array($this, 'my_extra_fields_direct_message'));
			add_action('save_post_direct_message',	array($this, 'true_save_box_data'));
			
			//set new post in location
			//add_action('wp_insert_post_data', array($this, 'on_insert_post'),11,2); 
			//set new commemt to your post
			add_action('wp_insert_comment', 		array($this, 'on_set_comment'), 10, 2);	

			add_filter('manage_edit-direct_message_columns', array($this, 'add_views_column'), 4);
			add_filter('manage_edit-direct_message_sortable_columns', array($this,'add_views_sortable_column'));
			add_filter('manage_direct_message_posts_custom_column', array($this,'fill_views_column'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php
			add_action('admin_head',				array($this,'add_column_css'));
			add_filter('pre_get_posts',				array($this,'add_column_views_request'));
					
		}	
		public function init_settings()
		{
			$my_post = array(
								  'post_title'   		=> __("Direct Messages", "smc"),
								  'post_type' 			=> 'page',
								  'post_content' 		=> "[smc_personal_page]"  ,
								  'post_status'  		=> 'publish',
								   'comment_status'		=> 'closed',
								);

			$new_loctype_ID								= wp_insert_post( $my_post );
			$options									= get_option(SMC_ID);
			$options["prsonal_page_id"]					= $new_loctype_ID;
			update_option(SMC_ID, $options);
		}
		
		static function deinstall()
		{
			$options									= get_option(SMC_ID);
			wp_delete_post( $options["prsonal_page_id"], true);
		}
		
		function add_direct_message_type()
		{
			$labels = array(
				'name' => __('Direct Messages', "smc"),
				'singular_name' => __("Direct Message", "smc"), // админ панель Добавить->Функцию
				'add_new' => __("add Direct Message", "smc"),
				'add_new_item' => __("add new Direct Message", "smc"), // заголовок тега <title>
				'edit_item' => __("edit Direct Message", "smc"),
				'new_item' => __("new Direct Message", "smc"),
				'all_items' => __("all Direct Messages", "smc"),
				'view_item' => __("view Direct Message", "smc"),
				'search_items' => __("search Direct Message", "smc"),
				'not_found' =>  __("Direct Message not found", "smc"),
				'not_found_in_trash' => __("no found Direct Message in trash", "smc"),
				'menu_name' => __("Direct Messages", "smc") // ссылка в меню в админке
			);
			$args = array(
				'labels' => $labels,
				'description' => 'Direct Message betwin Players',
				'exclude_from_search' =>true, 
				'public' => true,
				'show_ui' => true, // показывать интерфейс в админке
				'has_archive' => true, 
				'menu_icon' =>'dashicons-admin-site', //SMC_URLPATH .'/img/pin.png', // иконка в меню
				'menu_position' => 20, // порядок в меню			
				'supports' => array( 'title', 'editor', 'author')
				,'show_in_nav_menus' => true
				,'show_in_menu' => "metagame"
				,'capability_type' => 'page'
			);
			register_post_type('direct_message', $args);
		}
		
		// see http://truemisha.ru/blog/wordpress/post-types.html
		function direct_message_messages( $messages ) {
			global $post, $post_ID;
		 
			$messages['direct_message'] = array( // direct_message - название созданного нами типа записей
				0 => '', // Данный индекс не используется.
				1 => sprintf( __('Direct Message is updated'). '<a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
				2 => 'Параметр обновлён.',
				3 => 'Параметр удалён.',
				4 => 'Location type обновлена',
				5 => isset($_GET['revision']) ? sprintf( 'Location type восстановлена из редакции: %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
				6 => sprintf( 'Direct Message опубликована на сайте. <a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
				7 => 'Функция сохранена.',
				8 => sprintf( 'Отправлено на проверку. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
				9 => sprintf( 'Запланировано на публикацию: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Просмотр</a>', date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
				10 => sprintf( 'Черновик обновлён. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
			);
		 
			return $messages;
		}
		// мета-поля в редактор
		
		function my_extra_fields_direct_message() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smc"), array(&$this, 'extra_fields_box_func'), 'direct_message', 'normal', 'high'  );
		}
		function extra_fields_box_func( $post )
		{	
			$enabled	= (int)get_post_meta($post->ID, "enabled", true);
			$html		.= '<div>
				<input class="css-checkbox" type="checkbox"  name="enabled" id="enabled" ' . checked(1, $enabled, 0).' />
				<label for="enabled"  class="css-label">'.__("Enabled", "smc").'</label>
			</div><hr/><div>
				<input class="css-checkbox" type="checkbox"  name="viewed" id="viewed" ' . checked(1, get_post_meta($post->ID, "viewed", true), 0).' />
				<label for="viewed"  class="css-label">'.__("viewed","smc").'</label>
			</div><hr/>';
							
			$html 	.= '<div><p>
							<label for="addressee">'.__('addressee', 'smc').' </label>
							<p>'.
							wp_dropdown_users(
												array(
														'show_option_all'	=> __('select all'),
														'show_option_none'	=> __('select none'),
														'name' 				=> 'author', 
														'echo'				=> false, 
														'selected'			=> get_post_meta($post->ID, 'addressee',true),
														'show'				=> 'display_name',
														'name'				=> 'addressee',
														'multi'             => true,
													  )
											  ).
						'</div> 
						<div><hr/>';
			if(current_user_can("administrator"))	
			{
				$html 	.= '
							<label for="source_type">'.__('source type', 'smc').' </label>
							<p>
							<select  name="source_type" id="source_type"  style="">';
								$i=0;
								global $Direct_mesage_autor_types;
								foreach($Direct_mesage_autor_types	as $autor_type)
								{
									$html 	.= '<option '. selected (get_post_meta($post->ID, 'source_type',true), 1) .' value="'.$i.'">'.__($autor_type, "smc").'</option>';
									$i++;
								}
								
							'</select>							
						</div> ';	
			}
			else
			{
				$html	.= '
							<input disabled type="hidden" name="source_type" value="3" style=""/>
						</div> ';
			}
			
			echo $html;
			wp_nonce_field( basename( __FILE__ ), 'direct_message_metabox_nonce' );
		}
		
		function true_save_box_data ( $post_id ) 
		{
			// проверяем, пришёл ли запрос со страницы с метабоксом
			if ( !isset( $_POST['direct_message_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['direct_message_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			// проверяем, является ли запрос автосохранением
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// проверяем, права пользователя, может ли он редактировать записи
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;
			
			update_post_meta($post_id, 'source_type', 	esc_attr($_POST['source_type']));
			update_post_meta($post_id, 'addressee', 	esc_attr($_POST['addressee']));			
			update_post_meta($post_id, 'enabled', 		($_POST['enabled']) ? 1 : 0);			
			update_post_meta($post_id, 'viewed', 		($_POST['viewed']) ? 1 : 0);			
			return $post_id;
		}
		
		//======================
		//
		// оповещения
		//
		//=======================
		
		static function add_new_dm_form($params=null)
		{
			if(!$params)	$params	= array();
			if($params['adresse_id'])
			{
				$adresse	= '
				<br>'.
					get_userdata($params['adresse_id'])->display_name.
					'<input type="hidden" name="addressee" value="'.$params['adresse_id'].'"/>'.
				'<BR>';
			}
			else
			{
				$adresse	='
				<br>'.
						wp_dropdown_users( 
											array(
													'show_option_none'  => "---",
													'echo'				=> false,
													'show'				=> 'display_name',
													'name'				=> 'addressee',
													'multi'             => true,
												  )
										  );
			}
			return '<div class="lp-new-message-container" id="lp-new-message-container1">
				
					<h4>'.__("new Direct Message", "smc").'</h4>
					<div id="req_messages" style="DISPLAY:NONE;margin:5px; padding:5px; background:red; color:white; font-style: italic; font-face:san-serif;"></div>
					<input type="text" size=50 placeholder ="'.__('message title', 'smc').'" maxlengt="30" id="new_master_post_title_text" style="width:100%; margin-right:5px;margin-bottom:3px;"/>
					<br>				
					<textarea rows="5" placeholder ="'.__('Direct Message', 'smc').'" maxlengt="140" id="new_master_post_text" style="height:100px; width:98.5%; margin-right:5px;"></textarea>
					<br>
					<br>
					<div class="ermak_login_picto">
						<div style=" display:inline-block; float:left; width:50%;">
							<label for="addressee">'.__('addressee', 'smc').' </label>'.
							$adresse.
			'			</div>
						<div id="new_message_button_submit" class="lp-button-sector  hint hint--top" data-hint="'.__("Submit").'" style="margin-top:10px;">
							 <img src="'.SMC_URLPATH.'img/talk_ico.png">
						</div>
					</div>					
					'.
					//wp_nonce_field( 'new-direct-message' ).
				'
			</div>';
		}
		
		static function master_dm_form()
		{
			return '<div class="lp-new-message-container" id="lp-new-message-container1">
				<h4>'.__("Call to Master", "smc"). '</h4>
				<div id="req_messages" style="DISPLAY:NONE;margin:5px; padding:5px; background:red; color:white; font-style: italic; font-face:san-serif;"></div>
				<input type="text" size=50 placeholder ="'.__('message title', 'smc').'" maxlengt="30" id="new_master_post_title_text" style="width:100%; margin-right:5px;margin-bottom:3px;"/>
				<br>				
				<textarea rows="5" placeholder ="'.__('Direct Message', 'smc').'" maxlengt="140" id="new_master_post_text" style="height:100px; width:98.5%; margin-right:5px;"></textarea>
				<br>
					<div style=" display:inline-block; float:left; width:50%;">
					
					</div>
					<div class="ermak_login_picto">
						<div id="new_dm_master_button_submit" class="lp-button-sector  hint hint--top" data-hint="'.__("Submit").'" style="margin-top:10px;">
							 <img src="'.SMC_URLPATH.'img/talk_ico.png">
						</div>
					</div>	
			</div>';
		}
		static function owner_location_dm_form($location_id = -1)
		{
			global $is_location_owner_exist;
			if($is_location_owner_exist)	return false;
			$is_location_owner_exist 		= true;
			return '<div class="lp-new-message-container lp-hide" id="lp_dm_owner">
				<h2>'.__("Call to Location owners", "smc"). '</h2>
				<div id="req_messages" style="DISPLAY:NONE;margin:5px; padding:5px; background:red; color:white; font-style: italic; font-face:san-serif;"></div>
				<div id="new_owner_msg_title_text" style="width:100%; margin-right:5px;margin-bottom:3px;"></div>
				<br>				
				<textarea id="dm_owner_text_msg" rows="5" placeholder ="'.__('Direct Message', 'smc').'" maxlengt="140" id="new_master_post_text" style="height:100px; width:98.5%; margin-right:5px;"></textarea>
				<br>
				<div style=" display:inline-block; float:left; width:50%;">
				
				</div>
				<div class="ermak_login_picto">
					<div id="new_dm_loc_owner_submit" location_id="'.$location_id.'" class="lp-button-sector  hint hint--top" data-hint="'.__("Submit").'" style="margin-top:10px;">
						 <img src="'.SMC_URLPATH.'img/talk_ico.png">
					</div>
				</div>	
			</div>';
		}
		
		
		public function single_dm_text($post_id)
		{
			global $Direct_message_avatars;
			global $Direct_mesage_autor_types;
			$post				= get_post($post_id);
							
			$adressee			= get_userdata(get_post_meta($post_id, "addressee", true));
			$source_type		= get_post_meta($post_id, "source_type", true);
			
			if($source_type 	== 3)
			{
				$avatar			= get_avatar( $post->post_author , 90);
				$dispaly_mame	= get_userdata( $post->post_author )->display_name;
			}
			else
			{
				$avatar			= "<img src='".$Direct_message_avatars[$source_type]."'>";
				$dispaly_mame	= __($Direct_mesage_autor_types[$source_type], "smp");
			}
			
			update_post_meta($post_id, "viewed", 1);    
			$html				=	"<div class='lp-direct-message-body-container'>
										<table class='smc-table'>
											<tr>
												<td width=20% class='smc-table'>
													<center class='lp-direct-message-author'>".
														__("Direct Message from", "smc").
														" <br>".
														$avatar.
														"<br>
														<h3>". $dispaly_mame."</h3>".
													"</center>
													<div>
														<div style='margin-top:40px;'>
															<div dm='". $post_id ."' class='lp_dm_delete_btn lp-direct-message-button' >
																
																	<span class='fa-stack fa-lg'>
																		<i class='fa fa-circle  fa-4x fa-stack-2x'></i>
																		<i class='fa fa-trash-o fa-lg fa-stack-1x  fa-inverse'></i>
																	</span> ".
																	__('Delete').
													"			
															</div>
														</div>
													</div>
												</td>
												<td width=80% class='smc-table'>
													<div class='lp-direct-message-title'>".
														$post->post_title.
													"</div>
													<div class='lp-direct-message-content'>".
														$post->post_content.
													"</div>
												</td>
											</tr>
										</table>
									</div>";
		return $html;
		}
		
		public function send_dm($post_data, $addressee_id = -1, $source_type = -1 )
		{
			if($source_type == -1)
				$source_type		= 0;
			$my_post = array(
								  'post_title'   	=> $post_data['post_title'] ,
								  'post_type' 		=> 'direct_message',
								  'post_content' 	=> $post_data['post_content'],
								  'post_status'  	=> 'publish',
								  'post_author'   	=> 1,
								);

			// Вставляем запись в базу данных
			$new_loctype_ID		= wp_insert_post( $my_post );
			add_post_meta($new_loctype_ID, 			"source_type",	$source_type);
			add_post_meta($new_loctype_ID, 			"addressee",	$addressee_id);
			add_post_meta($new_loctype_ID, 			"enabled",		1);
			$mail_level				= get_user_meta($addressee_id, "mail_level_".$source_type);
			$mail					= get_userdata($addressee_id, $user_email);
			if(!$mail_level && isset($mail) && $mail !== "")
			{
				$options			= get_option(SMC);
				
				//wp_mail($mail, $options['project_name'] . ": " . $post_data['post_title'], $post_data['post_content']);
			}
			
			return $new_loctype_ID;
		}
		public function send_to_all_location_owners($location_id, $post_data, $source_type=0)
		{
			$tax					= SMC_Location::get_term_meta($location_id); // get_option("taxonomy_$location_id");
			$owner1_id				= (int)$tax['owner1'];
			$owner2_id				= (int)$tax['owner4'];
			$owner3_id				= (int)$tax['owner3'];
			if($owner1_id >0)		$this->send_dm($post_data, $owner1_id, $source_type);
			if($owner2_id >0)		$this->send_dm($post_data, $owner2_id, $source_type);
			if($owner3_id >0)		$this->send_dm($post_data, $owner3_id, $source_type);
		}
		function on_insert_post($post)
		{
			if($post->post_type != "post")			return;
			$my_post = array(
								  'post_title'   	=> wp_strip_all_tags( "Вы получили коммент" ),
								  'post_type' 		=> 'direct_message',
								  'post_content' 	=> "Вы получили post",
								  'post_status'  	=> 'publish',
								  'post_author'   	=> 1,
								);

			// Вставляем запись в базу данных
			$new_loctype_ID		= wp_insert_post( $my_post );
			add_post_meta($new_loctype_ID, 			"source_type",	$Direct_mesage_autor_types[0]);
			add_post_meta($new_loctype_ID, 			"addressee",	get_current_user_id());
		}		
		
		function on_set_comment($comment_id, $comment_object)
		{
			$comment			= get_comment($comment_id);
			$author				= get_userdata($comment->user_id)->display_name;
			$your_post			= get_post($comment->comment_post_ID);
			$content 			= "<b><a href='".$author->user_url."'>".$author."</a></b> ответил на Ваш пост <b>".$your_post->post_title.".</b><br>".
									"<div class='lp-direct-message-quite'>".wp_trim_words($comment->comment_content, 10)."</div>".
								  "<br><a href='".get_comment_link()."'> перейти к комментарию</a>";
			
			$my_post = array(
								  'post_title'   	=> wp_strip_all_tags( "Комментарий к <b>".mb_substr($your_post->post_title, 0, 10)."</b>. (".get_comments_number().")" ),
								  'post_type' 		=> 'direct_message',
								  'post_content' 	=> $content  ,
								  'post_status'  	=> 'publish',
								  'post_author'   	=> 1,
								);

			// Вставляем запись в базу данных
			$new_loctype_ID		= wp_insert_post( $my_post );
			add_post_meta($new_loctype_ID, 			"source_type",	4);
			add_post_meta($new_loctype_ID, 			"addressee",	get_post(get_comment($comment_id)->comment_post_ID)->post_author);
		}
		
		//==================================================
		//
		// редактируем колонки вкладки "Сообщения" 	
		//
		//=====================================================
		
		function add_views_column( $columns ){
			//$columns;
			$posts_columns = array(
				  "cb" 				=> " ",
				  "title" 			=> __("Title"),
				  "author" 			=> __("Author"),
				  "addressee" 		=> __("addressee", 'smc'),
				  "viewed" 			=> __("viewed", 'smc'),
				  "enabled"			=> __("Enabled", 'smc'),
				  "date" 			=> __("Date"),
			   );
			return $posts_columns;
			
		}
		// добавляем возможность сортировать колонку
		function add_views_sortable_column($sortable_columns){
			$sortable_columns['addressee'] 	= 'addressee';
			$sortable_columns['viewed'] 	= 'viewed';
			$sortable_columns['enabled'] 	= 'enabled';
			$sortable_columns['author'] 	= 'author';
			return $sortable_columns;
		}		
		// заполняем колонку данными	
		function fill_views_column($column_name, $post_id) {
			global $Direct_mesage_autor_types;
			switch( $column_name) 
			{
				case 'author':
					$dd			= get_post_meta($post_id, 'source_type', 1);
					if($dd == 3)
					{
						$user			= get_userdata(get_post_meta($post_id, 'addressee', 1));
						echo "<i class='fa fa-user'></i>  ".$user->display_name;						
					}
					else	
					{					
						echo "<i class='fa fa-cog'></i> <i>".__($Direct_mesage_autor_types[$dd], "smc")."</i>";
					}
					break;
				case 'addressee':
					echo get_userdata(get_post_meta($post_id, 'addressee', 1) )->display_name;
					break;
				case 'enabled':
					echo get_post_meta($post_id, 'enabled', 1) ? '<img src="'.SMC_URLPATH.'img/check_checked.png">' : '<img src="'.SMC_URLPATH.'img/check_unchecked.png">';
					break;
				case "viewed":
					echo get_post_meta($post_id, 'viewed', 1) ? '<img src="'.SMC_URLPATH.'img/check_checked.png">' : '<img src="'.SMC_URLPATH.'img/check_unchecked.png">';
					//echo get_post_meta($post_id, 'viewed', 1) ? '<i class="fa fa-check-square-o fa-2x"></i>' : '<i class="fa fa-square-o fa-2x"></i>';
					break;
			}		
		}
		// изменяем запрос при сортировке колонки	
		function add_column_views_request( $object )
		{
			if($object->post_type!="direct_message") return $object;
			switch( $object->get('orderby'))
			{
				case 'addressee':
					$object->set('meta_key', 'addressee');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'author':
					$object->set('meta_key', 'author');
					$object->set('orderby', 'meta_value_num');
					break;
				case "viewed":
					$object->set('meta_key', 'viewed');
					$object->set('orderby', 'meta_value_num');
					break;
				case "enabled":
					$object->set('meta_key', 'enabled');
					$object->set('orderby', 'meta_value_num');
					break;
			}
			return  $object;
		}
		// подправим ширину колонки через css
		function add_column_css(){
			echo '<style type="text/css">.column-title{width:30%;} .column-addressee{width:15%;} .column-author{width:15%;} .column-viewed, .column-enabled{width:130px;}</style>';
		}
	}
?>