<?php
	
	class SMC_Object_Type
	{
		public $object;
		protected static $_instance;
		function __construct()
		{
			add_filter("smc_add_option", array($this, "init_options"), 10);
			$this->init();
		}
		static function get_instance()
		{
			if(null === self::$_instance)
				self::$_instance = new SMC_Object_Type;
			return self::$_instance;
		}
		function init_options($array)
		{
			require_once(SMC_REAL_PATH.'class/SMC_Map.php');
			$options								= $array;		
			//
			$smc									= get_option(SMC_ID);
			unset($smc['my_log_page_id']);
			unset($smc['prsonal_page_id']);
			unset($smc['about_the_project_page_id']);
			unset($smc['rules_page_id']);
			unset($smc['location_page_id']);
			unset($smc['masters_page_id']);
			$options[SMC_ID]						= array(  "t" => array("type"=>"option"), "obj_type" => "option", "post_type"=>SMC_ID, "merge_type"=>MERGE_OPTION, "data" => $smc);
			//svg map	
			$SMC_Map	= new SMC_Map();
			$svg		= $SMC_Map->get_map_data();
			$str		= '" id="L';
			$smc_map_data1 = explode($str, $svg);
			$locs_ids	= array($smc_map_data1[0]);
			for($i = 1; $i < count($smc_map_data1); $i++)
			{
				$ss		= $smc_map_data1[$i];
				$stt	= stripos($ss, ' ')-1;
				$ff_id	= substr($ss, 0, $stt);
				$ff		= SMC_Location::get_instance($ff_id);
				$piece	= $str . '!!!!' . $ff->slug . '!!!!' . substr($ss, $stt);
				$locs_ids[]	= $piece;
			}
			$smc_map_data							= implode(" ", $locs_ids);
			$options["smc_map_data"]				= array( "t" => array("type"=>"option"), "obj_type" => "option", "post_type"=>"smc_map_data", "merge_type"=>CHANGE_OPTION,  "data" => $smc_map_data);	
			//$options[SMC_HASH]						= array( "t" => array("type"=>"option"), "merge_type"=>CHANGE_OPTION, "data" => get_option(SMC_HASH));	
			return $options;
		}
		function init()
		{
			$o										= array();
			//
			$location_type							= array();
			$location_type['t']						= array('type'=>'post');
			$location_type['slug']					= array('type'=>'string');
			$location_type['picto']					= array('type'=>'string');
			$location_type['hint_behavior']			= array('type'=>'bool');
			$location_type['show_content_type']		= array('type'=>'bool');
			$location_type['map_behavior']			= array('type'=>'number');
			$location_type['color']					= array('type'=>'string');
			$location_type['is_picto']				= array('type'=>'bool');
			$location_type['png_url']				= array('type'=>'string', 'download'=>true);
			$location_type['png_name']				= array('type'=>'string');
			$location_type['png_x']					= array('type'=>'number');
			$location_type['png_y']					= array('type'=>'number');
			$location_type['png_w']					= array('type'=>'number');
			$location_type['png_h']					= array('type'=>'number');
			$location_type['_thumbnail_id']			= array('type'=>'media', 'download'=>true);
			$o['location_type']						= $location_type;
			//
			$direct_message							= array();
			$direct_message['t']					= array('type'=>'post');
			$direct_message['source_type']			= array('type'=>'number');
			$direct_message['viewed']				= array('type'=>'bool');
			$direct_message['enabled']				= array('type'=>'bool');
			$direct_message['addressee']			= array('type'=>'id', "object"=>"user");
			$direct_message['author']				= array('type'=>'id', "object"=>"user");
			$o['direct_message']					= $direct_message;
			//
			$instruction							= array();
			$instruction['t']						= array('type'=>'post');
			$o['instruction']						= $instruction;
			//
			$master									= array();
			$master['t']							= array('type'=>'post');
			$master['user']							= array('type'=>'id', "object"=>"user");
			$master['descr']						= array('type'=>"string");
			$o['master']							= $master;
			//
			$location								= array();
			$location['t']							= array('type'=>'taxonomy');
			$location['map_type']					= array('type'=>'number');
			$location['location_type']				= array('type'=>'id', 'object'=>"location_type");
			$location['x_pos']						= array('type'=>'number');
			$location['y_pos']						= array('type'=>'number');
			$location['creator']					= array('type'=>'id', "object"=>"user");
			$location['owner1']						= array('type'=>'id', "object"=>"user");
			$location['owner2']						= array('type'=>'id', "object"=>"user");
			$location['owner3']						= array('type'=>'id', "object"=>"user");
			$location['hiding_type']				= array('type'=>'number');
			$location['class']						= "SMC_Location";
			$o[SMC_LOCATION_NAME]					= $location;
			
			
			$o										= apply_filters("smc_add_object_type", $o); 
			$this->object							= apply_filters("smc_add_option", $o);
			
		}
		function get($string)
		{
			return $this->object[$string];
		}
		function get_meta($string)
		{
			$r		= $this->get($string);
			unset($r['t']);
			return $r;
		}
		function get_type($string)
		{
			$r		= $this->get($string);
			return $r['t']['type'];
		}
		
		function is_meta_exists($string, $meta)
		{
			$obj	= $this->get($string);
			return array_key_exists($meta, $obj);
		}
		
		function get_object($id, $t)
		{
			global $new_dir;
			$d										=  $this->get($t);
			$obj									= array();
			switch($d['t']['type'])
			{
				case "post":
					$obj					= $this->get_post_elements($id, $t, $d);
					if(is_wp_error($obj))	
						insertLog("SMC_Object.get_object", array($id, $t, $d));
					break;
				case "taxonomy":				
					$obj					= $this->get_taxonomy_element($id, $t, $d);
					if(is_wp_error($obj))	
						insertLog("SMC_Object.get_object", array($id, $t, $d));
					break;
				case "db_row":
					
					break;
				default:
					return $t;
			}
			return apply_filters("smc_get_object", $obj, $obj);
		}
		
		function get_post_elements($id, $type, $data)
		{
			$el								= get_post($id);
			$obj							= array(
														'post_type'	=> $type,
														'obj_type'	=> $data['t']['type'],
														'title'		=> htmlentities(stripslashes ($el->post_title)),
														'name'		=> $el->post_name,
														//'id'		=> $id,
														'text'		=> htmlentities(stripslashes ($el->post_content)),																
													);
			$keys							= array_keys($data);
			$values							= array_values($data);
			for($i = 0; $i < count($keys); $i++)
			{						
				$meta						= get_post_meta($id, $keys[$i], true);
				if($keys[$i] == 't') continue;
				if($values[$i]['type'] == 'id')
				{
					$pos					= $this->get($values[$i]['object']);
					//return $meta . ' -- ' . $values[$i]['object'] . ' -- ' . $pos['t']['type'];
					switch($pos['t']['type'])
					{
						case 'post':
							$obj[$keys[$i]]	= $this->get_post_property($meta,  $values[$i]['object']);
							break;
						case 'taxonomy':
							$obj[$keys[$i]]	= $this->get_taxonomy_property($meta, $values[$i]['object'], $i);
							break;
						case 'user':
							$obj[$keys[$i]]	= $this->get_user_property($meta, $values, $i);
							break;
						case 'array':
							$arr			= $this->get_array_property( $meta, $values ); 
							if(!is_wp_error($arr))	$obj[$keys[$i]] = $arr;
						default:
							ob_start();
							print_r($meta);
							$obj[$keys[$i]]	= ob_get_clean();
							break;
					}
				}
				else if($values[$i]['type'] == 'array')
				{
					$arr			 		= $this->get_array_property($meta, $values[$i]);
					if(!is_wp_error($arr))	$obj[$keys[$i]] = $arr;
				}
				else if($values[$i]['type'] == 'media')
				{
					$meta					= htmlentities(stripslashes (get_post_meta($id, $keys[$i], true)));
					if($values[$i]['download'])
					{	
						$imageUrl			= wp_get_attachment_url(  $meta );
						// echo $imageUrl."<BR>";
						// continue;
						$stt				= (strrpos($imageUrl, '/'))+1;
						$fnn				= (strrpos($imageUrl, '.')) - $stt;
						$filename 			= substr($imageUrl,  $stt, $fnn);
						$thumbnail 			= substr($imageUrl,  $stt);
						$wp_check_filetype 	= wp_check_filetype($imageUrl);
						file_copy($imageUrl, $new_dir ."/".  $filename . "." . $wp_check_filetype['ext']);
						$obj[$keys[$i]]		= $imageUrl;//$filename . "." . $wp_check_filetype['ext'];
					/**/
					}
					//$obj['download']		= true;
				}
				
				else if($values[$i]['type'] == 'bool' || $values[$i]['type'] == 'number')
				{
					$obj[$keys[$i]]			= (int)$meta;
				}
				else if($values[$i]['type'] == 'string')
				{
					
					$meta					= get_post_meta($id, $keys[$i], true);
					$obj[$keys[$i]]			= ($meta);
					if($values[$i]['download'])
					{
					/*
						$stt				= (strrpos($meta, '/'))+1;
						$fnn				= (strrpos($meta, '.')) - $stt;
						$filename 			= substr($meta,  $stt, $fnn);
						$thumbnail 			= substr($meta,  $stt);
						$wp_check_filetype 	= wp_check_filetype($meta);
						file_copy($meta, $new_dir ."/".  $filename . "." . $wp_check_filetype['ext']);
						$obj[$keys[$i]]		= $meta;// $filename . "." . $wp_check_filetype['ext'];
						$obj['download']	= true;
					*/
					}
				}
			}
			return $obj;
		}
		
		/*	=================
		//	
		//	
		//	
			=================*/		
		function get_taxonomy_element($id, $type, $data)
		{	
			if($data['t']['type'] != "taxonomy")	return new WP_Error('no taxonomy');
			$el								= get_term_by("id", $id, $type);
			$parent							= get_term_by("id", $el->parent, $type);
			$parent		=		$parent==0	? "" : $parent->slug;				
			$obj							= array(
														'post_type'	=> $type,
														'obj_type'	=> $data['t']['type'],
														'title'		=> $el->name,
														'name'		=> $el->slug,
														'id'		=> $id,
														'text'		=> '',	
														'parent'	=> $parent
													);
			$class_name						= $data['class'];
			$option							= $this->object_property_args($class_name,"get_term_meta", $id); 
			$keys							= array_keys($data);
			$values							= array_values($data);
			
			for($i = 0; $i < count($keys); $i++)
			{
				if($keys[$i] == "class")	continue;
				if($values[$i]['type'] == 'db_row')
				{
					//$obj[$keys[$i]]	= array("db_field" => $values[$i]['db_field'], "db_name" => $values[$i]['db_name']);
					continue;
				}
				$meta						= $option[$keys[$i]];
				if($values[$i]['type'] == 'id')
				{
					$pos					= $this->get($values[$i]['object']);
					
					//return $meta . ' -- ' . $values[$i]['object'] . ' -- ' . $pos['t']['type'];
					switch($pos['t']['type'])
					{
						case 'post':
							$obj[$keys[$i]]	= $this->get_post_property($meta);
							break;
						case 'taxonomy':
							$obj[$keys[$i]]	= $this->get_taxonomy_property($meta, $values[$i]['object']);					
							break;
						case 'user':
							$obj[$keys[$i]]	= $this->get_user_property($meta);
							break;
						case 'array':
						default:
							ob_start();
							print_r($meta);
							$obj[$keys[$i]]	= ob_get_clean();
							break;
					}
				}
				else if($values[$i]['type'] == 'array')
				{
					ob_start();
					print_r($meta);
					$obj[$keys[$i]]			= ob_get_clean();
				}
				else if($values[$i]['type'] == 'bool' || $values[$i]['type'] == 'number')
				{
					$obj[$keys[$i]]			= (int)$meta;
				}						
				else
				{
					$meta					= $option[$keys[$i]];
					$obj[$keys[$i]]			= $meta;
				}
			}
			return $obj;
		}
		function get_property($val, $value)
		{
			if($value == "number" || $value == "bool")	
				return (int)$val;
			if($value == "string")
				return $val;
			if(is_array($value))
			{
				if($value['type'] == 'id')
				{
					foreach($val as $v)
					{
						switch($value['t'])
						{
							case "post":
								$obj		= $this->get_post_property($v);
								//insertLog("get_property "."post",  $obj );
								return $obj;
							case "taxonomy":
								$obj		= @$this->get_taxonomy_property($v, $value['object']);
								insertLog( "get_property ", array( $obj, $v ) );
								return $obj;
							default:
								insertLog( "get_property error", array( $obj, $value) );
						}
					}
				}
				$obj	= $this->get_array_property($val, $value);
				if(!is_wp_error($obj))
					return $obj;
			}
			//insertLog("get_property", array($val, $value));
		}
		function get_post_property($meta)
		{
			$pp				= get_post($meta);
			if(!$pp)		return "";
			$pos_slug		= $pp->post_name;
			return $pos_slug;
		}
		function get_user_property($meta)
		{
			$user			= get_userdata($meta);
			return $user->user_login;
		}
		function get_taxonomy_property($meta, $object)
		{
			$pp				= get_term_by("id", $meta, $object );
			$pos_slug		= $pp->slug;
			insertLog("get_taxonomy_property", $pp->slug);
			return $pos_slug;
		}
		function get_db_row_property($meta)
		{
			return $meta;
		}
		function get_array_property( $meta, $values )
		{
			if(!is_array($meta) || 	count($meta)==0)	return new WP_Error("parameter is not array");
			if(!is_array($values))	return new WP_Error("SMC_Object element is not array");
			$obj			= array( );
			foreach( $meta as $el => $val)
			{
				foreach($values as  $key => $value )
				{
					if($key	== "type")		continue;
					if($key == "object")
					{
						$obj[] = array($this->get_array_property($val, $value));
					}
					else
					{
						$xx			= $this->get_property($val, $value);
						$obj[$key]	= is_wp_error($xx) ? "" : $xx;
					}
					
				}
			}
			//insertLog("get_array_property = obj", $obj);
			return $obj;
		}
		/*
		��� ������, ����� ��� = id
		*/
		function convert_id($key, $val, $d, $id)
		{
			global $wpdb, $httml, $migration_url, $components;
			require_once(SMC_REAL_PATH."tpl/post_trumbnail.php");	
			if(	
				$key == 'title' || 
				$key == 'post_content' || 
				$key == 'name' || 
				$key == 'obj_type'  || 
				$key == 'id'  || 
				$key == 't'  || 
				$key == 'text'  || 
				$key == 'parent'  || 
				$key == 'post_type'  
				)
				return new WP_Error( $key );
			//echo "<p>-------  ".$key . ': ' . Assistants::echo_me($d[$key]['object'])."</p>";	
			if($d[$key]['type'] == 'id')
			{
				$pos	= $this->get($d[$key]['object']);
				switch($pos['t']['type'])
				{
					case 'post':
						$p			= $wpdb->get_row("SELECT ID FROM ".$wpdb->prefix."posts WHERE post_name='" .$val. "' LIMIT 1", ARRAY_A );
						$val		= $p["ID"];
						break;
					case 'taxonomy':
						$p			= $wpdb->get_row("SELECT term_id FROM ".$wpdb->prefix."terms WHERE slug='" .$val. "' LIMIT 1", ARRAY_A );
						$val		= $p["term_id"];
						break;
					case 'user':						
						break;
					default:						
						break;
				}
			}
			if(is_array($val))
			{
				$components[] = array( "key" => $key, "value" => $val, "id" => $id );
				$val		= ""; //$this->convert_array($key, $val, $id);
			}
			
			if($key == 'png_url')
				$val	= SMC_URLPATH . "picto/" . $val;
			if($key == "_thumbnail_id")
			{
				$wp_upload_dir 		= wp_upload_dir();				
				//$filename			= download_url($migration_url . $val);
				$filename			= (ERMAK_MIGRATION_PATH . $val);
				$httml .=  "<DIV>". $migration_url . $val ."</div>";
				if(is_wp_error($filename))
				{
					$httml .=  "<div style='color:red; font-size:20px;'>". Assistants::echo_me($filename->get_error_messages())."</div>";
				}
				else
				{
					$httml .=  "<DIV>filename = $filename</div>";
					$wp_filetype 	= wp_check_filetype(basename($filename), null );
					cyfu_publish_post($id, $filename);
					return new WP_Error( $key );
				}
			}
			return $val=="" ? new WP_Error( $key ) : $val;
		}
		
		function convert_array($val)
		{
			$arr			= array();
			foreach($val as $k => $v)
			{
				if(!$this->get($k))
				{
					if(is_array( $v ))
					{
						//$arr[$k]	= array();
						//foreach($v as $k1 => $v1)
						{
							$arr[$k]	= $this->convert_array($v);
							//break;
						}						
					}
					else
					{
						$arr[$k]	= $v;
					}
				}
				else
				{
					$tp				= $this->get($k);
					switch($tp['t']['type'])
					{
						case "post":
							$arr[$k]		= Assistants::get_postId_by_slug($v, $k);//$this->convert_id($k, $v, $this->get($k), -1);//$v;
							break;
						case "taxonomy":
							$arr[$k]		= Assistants::get_termId_by_slug($v, $k);
					}
					//echo Assistants::echo_me( $this->get($k));
				}
			}		
			return $arr;
		}
		
		function insert_post_meta($metas, $id, $post_type)
		{
			global $wpdb;
			$d								=  $this->get($post_type);
			foreach($metas as $key=>$val)
			{				
				$val = $this->convert_id($key, $val, $d, $id);				
				if(!is_wp_error($val)) 
					update_post_meta($id, $key, $val);
			}
		}
		function object_property_args($class_name, $method, $args)
		{
			return call_user_func(array($class_name, $method), $args);
		}
	}
	
	function file_copy($file, $distination)
	{
		if($file != "")
		{
			try {
				$adress	= str_replace(esc_url( home_url( '/' ) ), ABSPATH, $file);
				//insertLog("file_copy", $adress);
				$cl		= @copy($adress, $distination) ;
			}
			catch (Exception $e) 
			{
				insertLog("file_copy", $e->getMessage());
			}
			if ($cl)	
			{
				return true;				
			}
		}
		else
			return false;
	}
?>