<?php
	class SMC_Achivment
	{
		public $par_type;
		public $capability_type;
		
		function __construct()
		{
		
		}
		
		
		static function add_achivment()
		{
				$labels = array(
					'name' => __('Achivment', "smc"),
					'singular_name' => __("Achivment", "smc"), // ����� ������ ��������->�������
					'add_new' => __("Add ")." ".__("Achivment", "smc"),
					'add_new_item' => __('add new').' '.__("Achivment", "smc"), // ��������� ���� <title>
					'edit_item' => __("Edit")." ".__("Achivment", "smc"),
					'new_item' => __("new Achivment", "smc"),
					'all_items' => __("all Achivments", "smc"),
					'view_item' => __("view Achivment", "smc"),
					'search_items' => __("Search")." ".__("Achivment", "smc"),
					'not_found' =>  __("Achivment not found", "smc"),
					'not_found_in_trash' => __("no found Achivment in trash", "smc"),
					'menu_name' => __("Achivment", "smp") // ������ � ���� � �������
				);
				$args = array(
					'labels' => $labels,
					'public' => true,
					'show_ui' => true, // ���������� ��������� � �������
					'has_archive' => true, 
					'exclude_from_search' => true,
					'menu_position' => 111, // ������� � ����
					'show_in_nav_menus' => true,
					'show_in_menu' => "metagame",
					'supports' => array(  'title', 'editor', 'thumbnail')
					,'capability_type' => 'post'
				);
				register_post_type('smc_achivment', $args);
		}
		
		
		// ����-���� � ��������		
		static function my_extra_fields_smc_achivment() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smc"), array(__CLASS__, 'extra_fields_box_smc_achivment_func'), 'smc_achivment', 'normal', 'high'  );
		}
		static function extra_fields_box_smc_achivment_func( $post )
		{
			global $achivment_types;
			$count				= get_post_meta($post->ID, 'count', true);
			$current_count		= get_post_meta($post->ID, 'current_count', true);
			$post_meta			= get_post_meta($post->ID, 'post_meta', true);
			$post_meta_value	= get_post_meta($post->ID, 'post_meta_value', true);
			$winner_id			= get_post_meta($post->ID, 'winner_id', true);
			$par_type			= get_post_meta($post->ID, 'par_type', true);
			
			?>
			<div style='display:inline-block;'>
				
				<div style='float:left; position:relative; display:inline-block;'> 
					<div class='h'>	 					
						<label  class="h2" for="par_type"><?php echo __("Object Type", "smc"); ?></label><br>
						<select class="h2" id="par_type" name="par_type">
							<option> -- </option>
						<?php
							foreach($achivment_types as $achivment_type=>$val)
							{
								echo "<option value=" . $val->par_type . " " . selected($val->par_type, $par_type, true) . "> " . $achivment_type . " </option>";
							}
						?>
						</select>
					</div>
					<div class='h'>	 					
						<label  class="h2" for="post_meta"><?php echo __("Meta name", "smc"); ?></label><br>
						<input  class="h2" id="post_meta" name="post_meta" value="<?php echo $post_meta;?>"/>	<br>					
						<label  class="h2" for="post_meta_value"><?php echo __("Meta value", "smc"); ?></label><br>
						<input  class="h2" id="post_meta_value" name="post_meta_value" type="number" min="0" step="1" value="<?php echo $post_meta_value;?>"/> 
					</div>
					<div class='h'>	 				
						<label  class="h2" for="count"><?php echo __("Critical count", "smc"); ?></label><br>
						<input  class="h2" id="count" name="count" type="number" min="0" step="1" value="<?php echo $count;?>"/> <BR>				
						<label  class="h2"><?php echo __("Current count", "smc").": ". (int)$current_count; ?></label>
					</div>
				</div>				
				<div style='float:left; position:relative; display:inline-block;'> 
					<div class='h'>	
						<div class="h2">
						<?php if($winner_id)
						{
							$loc		= get_term_by("id", $winner_id, "location");
							echo sprintf(__("Winner is %s ", "smc"), $loc->name) . " ";
						}
						else
						{
							_e("Achivment is haven't winner.", "smc");
						}
						?>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
		static function true_save_box_data($post_id) 
		{
			global $wpdb;
			// ���������, �������� �� ������ ���������������
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// ���������, ����� ������������, ����� �� �� ������������� ������
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;	
			
			update_post_meta($post_id, 'count', 					$_POST['count']);
			update_post_meta($post_id, 'par_type', 					$_POST['par_type']);
			update_post_meta($post_id, 'post_meta', 				$_POST['post_meta']);
			update_post_meta($post_id, 'post_meta_value', 			$_POST['post_meta_value']);
			return $post_id;
		}
		
		static function add_views_column( $columns ){
			//$columns;
			$posts_columns = array(
				  "cb" 				=> " ",
				  "title"	 		=> __("Title"),
				  "par_type"	 	=> __("Object Type", "smc"),
				  "count"	 		=> __("Count", "smc"),
			   );
			return $posts_columns;			
		}
		// ��������� ����������� ����������� �������
		static function add_views_sortable_column($sortable_columns){						
			$sortable_columns['par_type'] 					= 'par_type';						
			$sortable_columns['count'] 						= 'count';						
			return $sortable_columns;
		}	
		// ��������� ������� �������	
		static function fill_views_column($column_name, $post_id)
		{
			$post			= get_post($post_id);
			switch( $column_name) 
			{	
				case 'count':
					echo get_post_meta($post_id, 'count', true);;
					break;		
				case 'par_type':
					$par_type		=  get_post_meta($post_id, 'par_type', true);
					echo $par_type;
					break;		
			}		
		}
		
		function init($parameters)
		{
			$this->par_type 		= $parameters['par_type'];
			$this->capability_type	= $parameters['capability_type'];
			$this->post_meta		= $parameters['post_meta'];
		}
	}
?>