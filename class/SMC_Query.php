<?php
	class SMC_Query
	{
		function __construct()
		{
			
		}
		function get_args($post_type, $params)
		{
			global $wpdb;
			$params['meta_query']	= $this->reform_post_meta($params['meta_query']);
			$default	= array(
				"orderby"			=> "DESC",
				"order"				=> "ID",
				"post_status"		=> "publish",
				"meta_query"		=> "1=1",
			);
			foreach($params as $obj=>$val)
			{
				$default[$obj]			= $val;
			}			
			$query		= "
SELECT * FROM " . $wpdb->prefix . "posts
INNER JOIN " . $wpdb->prefix . $post_type . "_meta AS pmeta ON ( " . $wpdb->prefix . "posts.ID = pmeta.id ) 
WHERE " . apply_filters("smc_query_where", 
	"1=1
	AND " . $default['meta_query'] . "
	AND (" . $wpdb->prefix . "posts.post_status = '" . $default['post_status'] . "')"
).
"GROUP BY " . $wpdb->prefix . "posts.ID
ORDER BY " . $wpdb->prefix . "posts.post_date " . $default['order'] . "
				";
			//insertLog("get_args", $query);
			return $query;
		}
		function get_posts($post_type, $params)
		{
			global $wpdb;
			if(!isset($params))		return WP_Error("parameters must be set");
			if(!is_array($params))	return WP_Error("parameters must be an array");
			$query					= apply_filters("smc_query_args", $this->get_args($post_type, $params));							
			return $wpdb->get_results($query);			
		}
		function reform_post_meta($meta)
		{
			if(!$meta)	return " 1=1 ";
			$mtext					= "(";
			foreach($meta as $key => $value)
			{
				foreach($value as $k => $v)
				{
					if ( !isset($v['compire'] ))	$v['compire'] = "="; 
					if ( isset($v) && is_array($v['value']))
					{
						$v['compire']	= " IN (";
						$v['value']		= implode("','",$v['value']);
						$add			= ")";
					}
					$mtext			.= "pmeta." . $v['key'] . " " . $v['compire'] . " '" . $v['value'] . "'" . $add;
				}
				$mtext				.= " AND ";
			}
			$mtext					.= " 1=1)";
			
			return $mtext;
		}
	}
?>