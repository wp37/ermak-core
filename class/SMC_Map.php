<?PHP
	class SMC_Map
	{
		function __construct()
		{
			
		}
		public function get_map_data()
		{
			$svg	= get_option("smc_map_data");
			//if($svg == "")
			//	$svg = "<svg width='1100' height='400' xmlns='http://www.w3.org/2000/svg' xmlns:svg='http://www.w3.org/2000/svg'></svg>";
			return $svg;
		}
		public function set_map_data($data)
		{
			update_option("smc_map_data", $data);
			$this->update_version();
		}
		public function get_version()
		{
			return (int)get_option("smc_map_version");
		}
		public function update_version()
		{
			$cur_v		= $this->get_version();
			update_option("smc_map_version", $cur_v+1);
		}
		
		//draw planet from map svg	
		public function get_location_map(
											$cur_location, 			// Location taxonomy
											$cur_lt, 				// Location Type data (post)
											$cur_location_options, 	// Location metadata
											$locations,				// children Locations
											$map_data
										)
		{
			function by_y_pos($a, $b)
			{
				$a_loc	= SMC_Location::get_term_meta($a);
				$b_loc	= SMC_Location::get_term_meta($b);
				//insertLog("by_y_pos", (int)$a_loc['y_pos']);
				return(int)$a_loc['y_pos'] - (int)$b_loc['y_pos'];
			}
			global $Soling_Metagame_Constructor;
			global $grandchildren;
			global $dot_content;
			global $lostlocations;
			global $UAM;
			global $cities;
			global $map_patterns;
			$html			= '';
			if(($map_data)=='')			
				return $html;
			do_action("smc_add_grandchildren_data_to_map");
			//if(count($locations)==0)		return $html;
			$cities 		= 0;
			
			$contour_color	= $Soling_Metagame_Constructor->options['contour_color'] ;
			//$html			.= '<svg width="1000" height="'.$smc_height.'" xmlns="http://www.w3.org/2000/svg">';
			$svg			= new SimpleXMLElement($map_data);
			$t1				= $svg->g[0]->children();	
			$gchn			= 0;
			$all_ids		= array();
			$grandchildren 	= '<div id="grandchildren" class="lp-grangchildren">';
/*
			$map_patterns	.= '
			<svg width="0" height="0"  xmlns="http://www.w3.org/2000/svg">
				<defs>
					<pattern id="land_pattern" patternUnits="userSpaceOnUse" width="217" height="217">
						<image xlink:href=/wp-content/uploads/2015/03/land_pattern.jpg" x="0" y="0" width="217" height="217" />
					</pattern>
				</defs>
			</svg>';
*/
			foreach($t1 as $child)
			{
				
				if($child->getName() == 'title') 	continue;
				//if($child->getName() == 'path')		$path = $b;
				$html		.= '<g svg_elem class="lp_svg_map" ';  
				$arr 		= $child->attributes();
				$id			= -1;			
				$attrs		= '<'.$child->getName().'  ';
				foreach($arr  as $a => $b)
				{			
					switch($a)
					{
						case "type":
							$type	= $b;
							break;
						case "stroke-opacity":
						case "fill-opacity":
						case "stroke":
							continue;
							break;
						case "id":
							$id		= $b;
							break;
						case "d":
							$path 	= $b;
							break;
					}
					/**/
					if($a == 'stroke-opacity' || $a == 'fill-opacity' || $a == "stroke")	
						continue;
					if($a=='id')
					{
						$id		= $b;
					}
					if($a=="d")	$path = $b;
					if($a == "fill")	$my_fill	= $b;
					
					$attrs	.=  $a.'="'.$b.'" ';
				}
				
				if(isset($type))
				{
					switch($type)
					{
						case "city":
							$html		.= " type='city'";
							break;
					}
				}
				
				if($id == -1)
				{
					$html	.= ' >'.$attrs.' fill-opacity="1" stroke="#FFFFFF"  stroke-opacity="0.0" ></'.$child->getName().'></g>';
				}				
				else
				{
					
					$id 				= mb_substr($id, 1, strlen($id));				
					for($i=0; $i<count($locations); $i++)
					{	
						//array_push($ids, $locations[$i]->term_id);return $html;
						if($locations[$i]->term_id == $id)
						{
							if(!$UAM->isVisibleLocation($id)) continue;
							array_push($all_ids, $id);
							$term_data	= SMC_Location::get_term_meta( $id );//get_option("taxonomy_". $id);
							$term_link	= SMC_Location::get_term_link( $id );
							
							$html .= 'term_id="'.$id.'" name="'.$locations[$i]->name. '" cx="" cy="" picto ="'.($term_data->picto) .'" targ="'. '"  my_fill="'.$my_fill.'"';
							
							// -- Поиск внуков по типу location--  
							/**/
							$tax_id 					= $locations[$i]->term_id;
							$tax_children 				= get_terms( SMC_LOCATION_NAME, array("parent"=>$tax_id, 'order'=>'DESC', "orderby"=>"y_pos", 'number'=>100, 'hide_empty' => false, 'fields' => 'ids')); 
							usort($tax_children, "by_y_pos");
							$ii							= 0;
							$grandchildren				.= '<div id="grandchild_'.$tax_id.'" term_id="'.$tax_id.'" class="grandchildren-mama">';
							$gchn						= 0;
							$grandchildren				.= '
							<div style="position:absolute; top:29px; left:5px; width:160px;">
								<div style="display:inline-block; width:160px;">';
								
							foreach ($tax_children as $child_id) 
							{
								if(!$UAM->isVisibleLocation($child_id)) continue;
								$this->draw_child($child_id, $tax_id);
							} 
							/*	*/
							$grandchildren				.= '	</div>	
																<div id="map_element_'.$tax_id.'" style="position:relative; display:none; margin:0;">' . apply_filters("smc_add_grandchildren_element_to_map", "", $tax_id) . '</div>';
							$grandchildren				.= '</div>
														</div>';
							break;
						}
					}
					
					$html	.= '>'.$attrs.' fill-opacity="1" stroke="'. $contour_color .'"  stroke-opacity="'. ($Soling_Metagame_Constructor->options['enable_contour'] ? .75 : 0) .'" ></'.$child->getName().'></g>';
				}
					
			}
			$html			.= '
			<g id="dop_paths" class="null"></g>';
			//
			$ids			= array();
			for($i=0; $i<count($locations); $i++)
				array_push($ids, $locations[$i]->term_id);
			$losts			= array_diff ($ids, $all_ids);
			$lostlocations	= '';
			$i=0;
			foreach($losts as $lost)
			{
				$l			= get_term_by('id', $lost, 'location');
				$lt			= $Soling_Metagame_Constructor->get_location_type($lost);
				if($lt->map_behavior==0)	continue;
				$lostlocations.='<div svg_elem  id="loc' . $lost . '" location_id="' . $lost . '" name="' . $l->name . '" class="lp-map-name lp-external-background" style="top:'. (32*$i++).'px; border: 1px solid transparent; ">
									<div style="width:400px;">'. 
										$lt->picto . ' ' . $l->name.
								'	</div>
								</div>';
			}
			//		
			$html			.= '</svg>';
			$grandchildren	= $grandchildren.'</div>'. '<div id="dot_content">'.$dot_content . '</div>';
			return $html;
		}
		function draw_child($child_id, $tax_id)
		{		
			global $Soling_Metagame_Constructor;			
			global $grandchildren;	
			global $dot_content;
			global $cities;
			global $UAM;
			
			$text						= "";
			$term1 		= SMC_Location::get_instance($child_id);//get_term_by( 'id', $child_id, 'location' );
			$term_link 	= SMC_Location::get_term_link($child_id, 'location');	
			$t_name		= $term1->name;
			$gch_data	= SMC_Location::get_term_meta( $child_id ); //get_option("taxonomy_". $child_id);
			$t_parent_id = $term1->parent;
			$d			= $Soling_Metagame_Constructor->get_location_type($child_id);
			if($t_parent_id != $tax_id)	continue;
			$cities++;
			
			if(($gch_data['x_pos'] != 0))
			{
				
				switch($d->is_picto)
				{
					case 0://dot
						$dctop					= $d->radius/2+15;
						break;
					case 1://pictogramm font-awesome
						break;
						$dctop					= 30;
					case 2://.png
						$dctop					= -5;									
						break;
					case 3:
						
						break;
				}
				
				//if($Soling_Metagame_Constructor->options['design_show_grandchildren'])
				{
					$dot_content_ii					= "<div ddoott style='display:inline-block; position:relative; margin-bottom: 10px;'>";
					$gchilds						= SMC_Location::get_child_location_ids($child_id);
					foreach($gchilds as $g)
					{
						if($g == $child_id)			continue;
						$gll						= SMC_Location::get_instance($g);
						$glld						= $Soling_Metagame_Constructor->get_location_type($g);
						
						$dot_content_ii				.= '
						<span svg_elem parent=' . $child_id . ' style="position:relavive; float:left; margin:0px; width:1.8em; height:1.8em; display:block;">
							<span class="lpdl lp-location-child-map hint  hint--top" location_id="' . $g . '" name="'. $gll->name . '" data-hint="'. $gll->name . '" style="background:' . $glld->color . ' ;font-size:13px;" params="0">'.
								$glld->picto .  
							'</span>
						</span>';
						/**/
					}
					$dot_content					.= "<div svg_elem class='dot_content' parent_id='" . $child_id . "' top='$dctop'>" . $dot_content_ii . '</div>';
				}
				
				switch($d->hint_behavior)
				{
					case 1:
						$hint_t					= "lp_svg_map_hint";
						break;	
					default:
						$hint_t					= "hint";
				}
				switch($d->show_content_type)
				{
					case 1:
						$dot_content		.= apply_filters("smc_dot_content", "", $child_id);
						break;
					case 0:
					default:
						$dot_content 			.= "";
				}
				
				switch($d->is_picto)
				{
					case 0://dot
						$grandchildren			.= '
												<div svg_dot  left="'.($gch_data['x_pos'] - $d->radius/2).'" top="'.($gch_data['y_pos'] - $d->radius/2).'" fill="'.$d->color.'" w="'.($d->radius/2).'" h="'.($d->radius/2).'"  center_x="0" center_y="0" style="display:inline-block;">
													<div svg_elem class="lpdl dot_location hint--top '.$hint_t.'" plink="'.SMC_Location::get_term_link($term1->term_id).'" location_id="'.$term1->term_id.'" loc_id="'.$term1->slug.'" name="'. $t_name.'" param="0" data-hint="'. $t_name.'" style="background:'.$d->color.'; width:'.$d->radius.'px; height:'.$d->radius.'px">'.
														
													'</div>'.
													apply_filters("smc_map_child_content", "", $term1->term_id, $d).
												'</div>';
						break;
					case 1://pictogramm font-awesome
						$grandchildren			.= '
												<div svg_dot left="'.($gch_data['x_pos']-6).'" top="'.($gch_data['y_pos']-6).'" w="10" h="10" center_x="0" center_y="0">
													<div svg_elem class="lpdl picto_location hint--top '.$hint_t.'" plink="'.SMC_Location::get_term_link($term1->term_id).'"  location_id="'.$term1->term_id.'" loc_id="'.$term1->slug.'" name="'. $t_name.'" param="0" data-hint="'. $t_name.'" style="background:'.$d->color.'">'.
														$d->picto.
													'</div>'.
													apply_filters("smc_map_child_content", "", $term1->term_id, $d).
												'</div>';
						break;
					case 2://.png
						$grandchildren			.= ' 
												
												<div svg_dot left="'.($gch_data['x_pos']).'" top="'.($gch_data['y_pos']).'" w="' . $d->png_w . '" h="' . $d->png_h .'" center_x="' . $d->png_x . '" center_y="' . $d->png_y . '" >
													<div svg_elem class="lpdl png_location hint--top '.$hint_t.'" plink="'.SMC_Location::get_term_link($term1->term_id).'"  location_id="'.$term1->term_id.'" loc_id="'.$term1->slug.'" name="'. $t_name.'" param="0" data-hint="'. $t_name.'" style=position:absolute;">'.
														'<img src="'.$d->png_url.'">
													</div>'.
													apply_filters("smc_map_child_content", "", $term1->term_id, $d).
												'</div>';								
						break;
					case 3:
						
						break;
				}
				
				
			}
			else
			{
				//if($Soling_Metagame_Constructor->options['design_show_children'])
				{
					$is_own					= $Soling_Metagame_Constructor->cur_user_is_owner($term1->term_id) ? Assistants::get_short_your_label()  : "";
					$grandchildren			.= '
					<span svg_elem location_id="' . $term1->term_id . '" plink="'.SMC_Location::get_term_link($term1->term_id).'" loc_id="'.$term1->slug.'" name="' . $t_name . '" class="lpdl" param="0" style="position:relavive; float:left; margin:0px; width:1.8em; height:1.8em; display:block;">
						<span class="lp-location-child-map hint  hint--top" data-hint="'. $t_name.'" style="background:'.$d->color.';font-size=1em;">'.
							$d->picto .  "" . $is_own .
					'	</span>
					</span>';
				}
			}
			$grandchildren			.= "<div svg_elem class='grandchild_widget_content' id='grand_child_widget_data".$child_id."'>". apply_filters("smc_add_grandchild_data", 'bbb', $child_id). "</div>";
			$dot_content 			.= "</div>";
			//if(++$ii>10)		break;
		}
	}
?>