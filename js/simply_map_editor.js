
var current_val_id, current_svg_id, save_map_handler;

jQuery( document ).ready(function( $ ) 
{  
	//file uploader
	var wrapper, 
		wrapper = $( ".svg_upload" ),
        inp = wrapper.find( "input" );
	var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;
    inp.live({change:function()
	{
        var file_name;
		wrapper = $( ".svg_upload" ),
        inp = wrapper.find( "input" );
        if( file_api && inp[ 0 ].files[ 0 ] )
            file_name = inp[ 0 ].files[ 0 ].name;
        else
            file_name = inp.val().replace( "C:\\fakepath\\", '' );

        if( ! file_name.length )
            return;
		//console.log(inp[ 0 ].files[ 0 ] );
		
		var reader = new FileReader();
		reader.onload = function (e) 
		{
			if($("#geom-svg svg").size() == 0)
				$("#geom-svg").append('<svg xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg"></svg>');
			$("#geom-svg svg").empty().append( $(e.target.result).children("g"));
			var rect = $("#geom-svg svg g")[0].getBoundingClientRect()
			var wwww	= parseInt(rect.width);
			var hhhh	= parseInt(rect.height);
			if(wwww<1100)	wwww  = 1100;
			if(hhhh<400)	hhhh  = 400;
			$("#geom-svg svg").width(wwww ).attr("width", wwww);
			$("#geom-svg svg").height( hhhh ).attr("height", hhhh);
			$(".geom-cont").width( wwww );
			$(".geom-cont").height( hhhh );
			$("#geom-svg").width( wwww);
			$("#geom-svg").height( hhhh );
			$("#geom-svg").closest(".sub-wrap").css({"max-width": wwww + "px" , width: wwww});
		}
		reader.readAsText( inp[ 0 ].files[ 0 ], "utf-8" );
    }})
	
		
	var active		= function (selector)
	{
		if($(".svg_menu_btn").size()<1) 	return;
		$(".svg_menu_btn").removeClass("selected");
		$(".svg_menu_btn").addClass("unselected");
		$(selector).removeClass("unselected");
		$(selector).addClass("selected");
		cur_item		= $(selector).attr("item");
		//alert(cur_item);
	}
	$(".svg_menu_btn").live("click", function(event)
	{
		active(this);
	});
	$("[item=map_pic_paint]").live({"click":function(e)
	{
		active("[item=map_draw]");
	}});
	$("[item=save_map_button]").live({"click":function(e)
	{
		save_map_handler();
	}});
	
	save_map_handler = function()
	{
		/*
		$("#smc_location_map1").detach();
		var ss = $("#smc_location_map").clone();
		ss.attr("id", "smc_location_map33");
		var svg = '<svg id="smc_location_map1" width="1000" height="400" xmlns="http://www.w3.org/2000/svg" style="vector-effect: non-scaling-stroke;" stroke="null"></svg>';
				
		$( svg ).append( $(ss).html() ).appendTo( "body" );
		*/
		var sssvvvggg = '<svg width="1100" height="400" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">' + $("#geom-svg svg").html() + "</svg>";
		//console.log( sssvvvggg );
		send(['save_map_admin', sssvvvggg, '0']);
	}
	
	
	$("body").mousedown(function(e)
	{
		var subj = $("#id_location_cont");
		if(e.target.id == subj.attr("id"))
			subj.hide();
	});
	$("path, ellipse").live({mouseup:function(e)
	{
		select_path(this);
	}});
	var select_path = function(selector)
	{
		$("#id_location_cont").fadeIn("slow");
		$("path, ellipse").each(function(nn, elem)
		{
			//if($(elem).attr("default_stroke"))
			{
				$(elem).attr("stroke", "default_stroke");
			}
		});
		$(selector).attr('default_stroke', $(selector).attr("stroke"));
		$(selector).attr('stroke', '#ff0000');
		$(selector).attr('stroke-width', '2');
		$(selector).attr('stroke-opacity', '1');
		current_svg_id =  $(selector).attr("id");
		if(current_svg_id)
		{
			if(current_svg_id.substring(0,1) == "L")
				current_val_id  = current_svg_id.substring(1);
			else
				current_val_id  = -1;
			$("#id_location_cont option[value="+current_val_id+"]").attr('selected', 'selected');
		}
	}
	$("#id_location_cont").change(function(e)
	{
		var new_id = $('#id_location_cont option:selected').val();		
		$("#L"+new_id).attr("id", "svg_" + $.now());
		$("#"+current_svg_id).attr("id", "L"+new_id);
	});
	var set_map = function(args)
	{
		
	}
		
	$("#edit_map_instruction").live({click:function(evt)
	{
		//a_alert("AA");
		a_alert_div( "map_instruction" );	
	}});
});