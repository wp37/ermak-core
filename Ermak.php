<?php

/*
Plugin Name: Ermak. Metagame
Plugin URI: http://wp-ermak.ru/?page_id=135&d_d=54
Description: Easy way to space for a multiplayer strategy  Live action Role-playing game
Version: 3.1.2 
Date: 28.08.2015
Author: Genagl
Author URI: http://www.genagl.ru
License: GPL2
*/
/*  Copyright 2014  Genagl  (email: genag1@list.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/ 
//библиотека переводов
function init_ermak_textdomain() {
	global $need_login_stroke;
	if (function_exists('load_plugin_textdomain')) 
	{
		load_plugin_textdomain("smc", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/');  
		$need_login_stroke = "<div class='smp-comment'><board_title>".__("You must logged in!", 'smc')."</board_title><div class='smc-alert button smc_padding' target_name='login_form' title='Login'>".__('Login', 'smc')."</div></div>";
	} 
} 
// Вызываем ее до начала выполнения плагина 
add_action('plugins_loaded', 'init_ermak_textdomain');

//Paths
define('SMC_URLPATH', WP_PLUGIN_URL.'/Ermak/');
define('SMC_ID','smc_');
define('SMC_REAL_PATH', WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/');
define('SMC_HASH', SMC_ID."hash");
define('SMC_LOCATION_NAME', "location");
define('SMC_LOCATION_TYPE_NAME', "location_type");
define('CHANGE_OPTION', "change");
define('MERGE_OPTION', "merge");

define('CHANGE_LOCATION_MEMBERS',	0);
define('CHANGE_BATCH_DISLOCATION', 	1);
define('FINISH_PRODUCTION_CYRCLE', 	2);
define('FINISH_CONSUME_CYRCLE', 	3);
define('CREATE_LOCATION', 			4);
define('CREATE_FACTORY', 			5);
;
define('SRC_YOUTUBE_PREFIX', 		"http://www.youtube.com/embed/");
define('YOUTUBE_VIDEO_TYPE', 		"youtube");

define('LOGIN_WIDGET_REASON', 		"login_widget_reason");
$smc_height 	= 500;

require_once(SMC_REAL_PATH.'class/SMC_Post.php');
require_once(SMC_REAL_PATH.'class/SMC_Location.php');
require_once(SMC_REAL_PATH.'class/SMC_Location_Type.class.php');
require_once(SMC_REAL_PATH.'class/Assistants.php');
require_once(SMC_REAL_PATH.'class/instruction_menager.php');
require_once(SMC_REAL_PATH.'class/master-manager.php');
require_once(SMC_REAL_PATH.'class/UAM.php');

require_once(SMC_REAL_PATH.'widgets/locationNavigatorWidget.php');
require_once(SMC_REAL_PATH.'widgets/publicPostWidget.php');
require_once(SMC_REAL_PATH.'widgets/SMCalx-posts.php');
require_once(SMC_REAL_PATH.'widgets/SMC_Login_Picto.php');
require_once(SMC_REAL_PATH.'shortcodes/shortcodes.php');
require_once(SMC_REAL_PATH.'class/direct-message-manager.php');
require_once(SMC_REAL_PATH.'class/SMC_Achivment.php');
require_once(SMC_REAL_PATH.'tpl/js_admin_voc.php');


/*
add_action('in_admin_header', 'my_get_current_screen');
function my_get_current_screen(){
	$screen_info = get_current_screen();
	
	echo '<pre>';
	print_r($screen_info);
	echo '</pre>';
}
*/
$SMC_Map;
$smc_main_tor_buttons		= array();
$location_hide_types;
$location_map_types			= array(
										
									);

//
register_activation_hook( __FILE__, array( Soling_Metagame_Constructor, 'init_settings' ) );
//register_activation_hook( __FILE__, array( Soling_Metagame_Constructor, 'init_location_types' ) );
register_activation_hook( __FILE__, array( Soling_Metagame_Constructor, 'init_location_template' ) );
//register_activation_hook( __FILE__, array( Instruction_Menager, 		'init_Instructions' ) );
register_activation_hook( __FILE__, array( Direct_Message_Menager, 		'init_settings' ) );

//register_activation_hook( __FILE__, array( Instruction_Menager, 		'add_instruction_menu_items' ) );
//register_activation_hook( __FILE__, array( Master_Menager, 			'insert_master_page' ) );

if (function_exists('register_deactivation_hook'))
{
	register_deactivation_hook(__FILE__, array( 'Soling_Metagame_Constructor',  'smc_deactivation'));
}


class Soling_Metagame_Constructor {

	public $assistants;
	public $location_member;	
	public $options;
	//project_name		= "";
	//admin_optimyze	= true;
	//hide_panel		= true;
	//use_panel			= true;
	//use_location_menu	= true;
	//use_exec			= false;
	//fx_index			= false;
	//smc_height		= 330;
	//map_titles_coords
	
	
    function __construct() 
	{	
		$this->options								= get_option(SMC_ID);		
		$this->assistants							= new Assistants();	
		global $SMC_Location;
		$SMC_Location								= new SMC_Location();
		global $Direct_Message_Menager;
		$Direct_Message_Menager						= new Direct_Message_Menager();	
		global $Master_Menager;
		$Master_Menager								= new Master_Menager();
		global $Intruction_Menager;
		$Intruction_Menager							= new Instruction_Menager();
		
		//
		global $achivment_types;
		$achivment_types							= array();
		//add_action( 'init',							array($this, 'init_achive_types') , 1);
		
		
		global $smc_main_tor_buttons;
		if(!isset($smc_main_tor_buttons))			$smc_main_tor_buttons = array();
		
		global $smc_height;
		$smc_height 								= $this->options['smc_height'];
		//add css style
		add_action( 'admin_enqueue_scripts', 		array($this, 'gg_styles_with_the_lot') );	
		add_action( 'admin_enqueue_scripts', 		array($this, 'gg_scripts_add') );	
		add_action( 'wp_enqueue_scripts', 			array($this, 'add_frons_js_script') );
		add_action( 'wp_head',						array($this, 'hook_css'));	
		add_action( 'ermak_body_script',			array($this, 'ermak_body_script'));	
		
		// Metagame option admin menu
		add_action( 'admin_menu',					array($this, 'metagame_menu') , 1);
		add_action( 'init', 						array($this, 'get_client_version'), 11 );
		add_action('admin_head',					array($this, 'add_column_css'));
				
		if($this->options['use_panel'])
			add_action('wp_footer',					array($this, 'add_location_interface'), 10);
		//
		
		add_action('after_setup_theme',				array($this, 'init_location_template'));
		add_action('after_switch_theme',			array($this, 'init_location_template'));
		
		add_action('wp_ajax_nopriv_myajax-submit',	array($this, 'myajax_submit') );
		add_action('wp_ajax_myajax-submit',			array($this, 'myajax_submit') );
		add_action('wp_ajax_myajax-admin-submit', 	array($this, 'my_action_callback'));
		
		
		add_filter( 'smc_load_panel_screen',		array($this, 'smc_load_panel_screen'), 11, 2 );
		add_action('user_register', 				array($this, 'user_register'));	
		
		add_filter('smc_add_grandchildren_element_to_map', array($this, '_smc_add_grandchildren_element_to_map'), 8, 2);
		
		//add_filter("smc_map_external_0",			array($this, "smc_map_external_0"),11	);
		
		//add_action('init', 							array($this, 'enable_zlib'));
		
		/*
		add_action( 'init', 											array(SMC_Achivment, 'add_achivment'), 17 );
		add_action( 'save_post_smc_achivment',							array(SMC_Achivment, 'true_save_box_data'),13);
		add_action( 'admin_menu',										array(SMC_Achivment, 'my_extra_fields_smc_achivment'));
		add_filter( 'manage_edit-smc_achivment_columns',				array(SMC_Achivment, 'add_views_column'), 4);
		add_filter( 'manage_edit-smc_achivment_sortable_columns', 		array(SMC_Achivment, 'add_views_sortable_column'));
		add_filter( 'manage_smc_achivment_posts_custom_column', 		array(SMC_Achivment, 'fill_views_column'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php
		*/
		
		add_filter( 'plugin_action_links', 			array($this, 'plugin_action_links'), 10, 2 );		
		add_filter( 'smc_main_panel_sidebar', 		array($this, 'smc_main_panel_sidebar'));		
		add_action( 'widgets_init', 				array($this, 'register_my_widgets') );
		add_action( 'admin_notices',				array($this, 'after_install_sticker') , 9);
		add_action( 'wp_ajax_svg_upload',			array($this, 'svg_upload') , 11);
		add_action('wp_ajax_nopriv_svg_upload', 	array($this, 'svg_upload'), 11);
		
		
		global $location_hide_types;
		$location_hide_types					= array(
															'Public Location',
															'Lock this location',
															'Hide this location'
														);
		global $location_map_types;
		$location_map_types						= array(
															"list",
															"map",
															'column'
														);
		
    }
	function get_client_version()
	{
		global $is_mobile;
		require_once(SMC_REAL_PATH."utils/Mobile_Detect.php");						
		$Mobile_Detect 	= new Mobile_Detect();
		$is_mobile		= $Mobile_Detect->isMobile();true;//
	}
	function after_install_sticker()
	{//
		$install_ermak			= get_option('install_ermak');
		if(!($install_ermak==1))	return;
		echo "
		<div class='updated notice smc_notice_ is-dismissible1' id='install_ermak_notice' >
			<p>".
				$this->get_sticker_button().
			"</p>
			<span class='smc_desmiss_button' setting_data='install_ermak'>
				<span class='screen-reader-text'>".__("Close")."</span>
			</span>
		</div>
		
		";
	}
	function get_sticker_button()
	{
		return "
		<div style='position:relative; display:inline-block;'>
			<div class='admin_sticker_block'>
				<h2>". __("Welcome to Ermak", "smc")."</h2>".
				__("Fast and comfortable way to start role play live game.", "smc").
				"<div class='button smc-large-button'>".__("More", "smc")."</div>".
			"</div>
			<div class='admin_sticker_block'>
				<div class='button-primary smc-large-button' id='inloty'>".__("Install predefined Location types", "smc")."</div>
				<div class='button-primary smc-large-button' id='indeme'>".__("Install default menu", "smc")."</div>
				<div class='button-primary smc-large-button' id='indein'>".__("Install default Instructions", "smc")."</div>
				<div class='button smc-large-button' id='momese'>".__("More metagame settings", "smc")."</div>
				
			</div>
			<div class='admin_sticker_block'>
				
			</div>
		</div>
		";
	}
	
	function smc_main_panel_sidebar($arg)
	{
		ob_start();
		dynamic_sidebar('fpmgp');
		$var	= ob_get_contents();
		if($var == "")
		{
			ob_end_clean();
			ob_start();
			$wid		= new SMC_Login_Picto;
			$wid->widget(array(), array("is_illustrated" => false));
			$var		= ob_get_contents();
		}
		ob_end_clean();
		return $args . "<div style=padding-left:14px;>" .$var."</div>";
	}
	function register_my_widgets()
	{
		if ( function_exists('register_sidebar') ) 
		{
				register_sidebar(
					array(
					'name' => __("First page in Metagame Panel","smc"),
					'id' => 'fpmgp',
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h2 class="widgettitle-footer">',
					'after_title' => '</h2>', 
				));			
		}
	}
	function plugin_action_links( $links, $file )
	{
		if ( $file == 'Ermak/Ermak.php'  ) 
		{
			$links[] = '<a href="' . admin_url( 'admin.php?page=metagame' ) . '">'. __("Settings") .'</a>';
		}

		return $links;
	}
	
	function enable_zlib()
	{
		if(is_admin() && !current_user_can("manage_options"))
		{
			 //wp_redirect(get_bloginfo('home'), 301);
		}
		ini_set('zlib.output_compression', 'On');
		ini_set('zlib.output_compression_level', '1');
	}
	function _smc_add_grandchildren_element_to_map($text, $term_id)
	{
		return "";
	}
	
	function default_post_trumbail($post_id)
	{
		global  $Soling_Metagame_Constructor;
		if(!get_post_thumbnail_id($post_id))//post has a Location
		{
			$locs				= wp_get_post_terms( $post_id, SMC_LOCATION_NAME, array("fields" => "ids"));
			if(count($locs)>0)
			{
				$loc_id			= $locs[0];
				global $taxonomy_images_plugin;
				
				$cur_bgnd_id	= $taxonomy_images_plugin->settings[$loc_id];
				//die("cur_bgnd_id = ".$cur_bgnd_id."");
				if($cur_bgnd_id)
				{
					update_post_meta($post_id, '_thumbnail_id', $cur_bgnd_id);
				}
				else
				{
					$cur_lt		= $Soling_Metagame_Constructor->get_location_type($loc_id);
					update_post_meta($post_id, '_thumbnail_id', get_post_thumbnail_id($cur_lt->ID));
				}
			}
		}
	}
	
	//=======================================================
	//
	//	CREATE LOCATION TAXONOMY TEMPLATE FOR CURRENT THEME
	//
	//======================================================
	
	public function init_location_template()
	{		
		if(file_exists(get_template_directory()."/taxonomy-location.php"))		return;
		
		$title_text		= '
		
		$cur_term		= get_term_by("name", single_term_title("",0), SMC_LOCATION_NAME);
		$tax_id 		= $cur_term->term_id;
		$lts			= SMC_Location::get_term_meta($tax_id); // get_option("taxonomy_".$tax_id);
		$lt				= $lts["location_type"];
		$location_type	= get_post($lt);
		echo "<h2>".$location_type->picto. " <span style=\'font-size:0.5em; opacity:0.75;\'>" .$location_type->post_title. "</span> " . $cur_term->name."</h2>";
		echo "<div class=\'location-tamplate-content\'>"; 		
			echo apply_filters("smc_location_tamplate_title", $cur_term);
		echo "</div>";	
		';
		
		$index			= get_template_directory()."/index.php";
		$old			= get_template_directory()."/archive.php";
		$new			= get_template_directory()."/taxonomy-location.php";
		$handle = @fopen($old, "rb");
		if($handle)	
		{
			$contents 		= fread($handle, filesize($old));
			fclose($handle);
			$new_file		= preg_replace("/[if]{1,}[\s]*[(]{1,}[\s]*[have_posts()]{1,}[\s]*[)]{1,}[\s]*[:]{1,}/", 'if (have_posts()) : '.$title_text, $contents);
			file_put_contents($new, $new_file);
		}
		else if($handle = @fopen($index, "rb"))
		{
			$contents 		= fread($handle, filesize($index));
			fclose($handle);
			$new_file		= preg_replace("/[if]{1,}[\s]*[(]{1,}[\s]*[have_posts()]{1,}[\s]*[)]{1,}[\s]*[:]{1,}/", 'if (have_posts()) : '.$title_text, $contents);
			file_put_contents($new, $new_file);
		}
		else
		{
			return;
			
		}
	}
	
	
	
	public function get_location_selecter($params=null)
	{
		$params		= isset($params)	? $params : array("name"=>"location_");
		$args		= array(
								 'number' 		=> 0
								,'offset' 		=> 0
								,'orderby' 		=> 'name'
								,"hide_empty"	=> false
							);
		$owners		= get_terms(SMC_LOCATION_NAME, $args);
		$p			= "<label class='smc_input'><select ";
		if(isset($params['id']))
			$p		.= 'id="'.$params['id'].'" ';
		if(isset($params['name']))
			$p		.= 'name="'.$params['name'].'" ';
		if(isset($params['class']))
			$p		.= 'class="'.$params['class'].'" ';
		if(isset($params['style']))
			$p		.= 'style='.$params['style'].'" ';		
		if(isset($params['multile']))
			$p		.= 'multiple ';	
		if(isset($params['selector']))
		{
			$p		.= $params['selector'] . "=" . $params['selector_name']." ";		
		}		
		if(isset($params['selector2']))
		{
			$p		.= $params['selector2'] . "=" . $params['selector_name2']." ";		
		}
		
		$p			.= '><option value="-1"> -- </option>';
		foreach($owners as $owner)
		{
			$t		= SMC_Location::get_term_meta( $owner->term_id ) ; //get_option("taxonomy_".$owner->term_id);
			$op_id	= $t['location_type'];
			$op		= get_post($op_id);
			$p		.= "<option value='" . $owner->term_id . "' " . selected($owner->term_id, $params['selected'], false) . " >" . $op->post_title. " " . $owner->name. "</option>";
		}
		$p			.= '</select></label>';
		return $p;
	}
	public function wp_drop_location_by_user_owner($params=null, $user_id=-1)
	{
		$params		= isset($params)	? $params : array("name"=>"location_");
		if($user_id == -1)	$user_id 	= get_current_user_id();
		$args		= array(
								 'number' 		=> 0
								,'offset' 		=> 0
								,'orderby' 		=> 'name'
								,"hide_empty"	=> false
							);
		$owners		= get_terms(SMC_LOCATION_NAME, $args);
		$p			= "<select ";
		if(isset($params['id']))
			$p		.= 'id="'.$params['id'].'" ';
		if(isset($params['name']))
			$p		.= 'name="'.$params['name'].'" ';
		if(isset($params['class']))
			$p		.= 'class="'.$params['class'].'" ';
		if(isset($params['style']))
			$p		.= 'style='.$params['style'].'" ';		
		if(isset($params['multile']))
			$p		.= 'multiple ';
		$p			.= '><option value="-1"> -- </option>';
		foreach($owners as $owner)
		{
			if( $this->user_is_owner($owner->term_id, $user_id))
			{
				$t		= SMC_Location::get_term_meta( $owner->term_id ) ;//get_option("taxonomy_".$owner->term_id);
				$op_id	= $t['location_type'];
				$op		= get_post($op_id);
				$p		.= "<option value='" . $owner->term_id . "' " . selected($owner->term_id, $params['selected'], false) . " >" . $op->post_title. " " . $owner->name. "</option>";
			}
		}
		$p			.= '</select>';
		return $p;
	}
	/*	
		return array of term_id
	*/
	public function all_user_locations($user_id=0)
	{
		global $user_location_ids;
		if(isset($user_location_ids) && $user_id==0)
		{
			//return $user_location_ids;
		}
		if($user_id==0)
		{
			$user_id		= get_current_user_id();
		}
		$all_locations		= SMC_Location::get_all_locations();//get_terms(SMC_LOCATION_NAME, array("number"=>120, 'orderby'=>'name', "hide_empty"=>false, "fields" => "ids"));
		$locations			= array();
		foreach($all_locations as $location)
		{
			if($this->user_is_owner($location, $user_id))
			{
				array_push($locations, $location);
			}
		}
		return $locations;
	}
	
	function get_locations($args=-1, $user_id = -1)
	{
		global $UAM;
		if($args==-1)
		{
			$args = array(
				'orderby'       => 'name', 
				'order'         => 'ASC',
				'hide_empty'    => false, 
				'number'        => '', 
				'fields'        => 'all', 
				//'slug'          => '', 
				//'parent'        => '',
				//'child_of'      => 0, 
				//'get'           => '', // ставим all чтобы получить все термины
				//'name__like'    => '',
				//'pad_counts'    => false, 
				'offset'        => '', 
				//'cache_domain'  => SMC_LOCATION_NAME,
				//'childless'     => false, // true не получит (пропустит) термины у которых есть дочерние термины. C 4.2.
				//'update_term_meta_cache' => true, // подгружать метаданные в кэш
			); 
		}
		$all_locations		= get_terms(SMC_LOCATION_NAME, $args);
		if( !$UAM->is_install() )
			return $all_locations;
		if($user_id == -1)
			$user_id = get_current_user_id();
		$locations			= array();
		foreach($all_locations as $location)
		{
			if($UAM->isVisibleLocation($location->term_id))
				$locations[] = $location;
		}
		return $locations;
	}
	function get_locations_id($args=-1, $user_id = -1)
	{
		global $UAM;
		if($args==-1)
		{
			$args = array(
				'orderby'       => 'name', 
				'order'         => 'ASC',
				'hide_empty'    => false, 
				'number'        => '', 
				'fields'        => 'ids', 
			); 
		}
		$all_locations		= get_terms(SMC_LOCATION_NAME, $args);
		if( !$UAM->is_install() )
			return $all_locations;
		if($user_id == -1)
			$user_id = get_current_user_id();
		$locations			= array();
		foreach($all_locations as $location)
		{
			if($UAM->isVisibleLocation($location->term_id))
				$locations[] = $location;
		}
		return $locations;
	}
	
	
	
	
	//=========================================================================================
	/// 
	//	разрешить пользователям видеть только те комменты в админке, которые оставлены к их постам
	//
	//=========================================================================================
	
	function true_get_comments_by_user_posts($clauses) {
		if (is_admin()) {
			global $wpdb, $user_ID;
			$clauses['join'] = ", {$wpdb->prefix}posts";
			$clauses['where'] .= " AND {$wpdb->prefix}posts.post_author = ".$user_ID." AND {$wpdb->prefix}comments.comment_post_ID = {$wpdb->prefix}posts.ID";
		}
		return $clauses;
	}
	
	

	//========================================================
	//
	//	Меню настроек Мета-игры в адмике
	//
	//=========================================================
	
	function metagame_menu() {
		add_menu_page( 
							__('Ermak. Metagame', 'smc'), // title
							__('Ermak. Metagame', 'smc'), // name in menu
							'manage_options', // capabilities
							'metagame', // slug
							array($this, 'metagame_menu_options'), // options function name
							'none',//'dashicons-welcome-learn-more',//SMC_URLPATH .'/img/pin.png', // icon url
							'20.301'
							);
		add_submenu_page( 
							  'metagame'  //or 'options.php' 
							, __('Main Metagame Panel', 'smc')							
							, __('Main Metagame Panel', 'smc')
							, 'manage_options'
							, 'smc_location_menu_sub_menu'
							, array($this,'mc_location_menu_menu_options')
						);
		add_submenu_page( 
							  'metagame'  //or 'options.php' 
							, __('Locations', 'smc')							
							, __('Locations', 'smc')
							, 'manage_options'
							, 'edit-tags.php?taxonomy='.SMC_LOCATION_NAME
						
						);
	}
	
	function metagame_menu_options() 
	{		
		include SMC_REAL_PATH."tpl/Metagame_Setting_form.php";
	}	
	function location_menu_options()
	{
		include SMC_REAL_PATH."tpl/Location_Menu_form.php";
	}	
	function mc_location_menu_menu_options()
	{
		include SMC_REAL_PATH."tpl/Main_Panel_form.php";
	}
		
	// подправим ширину колонки через css
	function add_column_css()
	{
		global $voc;
		echo '<style type="text/css"> 
				.column-title{width:20%;}
				.column-slug{width:100px!important;} 
				.column-id{width:40px;} 
				.column-IDS{width:70px; text-align:center;} 
				.column-use_by_player{width:90px; text-align:center;} 
				.column-picto{width:50px; text-align:center;} 
				.column-image{width:60px;} 
				.column-map_type{width:60px;font-size:11px!important;} 
				.column-hiding_type{width:70px;font-size:11px!important;} 
				.column-creator{width:100px;font-size:11px!important;}
				.column-doo{width:42px;}
				.toplevel_page_metagame, #toplevel_page_metagame.wp-has-current-submenu div.wp-menu-image
				{				
					background-image:url(' . SMC_URLPATH . 'img/ermak.png);
					background-repeat: no-repeat;
					background-position: 8px center;
				}
				.column-events{width:33%;};
				'.
				apply_filters("smc_admin_head_css", "").
			'</style>
			<script>' . 
				apply_filters("smc_admin_head_style", "").
			"</script>
			";
		
		if( $_SERVER['REQUEST_URI'] == '/wp-admin/edit-tags.php?taxonomy='.SMC_LOCATION_NAME)
		{
			echo'
			<style type="text/css">
				#col-right{width:100%;}
				#col-left{display:none;}
			</style>
			';
		}
		/**/	
	}
	
	//========================================================
	//
	//	Создаем типы локаций по умолчанию
	//
	//======================================================
	static function init_location_types()
	{
		require_once(SMC_REAL_PATH.'tpl/register_hook.php');
		pre_init_location_types();		
	}
	
	
	//===================================
	//
	//	FRONTEND LOCATION INTERFACE
	//
	//===================================
	public function get_location_type_id($location_id)
	{
		$ltd					= SMC_Location::get_term_meta( $location_id ) ; 
		return $ltd['location_type'];
	}
	public function get_location_type($location_id)
	{
		$location_type_id		= $this->get_location_type_id($location_id);
		$location_type			= get_post($location_type_id);
		return $location_type;
	}
	
	public function location_has_owners($location_id=0)
	{
		if($location_id == 0)	return false;
		$d						= SMC_Location::get_term_meta( $location_id );
		$ow1					= isset ($d['owner1']) || $d['owner1'] != "";
		$ow2					= isset ($d['owner2']) || $d['owner1'] != "";
		$ow3					= isset ($d['owner3']) || $d['owner1'] != "";
		if($ow1 && $ow2 && $ow3 === false )			return false;
		if($d['owner1'] == -1 && $d['owner2'] == -1 && $d['owner3'] == -1 ) return false;
		if($d['owner1'] == "" && $d['owner2'] == "" && $d['owner3'] == "" ) return false;
		return
			array($d['owner1'], $d['owner2'], $d['owner3']);
		
	}
	
	public function user_is_owner($location_id, $_user_id=-1)
	{
		if ($_user_id == -1) 
		{
			$_user_id	= get_current_user_id();
		}
		$d						= SMC_Location::get_term_meta( $location_id ) ;
		if($d['owner1'] == $_user_id)
			return true;
		if($d['owner2'] == $_user_id)
			return true;
		if($d['owner3'] == $_user_id)
			return true;
			
		return false;
	}
	
	public function cur_user_is_owner($location_id)
	{
		if(!is_user_logged_in()) return false;
		//return user_is_owner($location_id, get_current_user_id());
		$user_id				= get_current_user_id();
		$d						= SMC_Location::get_term_meta( $location_id );
		
		if($d['owner1'] == $user_id)
			return true;
		if($d['owner2'] == $user_id)
			return true;
		if($d['owner3'] == $user_id)
			return true;
			
		return false;
	}
	
	public function goto_to_location ($id)
	{
		return '   href="'. SMC_Location::get_term_link($id).'"    ';
	}
	function get_all_location_types()
	{
		$args = array( 'numberposts' => -1, "post_type" => "location_type", 'orderby'=>'title', 'order'=>'ASC');
		return get_posts($args);
	}
	function get_location_type_names()
	{
		$lts				= $this->get_all_location_types();
		$names				= array();
		foreach($lts as $lt)
		{
			$names[]		= $lt->post_title;
		}
		return $names;
	}
	function get_all_location_type_ids()
	{
		$args = array( 
							'numberposts'	=> -1,
							'offset'    	=> 0, 
							"fields"		=> "ids",
							'post_status'	=> 'publish',
							"post_type"		=> "location_type"
					);
		return get_posts($args);
	}
	public function wp_dropdown_location_type($params="")
	{
		if($params=="")	
			$params = array('id'=>'lts', 'name'=>'lts');
		$args 		= array( 'numberposts' => -1, "post_type" => "location_type", 'order'=>'ASC');
		$lts 		= get_posts($args);
		if($params['id'])
			$id		= "id='".$params['id'] . "' ";
		if($params['name'])
			$name	= "name='".$params['name'] . "' ";
		if($params['class'])
			$class	= "class='".$params['class'] . "' ";
		if($params['style'])
			$style	= "style='".$params['style']. "' ";
		$text		= "<option value='-1'>--</option>";
		foreach( $lts as $lt )
		{		
			$lid	= $lt->ID;
			if(!current_user_can("manage_options") && !get_post_meta($lid, "use_by_player", true))
			{
				continue;
			}
			$text	.= "<option value='".$lid."'>" . $lt->post_title . "</option>";
		}
		return "<select $id $name $style $class >".$text."</select>";
	}
	public function get_location_type_buttons()
	{
		global $post;
		$args = array( 'numberposts' => -1, "post_type" => "location_type", 'order' => 'ASC',);
		$posts = get_posts($args);
		$text		= '<div style="position:absolute; top:0; left:0; width:600px; display:inline-block; border:1px transparent dotted;">
							<div class="lp-title">'.
								'<span>'.__('Choose a Location type','smc').'</span>  <span id="choose_name" style="color:#E73F50;text-transform: uppercase;"></span>'.
							'</div>
							<div id="slider1_container" style="position: relative; top: 0px; left: 50px; width: 480px; height: 95px;color:#CCC; border:blue 1px dotted;">
								<div u="slides" id="sl_cont" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 480px; height: 95px; overflow: hidden;">';
		$tt1 		= '';
		$i			= 0;
		foreach( $posts as $post )
		{
			if($i == 0)
				$tt1 	.= '<div style="">';
			setup_postdata($post);
			if(!current_user_can("manage_options") && get_post_meta(get_the_ID(), "use_by_player", true)!='1')
			{
				continue;
			}
			$tt1		.= '<a class="appicon01 ab'.get_the_ID().'"  style="left:'.($i*120).'px; border:1px #666 solid;" href="javascript:void(0);" onmouseup="choos_new_location( '.get_the_ID().", '".get_the_title()."'".');"> 
								<center title="'.get_the_title().'"">
									<div style="font-size:40px;margin:0px;">'.
										get_post_meta(get_the_ID(), "picto", true).
									'</div>
									<div style="font-size:15px; font-family:Ubuntu Condensed;width:80px;">'.
										get_the_title().
									'</div>	
								</center>
							</a>';
			$i++;
			if ($i<4) 
			{
				null;
			}
			else
			{
				$i 		= 0;
				$tt1	.= "</div>";
			}	
		};
		$tt1			.= "</div>";
		$text 			.= $tt1;
		if($i<5)	$text.='</div></div>';
		return $text.'</div>					
				</div>';
	}
	public function get_location_hiding_name ($location_id, $color='', $is_help=true)
	{
		$d = SMC_Location::get_term_meta( $location_id ); //get_option( "taxonomy_$location_id");
		$help			= $is_help ? " " . $this->assistants->get_hint_helper($this->get_location_hiding_discr($location_id), 100, $color, "float:none!important;"):'';
		switch($d['hiding_type'] )
		{
			case 1:
				return '<div>'.
							__("Lock this location","smc").$help.
						'</div>';
				break;
			case 2:
				return '<div>'.
							__("Hide this location","smc").$help.
						'</div>';
				break;
			case 0:
			default:
				return '<div>'.
							__("Public Location","smc").$help.
						'</div>';
				
		}
	}
	public function get_location_hiding_discr($location_id)
	{
		$d = SMC_Location::get_term_meta( $location_id ); //get_option( "taxonomy_$location_id");
		switch($d['hiding_type'] )
		{
			case 1:
				return __("Only Members can to add new post, but all players can to read and to commented.","smc");
				break;
			case 2:
				return __("Players who havn't membership can't see there in menus, archives ans clouds. Only Members can read and add Posts.","smc");
				break;
			case 0:
			default:
				return __("","smc").'</h3><p>'.__("All Players can read and add Posts.","smc");
				
		}
	}
	
	//=============================
	//
	// DISPLAY LOCATION WINDOW
	//
	//=============================
	
	protected function get_location_rows(
											$cur_location, 			// Location taxonomy
											$cur_lt, 				// Location Type data (post)
											$cur_location_options, 	// Location metadata
											$locations				// parent Locations
										)
	{
		global $UAM;
		$html				= '<div id="location-list-cont" style="">
								<div id="loc_list_slides" u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; overflow: hidden;">';
		foreach($locations as $location)
		{
			$lt				= $this->get_location_type($location->term_id);
			$html			.='<div class="lp-map-row" id="'.$location->term_id.'" style="width:100%; display:inline-block;">
									<span>
										<a href="/?true='.$location->slug.'" class="lp-location-button">
										'.
											$lt->picto." <span class='lp-location-button-desc'>" .$lt->post_title ."</span> ".$location->name. '
										</a>
									</span>'.
									'<span>';
									
									// -- Поиск детей по типу location--
									$tax_id = get_term_by('name', $location->name, SMC_LOCATION_NAME)->term_id;
									$tax_children = get_term_children( $tax_id, SMC_LOCATION_NAME ); 
									$i=0;
									foreach ($tax_children as $child_id) 
									{
										$text											= "";
										$term1 		= SMC_Location::get_instance($child_id);//get_term_by( 'id', $child_id, SMC_LOCATION_NAME );
										$term_link 	= SMC_Location::get_term_link($child_id);//, SMC_LOCATION_NAME);	
										$t_name		= $term1->name;
										$t_parent_id				= $term1->parent;
										$d			= $this->get_location_type($child_id);
										
										if($t_parent_id != $tax_id)	continue;
										$html 					.= '<span>
																		<!--a href="'.$term_link.'"  class="lp-location-child hint  hint--left" data-hint="'. $t_name.'"-->
																		<a href="javascript:void(0);" onclick="open_metagame_panel('."'locations'".', '.$child_id.')" class="lp-location-child hint  hint--left" data-hint="'. $t_name.'">'.
																			$d->picto.
																		'</a>
																	</span>
										</span>';
										if(++$i>2)		break;
									} 
									
									
			$html			.= '		</div>';
		};	
		$html				.= '	</div>
											 <!-- Arrow Left -->
											<span u="arrowleft" class="jssorn08l" style="width: 50px; height: 50px; top: -51px; left: 8px;">
											</span>
											<!-- Arrow Right -->
											<span u="arrowright" class="jssorn08r" style="width: 50px; height: 50px; bottom: -58px; left: 8px">
											</span>
											<!-- Direction Navigator Skin End -->
										</div>';
		
		return $html;
	}
	
	
	protected function get_location_columns(
												$cur_location, 
												$cur_lt, 
												$cur_location_options, 
												$locations
											  )
	{
		global $smc_height;
		$html			= '<div id="lp-location-column-cont" class="lp-location-column-cont" style="height:'.($smc_height - 75).'px;">
								<div id="loc_colmn_slides" u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; height:'.($smc_height - 75).'px; overflow: hidden;">';
		foreach($locations as $location)
		{
			$lt			= $this->get_location_type($location->term_id);
			$html		.= '<div id="loc'.$location->term_id.'" class="lp-location-column" style="background:'.$lt->color.';">'.
								'<div style="margin-top:15px; font-size:30px;">' . $lt->picto . ' </div>
								<div style="font-size:12px;">' . $lt->post_title . '</div>
								<div style="font-weight:bold; margin-bottom:8px;"> ' . $location->name.'</div>
								<div>
									<a  href="/?true='.$location->slug.'"    class="lp-button-2">'.
										__("Goto", "smc").
									'</a>
								</div>';
			// -- Поиск внуков по типу location--
			/**/
			$tax_id = get_term_by('name', $location->name, SMC_LOCATION_NAME)->term_id;
			$tax_children = get_term_children( $tax_id, SMC_LOCATION_NAME ); 
			$ii=0;
			$html			.= '<center class="" style="position:absolute; height:'.($smc_height - 190).'px; display:inline-block;">';
			foreach ($tax_children as $child_id) 
			{
				$text						= "";
				$term1 		= SMC_Location::get_instance($child_id);//get_term_by( 'id', $child_id, SMC_LOCATION_NAME );
				$term_link 	= SMC_Location::get_term_link($child_id);//, SMC_LOCATION_NAME);	
				$t_name		= $term1->name;
				$t_parent_id				= $term1->parent;
				$d			= $this->get_location_type($child_id);
				if($t_parent_id != $tax_id)	continue;
				
				$html		.= '<div parent='.$tax_id.' style="position:absolute; bottom:' . ($ii++ *60) . 'px; left:-60px;">
									<div style="display:inline-block;">
										<a href="javascript:void(0);" onclick="open_metagame_panel('."'locations'".', '.$child_id.')" class="lp-location-child-map hint  hint--right" data-hint="'. $t_name.'" style="background:'.$d->color.'; ">
											'.
												$d->picto.
											'									
										</a>
									</div>	
									<div style="font-size:9px; width:120px; font-family:Verdana, sans-serif;">'.
										$t_name.
									'</div>
								</div>';
			} 
			$html			.= '	</center>
								</div>';
		}
		return 		$html.'		
								</div>
								 <!-- navigator container -->
								<div u="navigator" class="jssorn01" style="position: absolute; bottom: 30px; right: 10px;">
									<!-- navigator item prototype -->
									<div u="prototype" style="POSITION: absolute; WIDTH: 14px; HEIGHT: 14px;"></div>
								</div>
								<!-- Navigator Skin End -->
								
								<!-- Direction Navigator Skin Begin -->			
								<!-- Arrow Left -->
								<span id="arrow-left" u="arrowleft" class="jssord03l" style="width: 55px; height: 55px; top: 10px; left:-65px!important;">
								</span>
								<!-- Arrow Right -->
								<span id="arrow-right" u="arrowright" class="jssord03r" style="width: 55px; height: 55px;">
								</span>
								<!-- Direction Navigator Skin End -->
							</div>';		
	}
	
	function get_map_editor($location_id=0)
	{
		require(SMC_REAL_PATH.'tpl/simple_map_editor.php');
		return $this->simple_map_editor;
	}
	
	public function get_main_map_svg_data()
	{
		global $SMC_Map;
		require(SMC_REAL_PATH.'class/SMC_Map.php');
		$SMC_Map		= new SMC_Map;
		return 			$SMC_Map->get_map_data();
	}
	public function save_main_map_svg_data($data)
	{
		global $SMC_Map;
		require(SMC_REAL_PATH.'class/SMC_Map.php');
		$SMC_Map		= new SMC_Map;
		$SMC_Map->set_map_data($data);
	}
	
	
	public function set_location_map($location_id)
	{
		global $lostlocations;
		global $grandchildren;
		global $UAM;
		global $smc_height;
		global $cities;
		global $Direct_Message_Menager;
		global $map_patterns;
		global $SMC_Map;
		if(	
			!isset($location_id) || 
			$location_id == 'undefined'
		  )	
			$location_id = 0;
		$args				= array(
										'number' 		=> 200
										,'offset' 		=> 0
										,'orderby' 		=> 'slug'
										,'order' 		=> 'ASC'
										,'hide_empty' 	=> false
										,'fields' 		=> 'all'
										,'slug' 		=> ''
										,'hierarchical' => true
										,'name__like' 	=> ''
										,'pad_counts' 	=> false
										,'get' 			=> 'all'
										,'child_of' 	=> 0
										,'parent' 		=> $location_id
									);
									
		$locs				= get_terms(SMC_LOCATION_NAME, $args);
		$locations			= array();
		foreach($locs as $loc)
		{
			if($UAM->isAccessLocation($loc->term_id))
				$locations[] = $loc;
		}
		$cur_location		= SMC_Location::get_instance( $location_id );//get_term_by("term_id", $location_id, 'location');
		$cur_lt				= $this->get_location_type($cur_location->term_id);
		$cur_location_options = SMC_Location::get_term_meta( $location_id ) ;//get_option( "taxonomy_$location_id");
		$is_owner			= $this->cur_user_is_owner($location_id);
		
		if($location_id == 0)
		{
			$map_type		= (int)$this->options['map_type'];			
			$map_data		= $this->get_main_map_svg_data();
		}
		else
		{
			$map_type		= (int)$cur_location_options['map_type'];
			$map_data		= SMC_Location::get_term_meta($location_id);//get_option('taxonomy_map_'. $location_id);
			require_once(SMC_REAL_PATH.'class/SMC_Map.php');
			$SMC_Map		= new SMC_Map;
		}
		//start map generation
		$html				.= 		'<div class="lp-sector-2 lp-wrap-open">
										<div class="lp-head-2 breadcrumb" id="lp-head-2" style="display:none;">'.
											apply_filters("smc_map_breadcrumb", $this->assistants->the_breadcrumb($location_id), $location_id ). 
										'</div>
										<div id="lp-sec-cont-2" class="lp-sector-container">
											<div id="location-container" class="lp-location-container" style="display:none;">
												<div id="location-map-cont" style="">';
		switch($map_type)
		{
			case 0:
				$html			.= $this->get_location_rows(
																$cur_location, 			// Location taxonomy
																$cur_lt, 				// Location Type data (post)
																$cur_location_options, 	// Location metadata
																$locations				// child Locations				
															);
				break;
			case 1:
				$svg_map_cont	.= $SMC_Map->get_location_map(
																$cur_location, 
																$cur_lt, 
																$cur_location_options, 
																$locations,
																$map_data
															 );															
														
				$html				.= '	<div id="svg-map" style="height:'.$smc_height.'px; ">' . 
											'</div >';
				$html				.= '	<div id="externals_0" style="position:absolute; top:0; 	background:transparent; width:100%;">'. 
												@apply_filters("smc_map_external_0", "", $cur_location) .
											'</div>
											<div id="externals_1" style="position:absolute; top:0; 	background:transparent; width:100%;">'. 
												@apply_filters("smc_map_external_1", "", $cur_location) .
											'</div>
											<div id="externals_2" style="position:absolute; top:0; 	background:transparent; width:100%;">'. 
												@apply_filters("smc_map_external_2", "", $cur_location) .
											'</div>
											<div id="externals_3" style="position:absolute; top:0; 	background:transparent; width:100%;">'. 
												@apply_filters("smc_map_external_3", "", $cur_location) .
											'</div>
											<div id="externals_4" style="position:absolute; top:0; 	background:transparent; width:100%;">'. 
												
											'</div>
											<div id="externals_5" style="position:absolute; top:0; 	background:transparent; width:100%;">'. 
												@apply_filters("smc_map_external_5", "", $cur_location) .
											'</div>
											<div id="lostlocations" style="color:red; font-size:20px;position:absolute; top: 10px; right: 20px;">'.
												$lostlocations.
											'</div>
											<div id="externals_6" style="position:absolute; top:0; 	background:transparent; width:100%;">'. 
												
											'</div>';
				/**/
				break;
			case 2:
				$html			.= $this->get_location_columns(
																$cur_location, 
																$cur_lt, 
																$cur_location_options, 
																$locations
															  );
				break;
		}
		
		$html				.=					$grandchildren.
											'</div>
											</div>
											<div id="lp_loc_data">											
											</div>											
										</div>
									</div>';
									
		
		//end map generation
		return array($html, $svg_map_cont, $map_patterns);
	}
	public function set_location($location_id, $params)
	{
		global $lostlocations;
		global $grandchildren;
		global $UAM;
		global $smc_height;
		global $cities;
		global $Direct_Message_Menager;
		global $map_patterns;
		global $taxonomy_images_plugin;
		global $start;
		if($params[2])		$smc_height = $params[2];
		$cur_location		= SMC_Location::get_instance( $location_id ) ; //get_term_by("term_id", $location_id, 'location');
		$cur_lt				= $this->get_location_type($cur_location->term_id);
		$cur_location_options = SMC_Location::get_term_meta( $location_id ) ;//get_option( "taxonomy_$location_id");
		$is_owner			= is_user_logged_in() && $this->cur_user_is_owner($location_id);
		
		$its_your			= $is_owner ? '<div class="smc-your-label-up">'.
												__("It's your", "smc").
											'</div>' : ""; 
											
		$own				= $is_owner ? "<div class=smc-your-label style='right:0px;'>".__("It's your", "smc")."</div>" : "<div class='smc-notyour-label' style='right:0px; '>" . __("It is not your", "smp") . "</div>" . Assistants::get_call_to_owner_hinter($location_id, __("About Location ", "smc") );
										
		$cur_bgnd_id		= $taxonomy_images_plugin->settings[$location_id];
		if($cur_bgnd_id)
		{
			// set a Location trumbail
			$d 				= wp_get_attachment_image_src($cur_bgnd_id, 'large');
			$cur_bgnd		= $d[0];
		}
		else
		{
			// set a trumbail of Location Type
			$d				= wp_get_attachment_image_src(get_post_thumbnail_id($cur_lt->ID), 'large');
			$cur_bgnd		= $d[0];
		}
		if($location_id == 0)
		{
			$pic_id			= $this->options['main_map_pic'];
			if(isset($pic_id))
			{
				//$d			= wp_get_attachment_image_src($pic_id, "full");
				//$cur_bgnd	= $d[0];
				$cur_bgnd	= $pic_id;
			}
			$color			= 'background-color:'.$this->options['main_map_color'].';';
		
		}
		else
		{
			$color			= isset($cur_lt->color) ? 'background-color:'.$cur_lt->color.';' : 'background-color:#009ACD;';
		}
		$bckgrn_style		= $location_id == 0  ? 'style="width:100%; height:100%; '.$color.' background-image:url('.$cur_bgnd.')!important;opacity:1;"' : 'style=" '.$color.' background-image:url('. $cur_bgnd.')!important;"';				
		$html				= '<div class="lp-backgrnd  lp-wrap-open" '.$bckgrn_style.' ></div> 
		<div id="main_location_cont2" style="position:absolute;top:0; left:0;width:100%;">
			<div class="lp-sector-menu lp-cover" id="clapan-left" style="' . $this->get_klapan_bg() . '">
				<div style="position:absolute; top:0; left:0;">
					<svg width="40" height="' . $smc_height . '" xmlns="http://www.w3.org/2000/svg">
						<g>
							<path d="M0,0 L40,0,40,130,0,170,0,0" fill="#000000" fill-opacity="1"></path>
						</g>
					</svg>
					<div id="lp-menu-left" class="lp-menu-button">												
						<i class="fa fa-bars"></i>
					</div>
				</div>
				<center id="content-left" style="-moz-box-sizing:border-box; -webkit-box-sizing:border-box; display:inline-block; margin-left:40px;min-width:200px; width:100%;">
				</center>
			</div>
			<div class="lp-sector-1 lp-wrap-open">
				<div class="lp-head-1 lp-head lp-external-background" id="lp-head-1"  >'.   
					@apply_filters("smc_lp_head_1", "", $location_id).
				'</div>
				<div id="lp-sec-cont-1" class="lp-sector-container" style="overflow:visible!important; ">	
					<div id="cur-loc" style="width:100%; display:none;">';
		
		if($cur_location->name)
		{				
			$html			.= '
			<center style="margin: 20px;">											
				<div style="padding:3px; padding-top:12px; display:block;  background-color:rgba(0, 0, 0, 0.35); ">
					<div class="lp-location-picto">'.																
						$cur_lt->picto.
					'</div>
					<div class="lp-location-title">'.
						$cur_lt->post_title.
					'</div>
					<span class="lp-location-name" style="background:' . $this->options['title_background'] . '">'.
						$cur_location->name.
					'</span>'.
					'<span class="lp-location-hiding">'.	
						$this->get_location_hiding_name($location_id).
					'</span>	
				</div>											
				<div class="lp-location-exec-button-cont">
					<ul class="lp-location-exec-button lp_transparent">';
			if($UAM->is_current_user_member($location_id))											
			{
					$html		.= '
					<li>
						<a class="smc-alert" target_name="add_new_post_message_win" onclick1="open_add_klapan2();" id="post_location_btn">'.
							__("Send post", "smc").
						'</a>
					</li>
					<li>
						<a onclick1="open_add_klapan();" id="add_location_btn" >'.
							__("Create location", "smc").
						'</a>
					</li>
				<ul>';
			}
			$html			.= 			'			</div>
												</center>';
		}
		else
		{
			$html			.= '
			<center style="margin:20px;">
													<span style="color:#000; line-height:1.8; font-size:26px; font-family:Ubuntu Condensed; padding:5px; background:' . $this->options['title_background'] . ';">'.
														$this->options['main_map_name'].
													'</span>';
			if(current_user_can("manage_options"))
			{
				$html			.= '
				<div class="lp-location-exec-button-cont">
					<ul class="lp-location-exec-button lp_transparent"> 
						<li>
							<a href="javascript:void(0);" onclick="open_add_klapan();" id="add_location_btn">'.
								__("Create location", "smc").
							'</a>
						</li>
					<ul>
				</div>';
			}
			$html			.= apply_filters("smc_lp_main_menu", '');
			
			$html			.= 			'		</center>';
		}
		$html				.= 			'	</div>'.	
											$its_your.
										'</div>
									</div>';
		
		
		
		$location_map_data	= $this->set_location_map($location_id);
		$html				.= $location_map_data[2] . $location_map_data[0];
		$html				.=		'<div class="lp-sector-3 lp-wrap-open">
										<div class="lp-head-3" id="lp-head-3">';
		$html				.=	apply_filters("smc_open_location_content_title", $location_id==0 ? "" : "<span class='lp_under_title_klapan'>".$cur_lt->post_title . " " .$cur_lt->picto . " </span>" . $cur_location->name, $location_id);		
		$html				.=	'		</div>
										<div id="lp-sec-cont-3" class="lp-sector-container">
											<div class="lp-location_params">';	
											
		//START LOCATION WIDGET
		
		$ass			= SMC_Location::get_location_widget($location_id);
		$widget			= Assistants::get_switcher(
													apply_filters("open_location_content_widget",  $ass, $params)
												   ) . $own;												
		$html				.= $location_id==0 ? apply_filters("smc_main_panel_sidebar", "") :  $widget;		
		// END LOCATION WIDGET								
										
		$html				.=				'</div>
										</div>						
									</div>									
									<div class="lp-sector-menu-right lp-cover" id="clapan-right" style="' . $this->get_klapan_bg() . '">
										<div id="lp-sector2_svg" style="position:absolute; top:0; left:0px;">	
											
											<svg width="40" height="' . $smc_height . '" xmlns="http://www.w3.org/2000/svg">
												<g>
													<path d="M0,0 L50,0,50,150,0,100,0,0" fill="#000000" fill-opacity="1"></path>
												</g>
											</svg>
										</div>
										<div id="lp-menu-right" class="lp-menu-button">
											<i class="fa fa-bars"></i>
										</div>
										<div id="content-right" style="display:inline-block; margin-left:40px;min-width:200px;width:100%;">
										</div>
									</div>
									<div id="lp-klapan" class="lp-klapan lp-k1 lp-wrap-open" style="'.$this->get_klapan_bg().'">' . 									
										//Assistants::add_new_location_widget($location_id) .
									'</div>
									<div  id="lp-klapan3" class="lp-klapan lp-k2 lp-wrap-open lp-cover" style="' . $this->get_klapan_bg() . '">
										<div class="lp-klapan-title" id="lp-klapan-title">
											
										</div>
										<div id="lp-klapan-body" style="padding:20px;">
										
										</div>
										<div style="top:'.($smc_height/2 - 20).'px;  " id="handler_klapan_3">
											<!--span class="lp-location-btn-directives2" id="lp_goto_clap_btn" style="' . $this->get_klapan_bg() . '">'.
												'<i class="fa fa-chevron-left"></i>'.
											'</span-->
											<span class="lp-location-btn-directives2" id="lp_close_clap_btn" style="' . $this->get_klapan_bg() . '">'.
												'<i class="fa fa-times"></i>'.
											'</span>
										</div>
									</div>
								</div>	';
								/**/
		// send to ajax
		$d					=  array(	
										1, //command id
										array(
												'text' 	=> $html,
												'exec' 	=> 'post_draw',
												'args' 	=> array($location_map_data[1], $this->options['map_titles_coords'], SMC_REAL_PATH.'class/SMC_Map.php'),
												'time'	=>  ( getmicrotime()   - $start ),
												'height'=> $smc_height
											  )
									);
		$d					= apply_filters("smc_ajax_data", $d);
		$d_obj				= json_encode($d);
		print $d_obj;
	}	
	
	function get_klapan_bg()
	{
		return "background-image:url(" . SMC_URLPATH.'/img/'. $this->options['klapan_bckgrnd1'] . ")!important;";
	}
	
	
	
	//=============================
	//
	// DISPLAY CAROUSEL POSTS WINDOW
	//
	//============================
	
	protected function set_carousel($params) 
	{
		
		$html				= '<div class="lp-backgrnd lp-bg2" style="width:100%; height:100%; background:#CDCD00; background-image:url(aa)!important;opacity:1;"></div>
									<div style="position:absolute;top:0; left:0;width:100%;">
										<div class="lp-sector-1 lp-wrap-open">
											<div class="lp-head-1 lp-external-background" id="lp-head-1">'.
												//$params[0].
											'</div>
											<div id="lp-sec-cont-1" class="lp-sector-container">
											</div>
										</div>
										<div class="lp-sector-2 lp-wrap-open">
											<div class="lp-head-2" id="lp-head-2">
											</div>
											<div id="lp-sec-cont-2" class="lp-sector-container">
												<div class="lp-location-container">';
			$html				.=				'</div>
											</div>
										</div>
										<div class="lp-sector-3 lp-wrap-open" style="padding:0!important;">
											<div class="lp-head-3" id="lp-head-3">
												<span class=" lp-instruction">'.
													__("Сюда попадают все фото и видео-посты","smc").
									'			</span>
											</div>
											<div id="lp-sec-cont-3" class="lp-sector-container">
											</div>						
										</div>
									</div>	';
			//$html				.= get_slide_show($this->options, $this->get_iface_color(), $params);
			require_once (SMC_REAL_PATH."tpl/slide_show.php");
			//require(SMC_REAL_PATH."tpl/slide-show-del3.php");
			$d					=  array(	
											1, //command id
											array(
													'text' => $html,
													'exec' => 'start_slide_show_jssor3',
													'args' => (int)($smc_height/75),
													'time'	=>  ( getmicrotime()   - $start )
												  )
										);
			$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
			echo $d_obj;
	}
	
	//==============================
	//
	// DISPLAY ACCOUNT WINDOW
	//
	//==============================
	protected function get_element_size($width)
	{		
		global $smc_height;	
		global $www;	
		global $www2;	
		global $marg;	
		$www				= min(($smc_height - 50)/2, ($width - 0)/3);
		$marg				= 2;
		$www2				= ($www/2 - $marg);
	}
	protected function account_form()
	{
		global $smc_height;	
		global $www;	
		global $www2;	
		global $marg;	
		$html			= '<div style="position:absolute; top:'. ($www2/2 - 40) .'px; width:100%; padding-left:20px; text-align:left;">';
		if(is_user_logged_in())
		{
			$_user		= wp_get_current_user();
			$html		.=	__("Hallo", "smc").', <span style="font-weight:bold;">'.
								$_user->display_name .' ' . 
							'</span>
							
							<div style="margin-top:2px;">
								<a href="'.wp_logout_url().'">
									<span class="lp-location-exec-button">'.__('log out', 'smc').'</span>
								</a>
								<a href="'.get_admin_url().'">
									<span class="lp-location-exec-button">'.__('Admin', 'smc').'</span>
								</a>
							</div>';			
		}
		else
		{
			$html		.= '<div>'.
								'<a href=""  style="position:absolute; width:' . $www2 . 'px; left:-' . $www2 . 'px; color:red; text-align:center; top:12px; font-size:20px; font-weight:bold;">'.__('Enter', 'smc').'</a>'.
							'</div>
							<div id="login-buttons" style="margin-top:15px;">
								<a href="javascript:void(0);" onclick="get_login_form();">
									<span class="lp-location-exec-button">'.__('Login', 'smc').'</span>
								</a>
								<a href="javascript:void(0);" onclick="get_register_form();">
									<span class="lp-location-exec-button">'.__('Register', 'smc').'</span>
								</a>
							</div>
							<div id="login-form" style="display:none;position:absolute; top:0">
								<form  name="logining" method="post"  enctype="multipart/form-data" id="logining">
									<input type="text" id="login" name="login" style="width:'. ($www2 * 3 - 50).'px;" placeholder="'.__("Enter Account", 'smc').'" value="lea"/>
									<p></p>
									<input type="text" id="password" name="password" style="width:'. ($www2 * 3 - 50).'px;" placeholder="'.__("Enter Password", 'smc').'"value="zaq12ws"/>
									<div style="position:absolute; top:' . $www2 . 'px">
										<a href="javascript:void(0);" onclick="get_autent();">
											<span class="lp-location-exec-button">'.__('Enter', 'smc').'</span>
										</a>										
									</div>
								</form>
							</div>';
		}
		return $html.'</div>';
	}
	protected function get_avatar_user()
	{	
		global $www2;		
		return (is_user_logged_in())  ? get_avatar( get_current_user_id(),  $www2 - 26) : "";		
	}
	protected function get_font_size()
	{
		global $www;
		switch(true)
		{
			case ($www>275):
				return 20;
			case ($www>225):
				return 18;
			case($www>175):
				return 16;
			default:
				return 14;
		}
	}
	protected function set_account($params)
	{
		
		global $post;
		global $posts;
		global $wp_query;
		global $smc_height;	
		global $www;	
		global $www2;	
		global $marg;
		global $Master_Menager;		
		$w					= 70;	
		$this->get_element_size($params[15]/2 - 50);
		$wp_query = new WP_Query(array(
									'posts_per_page'	=> 0,
									'paged' 			=> get_query_var( 'paged' ),
									'orderby'			=> 'post_date',
									'order'				=> 'DESC',
									'post_type'			=> 'direct_message',
									'meta_query' 		=> array(
																	array(
																			'key' => 'enabled',
																			'value' => 1,
																			'compare' => '='
																		),
																	array(
																			'key' => 'addressee',
																			'value' => get_current_user_id()
																		)
																)
								));
		$messages					= "<div>";
		$i							= 0;
		while ( $wp_query->have_posts() ) 
		{
			$wp_query->the_post();
			
			$source_type			= get_post_meta(get_the_ID(), "source_type", true);
			$viewed					= get_post_meta(get_the_ID(), "viewed", true);
			$logo					= $viewed == '1' ? '<i class="fa fa-envelope-o"></i>':'<i class="fa fa-envelope"></i>';
			$reset					= $viewed == '1' ? ' lp-viewed ' : '';
			global $Direct_mesage_autor_types;
			$author					= get_the_author();
			if($source_type != 3)
			{
				$author 			= __($Direct_mesage_autor_types[$source_type], "smc");
			}
			$messages				.= "	<div class='lp-meggase-title-container'>
											<a id=".get_the_ID()." href='javascript:void(0);' onclick='send_direct_message(".get_the_ID().");'  class='lp-direct_message-title $reset'>".
												" <span class='lp-message-autor'> ".$logo. " " .$author. ": </span> ".get_the_title()."<span class='lp-message-date $reset'>".get_the_time('j M, Y', get_the_ID())."</span>".
									"		</a>
										</div>";
			$i++;
		}
		if($i==0)
		$messages					.= '<div class="lp-you-losser">'.__("You haven't Direct Messages now","smc").'</div>';
		$messages					.= "
									</div>";
									
		//new d message win
		$new_dm				=		'<style>
										.lp-my-location
										{
											display:inline-block; 
											border:1px solid transparent; 
											background:'. $this->options['fills'][1][0].'; 
											max-width:100%; 
											height:30px; 
											overflow:hidden; 
											text-align:left; 
											padding:5px;
											margin:4px;
										}
										.lp-my-location:hover
										{
											background:'. $this->options['fills'][1][1].'; 
											color:white!important;
										}
										.lp-my-location-select
										{
											background:'. $this->options['fills'][0][1].'; 
										}
										.lp-black-btn
										{
											opacity:.75;
											background:rgba(0,0,0,0.25); 
											width:100%; 
											height:100%;
											cursor:pointer;
										}
										.lp-black-btn:hover
										{
											background:transparent;
											opacity:1;
										}
										.lp-black-btn:active
										{
											background:'.$this->get_iface_color().';
										}
										.lp-menu-person-title
										{
											position:absolute; 
											top:13px; 
											width:100%;
										}
										.lp-menu-element
										{
											width:'.$www.'px; 
											height:'.$www.'px; 
											color:white; 
											font-family:Open Sans Condensed; 
											font-weight:normal; 
											font-size:'.$this->get_font_size().'px;
											position:absolute; 
											top:0; 
											left:0; 
											text-align:center;
											margin:4px; 
											float:left;
										}
									</style>		
									<div class="lp-sector-menu lp-cover" id="clapan-left" style="' . $this->get_klapan_bg() . '">
										<span id="lp-menu-left" class="lp-menu-button">
											<i class="fa fa-bars"></i>
										</span>
									</div>
									<div class="lp-sector-1 lp-wrap-open">
										<div class="lp-head-1 lp-external-background" id="lp-head-1">'.
											__("add new Direct Message", "smc").
										'</div>
									</div>
									<div class="lp-sector-2 lp-wrap-open">
										<div class="lp-head-2" id="lp-head-2"></div>
										<div id="lp-sec-cont-2" class="lp-sector-container">
											<div class="lp-new-message-container">
												<form name="new_post" method="post"  enctype="multipart/form-data" id="new_post">
													
													<input type="text" size=50 placeholder ="'.__('message title', 'smc').'" maxlengt="30" id="new_post_title" style="width:100%; margin-right:5px;"/>
													<p>
													
													<textarea rows="5" placeholder ="'.__('Direct Message', 'smc').'" maxlengt="140" id="new_post_text" style="height:100px; width:100%; margin-right:5px;"></textarea>
													<p>
													<div style=" display:inline-block; float:left; width:50%;">
														<label for="addressee">'.__('addressee', 'smc').' </label>
														<p>'.
														wp_dropdown_users(
																			array(
																					'show_option_none'  => "---",
																					'echo'				=> false,
																					'show'				=> 'display_name',
																					'name'				=> 'addressee',
																					'multi'             => true,
																				  )
																		  )./**/
										'			</div>
													<div style="margin-top:20px;">
														<a href="javasctipt:void(0);" onclick="submit_new_message();" id="new_message_submit" class="lp-direct-message-button" style="width:50%; max-width:50%;">'.
															__("Submit").
														'</a>
															
													</div>'.
													//wp_nonce_field( 'new-direct-message' ).
												'</form>
											</div>
										</div>						
									</div>
									<div class="lp-sector-3 lp-wrap-open">
										<div class="lp-head-3">
										</div>
									</div>';
									
		// dm list
		$dm_list			= $this->set_carousel_form(	
														__("Direct Messages","smc"),														
														'<div class="lp-messages-container">'.
															$messages.
															'<div id="pagination" style="">'.														
																//$this->assistants->kama_pagenavi(" prev ", " next ",false).
															'</div>
														</div>',
														"",
														"",
														"",
														"",
														2
													);	
		// get all user's Locations
		$all_locations		= get_terms(SMC_LOCATION_NAME, array("number"=>120, 'orderby'=>'name', "hide_empty"=>false));
		$locations			= array();
		
		//my locations menu
		$locs				= '	<div id="my_locs_cont" style="position: relative; top: 50px; left: 0; width: 220px; height: 250px;">
									<div u="slides" id= "my_locs" style="cursor: move;  overflow: hidden; text-align:left; text-size:16px; position:absolute; top:0; left:0; margin-top:15px;">';
		$id					= 0;
		foreach($all_locations as $location)
		{
			if($this->cur_user_is_owner($location->term_id))
			{
				$locations[]	= $location;
				$cur_lt			= $this->get_location_type($location->term_id);
				$locs			.=	'<span  id="loc'.$id.'" class="lp-my-loc lp-my-location">
										<a href="javascript:void(0);" onclick="choose_my_location(' . $id++ . ', ' . $location->term_id . ');" class="">
											<span class="lp-location-picto" style="font-size:19px;">'.$cur_lt->picto.'</span>
											<span style="font-size:9px; font-family:Verdana, sans-serif;">	'.
												
											'</span>
											<span class="lp-location-title" style="">'.$location->name.'</span>
										</a>
									</span>';
			}
		}
		if($id==0)
		{
			$locs				.= "<div class='lp-you-losser'>".__("You haven't Locations now", "smc")."</div>";
		}
		$locs					.='		</div><?-- my_locs -->
										 <!-- Arrow Left -->
										<span u="arrowleft" class="jssorn08l" style="width: 50px; height: 50px; top: -40px; left: 8px;">
										</span>
										<!-- Arrow Right -->
										<span u="arrowright" class="jssorn08r" style="width: 50px; height: 50px; bottom: -70px; left: 8px">
										</span>
										<!-- Direction Navigator Skin End -->
									</div><?-- my_locs_cont -->';	
		//end of my locations menu
		
		$loc_setting_menu		= '	<div id="save_my_location" style="margin-left:7%; border-bottom:rgba(0,0,0,0.2); display:none;">
										<a id="bt0" loc="0" class="lp-location-setting-menu lp-current">'.__("Settings", "smc").'</a>
										<a id="bt1" loc="1" class="lp-location-setting-menu">'.__("Owners", "smc").'</a>
										<a id="bt2" loc="2" class="lp-location-setting-menu">'.__("List of Members", "smc").'</a>
									</div>';
		
		$menushechka			= '	<center>
										<div  class="lp-menu-element" style="opacity:1!important; background:white; width:'. $www2 .'px; height:'. $www2.'px; top:'.($www+$marg*1).'px; left:'.($www+$marg).'px;">'.
											'<div class="lp-black-btn" style="opacity:1!important; background:white;">
												<div class="lp-menu-person-title" style="margin:0 1px;">'.
													$this->get_avatar_user().
												'</div>
											</div>
										</div>
										<div  class="lp-menu-element" style="opacity:1!important; background:#333; width:'. ($www2 + $www + $marg) .'px; height:'. $www2.'px; top:'.($www+$marg*1).'px; left:'.($www + $www2 + 2 * $marg).'px;">'.
											'<div class="lp-black-btn" style="">
											<div class="lp-menu-person-title" style="margin:0 1px;">'.
												$this->account_form().
											'</div></div>'.
										'</div>';
		if(is_user_logged_in())
		{
			$menushechka		.=	'	<div class="lp-menu-element" style="background:'.$this->options['fills'][0][0].';">'.
											'<div class="lp-black-btn" slide_id="1">
											<div class="lp-menu-person-title">'.__("Direct Messages", "smc").'</div>
											<div style="position:absolute;bottom:35%; width:'.$www.'px; font-size:48px;">
												<i class="fa fa-folder-open"></i>
											</div></div>'.
										'</div>
										<div  class="lp-menu-element" style="background:'.$this->options['fills'][1][0].'; left:'.($www+$marg).'px;">'.
											'<div class="lp-black-btn"  slide_id="2" style="">
											<div class="lp-menu-person-title">'.
												__("add new Direct Message", "smc").'
											</div>
											<div style="position:absolute; bottom:35%; width:'.$www.'px; font-size:48px;">
												<i class="fa fa-edit"></i>
											</div></div>'.
										'</div>
										<div  class="lp-menu-element" style="background:'.$this->options['fills'][2][0].'; top:'.($www+$marg).'px;">'.
											'<div class="lp-black-btn"  slide_id="3" style="">
												<div class="lp-menu-person-title">'.
													__("My Locations", "smc").'
												</div>
												<div style="position:absolute; bottom:35%; width:'.$www.'px; font-size:48px;">
													<i class="fa fa-globe"></i>
												</div>
											</div>
										</div>';
		
		/*
		Образцы кнопки, которую надо заменять фильтром 	smc_personal_button	
		
		$obrazes0					= '	
										<div  class="lp-menu-element" style="background:'.$this->options['fills'][31][0].'; width:'. $www2 .'px; height:'. $www2 .'px; left:'.(($www+$marg) * 2) .'px;">
											<div class="lp-black-btn" slide_id="4" style="">
												<div class="lp-menu-person-title">'.
													__("Call a Master", "smc").
												'</div>
												<div style="position:absolute; bottom:15%; width:'.$www2.'px; font-size:24px;">
													<i class="fa fa-coffee"></i>
												</div>	
											</div>
										</div>';
										
		$obrazes1					= '
										<div  class="lp-menu-element" style="background:'.$this->options['fills'][51][0].'; width:'. $www2 .'px; height:'. $www2 .'px; left:'.(($www + $marg) * 2 + $www/2) .'px;">'.
											'<div class="lp-black-btn" slide_id="5" style="">
												<div class="lp-menu-person-title">'.
													__('Bank', 'smc').
												'</div>
												<div style="position:absolute; bottom:15%; width:'.$www2.'px; font-size:24px;">
													<i class="fa fa-money"></i>
												</div>	
											</div>
										</div>'';
		*/
		
		$menushechka1				= '
										<div  class="lp-menu-element" style="background:'.$this->options['fills'][31][0].'; width:'. $www2 .'px; height:'. $www2 .'px; left:'.(($www+$marg) * 2) .'px;">
											<div class="lp-black-btn" style="">
												
											</div>
										</div>';
										
		$menushechka2				= '
										<div  class="lp-menu-element" style="background:'.$this->options['fills'][51][0].'; width:'. $www2 .'px; height:'. $www2 .'px; left:'.(($www + $marg) * 2 + $www/2) .'px;">'.
											'<div class="lp-black-btn" style="">
												
											</div>
										</div>';
		
		$m1							= apply_filters("smc_personal_button", "ddd");	
		$menushechka				.= !isset($m1) ? $m1 : $menushechka1;	
		$m2							= apply_filters("smc_personal_button", "ddd");	
		$menushechka				.= !isset($m2) ? $m2 : $menushechka2;							
		
		/**/
		$menushechka				.='
										<div  class="lp-menu-element" style="background:'.$this->options['fills'][61][0].'; width:'. $www2 .'px; height:'. $www2 .'px; top:'.($www/2 + $marg).'px; left:'.(($www+$marg) * 2) .'px;">'.
											'<div class="lp-black-btn" slide_id="6" style="">
											<div class="lp-menu-person-title">'.
											
											'</div></div>'.
										'</div>
										<div  class="lp-menu-element" style="background:'.$this->options['fills'][71][0].'; width:'. $www2 .'px; height:'. $www2 .'px; top:'.($www/2 + $marg).'px; left:'.(($www + $marg) * 2 + $www/2) .'px;">'.
											'<div class="lp-black-btn" slide_id="7" style="">
												<div class="lp-menu-person-title">'.
												
												'</div>
											</div>'.
										'</div>
										<div  class="lp-menu-element" style="background:'.$this->options['fills'][71][0].'; width:'. $www2 .'px; height:'. $www2 .'px; top:'.($www + $www2 + $marg*3).'px; left:'.($www+$marg).'px;">'.
											'<div class="lp-black-btn" slide_id="8" style="">
												<div class="lp-menu-person-title">'.
												
												'</div>
											</div>
										</div>
										<div  class="lp-menu-element" style="background:'.$this->options['fills'][71][0].'; width:'. $www2 .'px; height:'. $www2 .'px; top:'.($www + $www2 + $marg*3).'px; left:'.($www+$www2+$marg*2).'px;">'.
											'<div class="lp-black-btn" slide_id="9" style="">
												<div class="lp-menu-person-title">'.
												
												'</div>
											</div>
										</div>
										<div  class="lp-menu-element" style="background:'.$this->options['fills'][71][0].'; width:'. $www2 .'px; height:'. $www2 .'px; top:'.($www + $www2 + $marg*3).'px; left:'.($www*2+$marg*1).'px;">'.
											'<div class="lp-black-btn" slide_id="10" style="">
												<div class="lp-menu-person-title">'.
												
												'</div>
											</div>
										</div>
										<div  class="lp-menu-element" style="background:'.$this->options['fills'][71][0].'; width:'. $www2 .'px; height:'. $www2 .'px; top:'.($www + $www2 + $marg*3).'px; left:'.($www*2+$www2+$marg*2).'px;">'.
											'<div class="lp-black-btn" slide_id="11" style="">
												<div class="lp-menu-person-title">'.
												
												'</div>
											</div>
										</div>
									</center>';
		}
		else
		{
			$menushechka		.=	'	<div class="lp-menu-element" style="">'.
											'<div class="lp-black-btn" ></div>											
										</div>
										<div  class="lp-menu-element" style="left:'.($www+$marg).'px;">'.
											'<div class="lp-black-btn" style="">
												<div id="login-comment" style="position:absolute; bottom:5px;left:5px;">
													
												</div>
											</div>
										</div>
										<div  class="lp-menu-element" style="top:'.($www+$marg).'px;">'.
											'<div class="lp-black-btn" style=""></div>
										</div>
										<div  class="lp-menu-element" style="width:'. $www2 .'px; height:'. $www2 .'px; left:'.(($www+$marg) * 2) .'px;">'.
											'<div class="lp-black-btn" style=""></div>
										</div>'.
										'<div  class="lp-menu-element" style="width:'. $www2 .'px; height:'. $www2 .'px; left:'.(($www + $marg) * 2 + $www/2) .'px;">'.
											'<div class="lp-black-btn" style=""></div>
										</div>
										<div  class="lp-menu-element" style="width:'. $www2 .'px; height:'. $www2 .'px; top:'.($www/2 + $marg*0).'px; left:'.(($www+$marg) * 2) .'px;">'.
											'<div class="lp-black-btn" style=""></div>
										</div>
										<div  class="lp-menu-element" style="width:'. $www2 .'px; height:'. $www2 .'px; top:'.($www/2 + $marg*0).'px; left:'.(($www + $marg) * 2 + $www/2) .'px;">'.
											'<div class="lp-black-btn" style=""></div>
										</div>
										<div  class="lp-menu-element" style="width:'. $www2 .'px; height:'. $www2 .'px; top:'.($www + $www2 + $marg*2).'px; left:'.($www+$marg).'px;">'.
											'<div class="lp-black-btn" style=""></div>
										</div>
										<!--div  class="lp-menu-element" style="width:'. $www2 .'px; height:'. $www2 .'px; top:'.($www + $www2 + $marg*2).'px; left:'.($www+$www2+$marg*2).'px;">'.
											'<div class="lp-black-btn" style=""></div>
										</div-->
										<div  class="lp-menu-element" style="width:'. $www2 .'px; height:'. $www2 .'px; top:'.($www + $www2 + $marg*2).'px; left:'.($www*2+$marg*1).'px;">'.
											'<div class="lp-black-btn" style=""></div>
										</div>
										<div  class="lp-menu-element" style="width:'. $www2 .'px; height:'. $www2 .'px; top:'.($www + $www2 + $marg*2).'px; left:'.($www*2+$www2+$marg*2).'px;">'.
											'<div class="lp-black-btn" style=""></div>
										</div>
									</center>';
		}
		//Masters lists
		$masters			= '	<div id="my_masters_cont" class="lp-transform-300" style="position: relative; top: 4px; left: 4px; width: 700px; height:410px;overflow: hidden;">
									<div u="slides" id= "my_masters" style="cursor: move;  overflow: hidden; width: 700px; height: 410px; text-align:left; text-size:16px; position:absolute; top:0; left:0; ">';
		$id					= 0;
		$msts				= get_posts(array(
												'numberposts'     => -1,
												'offset'          => 0,
												'orderby'         => 'id',
												'order'           => 'DESC',
												'include'         => '',
												'exclude'         => '',
												'meta_key'        => '',
												'meta_value'      => '',
												'post_type'       => 'master',
												'post_mime_type'  => '',
												'post_parent'     => '',
												'post_status'     => 'publish'
											));
		$masters		.= '<div id="masters_cont" style="width:100%; overflow:visible;border:1px dotted #000;">';
		foreach($msts as $mst)
		{
			$masters		.= $Master_Menager->draw_master($mst, $id++, false);
		}
		
		if($id==0)
		{
			$masters		.= "<div class='lp-you-losser'>".__("You haven't Locations now", "smc")."</div>";
		}
		$masters			.='		</div></div><?-- my_locs -->
										 <!-- Arrow Left -->
										<span u="arrowleft" class="jssorn08l" style="width: 50px; height: 50px; top: -40px; left: 8px;">
										</span>
										<!-- Arrow Right -->
										<span u="arrowright" class="jssorn08r" style="width: 50px; height: 50px; bottom: -70px; left: 8px">
										</span>
										<!-- Direction Navigator Skin End -->
									</div><?-- my_masters_cont -->';	
									
		$my_locations	= $this->set_carousel_form(	
														__("My Locations", "smc"),
														$locs,
														$loc_setting_menu,
														"",
														"",
														"",
														3
													);			
		$my_settings	= $this->set_carousel_form(	
														__('Personal Settings','smc'),
														"",
														"",
														$menushechka,
														"",
														"",
														4
													);			
		$my_masters	= $this->set_carousel_form(	
														__('Masters','smc'),
														"",
														"",
														$masters,
														"",
														"",
														5
													);		
								
		$html			=	'<div class="lp-backgrnd lp-bg3" style="width:100%; height:100%; background:#CDAF95; background-image:url(aa)!important;opacity:1;"></div>
								<div style="position:absolute;top:-30px; left:0;width:100%;">
									<div class="lp-slider-cont" id="slider1_container" style="position: relative; top: 30px; left: '.$w.'px; width: ' . ($params[15]-$w) . 'px;   height: '. $smc_height . 'px; background:transparent;">
										 <!-- Loading Screen >
										<div u="loading" style="position: absolute; top: 0px; left: 0px;">
											<div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block; background-color: transparent; top: 0px; left: 0px;width: 100%;height:100%;">
											</div>
											<div style="position: absolute; display: block; background: url(wp-content/plugins/Ermak/img/loading.gif) no-repeat center center;
												top: 0px; left: 0px;width: 100%;height:100%;">
											</div>
										</div-->
										
										<!-- Slides Container -->
										<div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: ' . ($params[15]-$w) . 'px; height: '. $smc_height . 'px; overflow: hidden;">
											<!-- Next Window -->
											<div class="" style="background:transparent; height:100%">
												<div u="thumb">
													<div class="i" style="background-image:url(' . SMC_URLPATH . '/img/key_icon.png)!important;"></div>													
												</div>	
												<div>'.
													$my_settings.
												'</div>
											</div>';
		//if(is_user_logged_in())
		{		
			$html			.= 				'
											<!-- Direct Message List and Read Client -->
											<div class="jssor-bck" style="background:transparent; height:100%">
												<div>'.
													$dm_list.												
												'</div>
												<div u="thumb">
													<div class="i" style="background-image:url(' . SMC_URLPATH . '/img/add_user_icon.png)!important;"></div>	
												</div>	
											</div>
											<!-- New Direct Message Window -->
											<div class="" style="background:transparent; height:100%">
												<div u="thumb">
													<div class="i" style="background-image:url(' . SMC_URLPATH . '/img/pencil_icon.png)!important;"></div>													
												</div>
												<div>'.
													$new_dm.
												'</div>
											</div>
											<!-- Locations Window -->
											<div class="" style="background:transparent; height:100%">
												<div>'.
													$my_locations.
												'</div>
												<div u="thumb">
													<div class="i" style="background-image:url(' . SMC_URLPATH . '/img/location_icon.png)!important;"></div>													
												</div>	
											</div>
											<!-- Masters Window -->
											<div class="" style="background:transparent; height:100%">
												<div>'.
													$my_masters.
												'</div>
												<div u="thumb">
													<div class="i" style="background-image:url(' . SMC_URLPATH . '/img/helper_icon.png)!important;"></div>													
												</div>	
											</div>';
		}
		$html			.= 				'</div>
										<!-- ThumbnailNavigator Skin Begin-->
										<div u="thumbnavigator" class="jssort11" style="position: absolute; width: '.$w.'px; height: '. ($smc_height -30 ). 'px; left:-'.$w.'px; top:10px;">
											<div u="slides" style="cursor: pointer;">
												<div u="prototype" class="p" style="position: absolute; width: 70px; height: 70px; top: 0; left: 0;">
													
													<thumbnailtemplate style=" width: 100%; height: 100%; border: none;position:absolute; top: 0; left: 0;">
													</thumbnailtemplate>
												</div>
											</div>
										</div >
										<!-- Thumbnail Item Skin End -->
									</div>
								
							</div>';	
		
		wp_reset_postdata();
		$d					=  array(	
										1, //command id
										array(
												'text'	=> $html,
												'exec'	=> 'location_slideshow',
												'args'	=> 'account',
												'time'	=> ( getmicrotime()   - $start )
											  )
									);
		$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
		print $d_obj;
		
	}
	
	function set_carousel_form(
									$title1 = "",
									$content1 = "",
									$title2 = "",
									$content2 = "",
									$title3 = "",
									$content3 = "",
									$id = 0				// lp-sector-1-{$id}, lp-head-1-{$id}, lp-contents-1-{$id}
								)
	{
		return			 '		<div class="lp-sector-menu lp-cover" id="clapan-left" style="' . $this->get_klapan_bg() . '">
									<span id="lp-menu-left" uid="'.$id.'" class="lp-menu-button">
										<i class="fa fa-bars"></i>
									</span>
								</div>
								<div id="lp-sector-1-'.$id.'" class="lp-sector-1 lp-wrap-open">
									<div  id="lp-head-1-'.$id.'" class="lp-head-1 lp-external-background">'.
										$title1.//
									'</div>
									<div  id="lp-contents-1-'.$id.'" class="lp-sector-container lp-scroll-content">'.
										$content1.
									'</div>
								</div>	
								<div  id="lp-sector-2-'.$id.'" class="lp-sector-2 lp-wrap-open">
									<div  id="lp-head-2-'.$id.'" class="lp-head-2">'.
										$title2.
									'</div>
									<div  id="lp-contents-2-'.$id.'" class="lp-sector-container lp-scroll-content">'.
										$content2.
									'</div>
								</div>							
								<div  id="lp-sector-3-'.$id.'" class="lp-sector-3 lp-wrap-open">
									<div  id="lp-head-3-'.$id.'" class="lp-head-3">'.
										$title3.
									'</div>
									<div  id="lp-contents-3-'.$id.'" class="lp-sector-container lp-scroll-content">'.
										$content3.
									'</div>
								</div>																
								<div class="lp-sector-menu-right lp-cover" id="clapan-right" style="' . $this->get_klapan_bg() . '">
									<span id="lp-menu-right" uid="'.$id.'" class="lp-menu-button">
										<i class="fa fa-bars"></i>
									</span>
								</div>';
	}
	
	function choose_my_location($params)
	{
		global $UAM;
		$my_location		= SMC_Location::get_instance($params[1]);
		$my_lt				= $this->get_location_type($params[1]);
		$l_params			= SMC_Location::get_term_meta( $params[1] );
		
		
		
		$hiding				= '	<div  class="lp-floater" style="margin-left:20px;">
									<center style="left:5px 0;">
										<input type="radio" name="hide" id="is_locked1" value=0  class="css-checkbox" '.checked( $l_params['hiding_type'], 0, false ).' />
										<label for="is_locked1" class="css-label label-selected">'.__('Public Location', 'smc').'</label>
									 
										<input type="radio" name="hide" id="is_locked2" value=1 class="css-checkbox"  '.checked( $l_params['hiding_type'], 1, false ).' />
										<label for="is_locked2" class="css-label">'.__('Lock this location', 'smc').'</label>
																							
										<input type="radio" name="hide" id="is_locked3" value=2 class="css-checkbox"  '.checked( $l_params['hiding_type'], 2, false ).' />
										<label for="is_locked3" class="css-label ">'.__('Hide this location','smc').'</label>
									</center>
								</div>';
		$settings			= '				<div style="height:'.$heiht_1.'px; display:inline-block; margin-top:10%;>
												<center style="font-family:Arial, Sans-serif; ">
													<div class="lp-abzaz" style="width:100%;font-family:Arial, sans-serif;font-size:16px;">'.
														$my_lt->picto. " " . $my_lt->post_title.
													'</div>
													<center class="lp-abzaz" style=" display:inline-block;">
														<input type="text" maxlength="50" name="location_name" value="' . $my_location->name . '" style="font-size:23px; text-align:center; width:'.($params[15]/2 - 100).'px;"/>
													</center>'.
													$hiding.
												'</center>
											</div>';
		$owners				= '<center style="display:inline-block; margin-top:10%;">';
		$owners				.= '<div style="float:left; padding:3px;">'.
									wp_dropdown_users(
																	array(
																			'show_option_none'  => "---",
																			'echo'				=> false,
																			'show'				=> 'display_name',
																			'name'				=> 'owner1',
																			'multi'             => true,
																			'selected'			=> $l_params ['owner1']
																		  )
																  ).
								'</div>';
		$owners				.= '<div style="float:left; padding:3px;">'.
									wp_dropdown_users(
																	array(
																			'show_option_none'  => "---",
																			'echo'				=> false,
																			'show'				=> 'display_name',
																			'name'				=> 'owner1',
																			'multi'             => true,
																			'selected'			=> $l_params ['owner2']
																		  )
																  ).
								'</div>';
		$owners				.= '<div style="float:left; padding:3px;">'.
									wp_dropdown_users(
																	array(
																			'show_option_none'  => "---",
																			'echo'				=> false,
																			'show'				=> 'display_name',
																			'name'				=> 'owner1',
																			'multi'             => true,
																			'selected'			=> $l_params ['owner3']
																		  )
																  ).
								'</div>';
		$owners				.= '</center>';
		
		if(is_plugin_active('user-access-manager/user-access-manager.php'))
		{
			$users			= $UAM->get_uam_users(get_option('uam_group_'.$params[1]));
			$member_list	= '	<style>									
									.lp-list
									{
										padding:2px 10px 2px 2px;;
										dispaly:inline-block;
										background-color:#888;
										ovedflow:hidden;
										float:left;
										margin:4px;
										font-family:Arial;
										font-size:14px!important;
									}
								</style>								
								<div style="text-align:left; padding:0 30px; margin:10px; dispaly:inline-block; ">
																				
								';
			//$member_list	.= $l_params['uam_group'] . " users: " . count($users)."  ".get_option('uam_group_'.$params[1]);
			$i=0;
			foreach($users as $user)
			{
				if($this->user_is_owner($params[1], $user->id)) continue;
				//$member_list.= '		<span class="lp-list"><label for="'.$i++.'">'.get_userdata($user->id)->display_name.'</label><input type="checkbox" name="'.$i++.'" checked="checked"/></span>';
				$member_list.= '<span class="lp-list"><input type="checkbox" checked="checked" name="user'.$i++.'" id="user'.$i.'" class="css-checkbox1" /><label for="user'.$i.'" class="css-label1">'.get_userdata($user->id)->display_name.'</label></span>';
			}
			$member_list	.= '	
								</div>
								<p style="display:inline-block; border-top:1px dotted rgba(0,0,0,0.1);border-bottom:1px solid rgba(255,255,255,0.1);width:70%;">
								<p>
								<div style="dispaly:inline-block; height:60px; margin:10px;font-family:Ubuntu Condensed; font-size:21px; color:white; height:80px;">'.
									__("Add new", "smc")."<p></p>".
									wp_dropdown_users(
																	array(
																			'show_option_none'  => "---",
																			'echo'				=> false,
																			'show'				=> 'display_name',
																			'id'				=> 'new_member',
																			'name'				=> 'new_member',
																			'class'				=> 'chosen-select',
																			'multi'             => true
																		  )
																  ).
								'</div>'; 
		}
		else
		{
			$member_list	= ''.__("Membership system not present", "smc");
		}	
		
		$html				= '<center id="location_settings0" class=" loc-setting-win lp-hide">'.
									$settings.
								'</center>
								<center id="location_settings1" class="loc-setting-win  lp-hide">'.
									$owners.
								'</center>
								<center id="location_settings2" class="loc-setting-win  lp-hide">'.
									$member_list.	
								'</center>
								<div style="position:absolute; bottom:15px; right:20%;">
									<a href="#" class="lp-direct-message-button">'.__("Save").'</a>
								</div>';
								
		$d					=  array(	
										6, //open my location
										array(
												'text'	=> $html,
												'time'	=>  ( getmicrotime()   - $start )												
											  )
									);
		$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
		print $d_obj;
	}
	
	//window for DEBUG
	
	function get_proba($params)
	{
		$w					= 70;	
		global $smc_height;	
		$first_form			= $this->set_carousel_form("","", "browser size: ".$params[15]." x ".$params[16]."px","","","",0);
		$second_form		= $this->set_carousel_form("","","","","","", 1);
		$thierd_form		= $this->set_carousel_form("","","","","","", 1);
		$fourd_form			= $this->set_carousel_form("","","","","","", 1);
		$html				= '<div class="" style="width:100%; height:100%; background:#8968CD;"></div> 
								<div style="position:absolute;top:-30px; left:0;width:100%;">
									<div class="lp-slider-cont" id="slider1_container" style="position: relative; top: 30px; left: '.$w.'px; width: ' . ($params[15]-$w) . 'px;   height: '. $smc_height . 'px; background:transparent;">
										 <!-- Loading Screen -->
										<div u="loading" style="position: absolute; top: 0px; left: 0px;">
											<div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block; background-color: transparent; top: 0px; left: 0px;width: 100%;height:100%;">
											</div>
											<div style="position: absolute; display: block; background: url(wp-content/plugins/Ermak/img/loading.gif) no-repeat center center;
												top: 0px; left: 0px;width: 100%;height:100%;">
											</div>
										</div>
										
										<!-- Slides Container -->
										<div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: ' . ($params[15]-$w) . 'px; height: '. $smc_height . 'px; overflow: hidden;">
											<!-- Direct Message List and Read Client -->
											<div class="jssor-bck" style="background:transparent; height:100%">
												<div>'.
													$first_form.												
												'</div>
												<div u="thumb">
													<div class="i" style="background-image:url(' . SMC_URLPATH . '/img/raiting_icon.png)!important;"></div>	
												</div>	
											</div>
											<!-- New Direct Message Window -->
											<div class="jssor-bck" style="background:transparent; height:100%">
												<div>'.
													$second_form.
												'</div>
												<div u="thumb">
													<div class="i" style="background-image:url(' . SMC_URLPATH . '/img/raiting_icon.png)!important;"></div>													
												</div>
											</div>
											<!-- New Direct Message Window -->
											<div class="jssor-bck" style="background:transparent; height:100%">
												<div>'.
													$thierd_form.
												'</div>
												<div u="thumb">
													<div class="i" style="background-image:url(' . SMC_URLPATH . '/img/raiting_icon.png)!important;"></div>													
												</div>
											</div>
											<!-- New Direct Message Window -->
											<div class="jssor-bck" style="background:transparent; height:100%">
												<div>'.
													$fourd_form.
												'</div>
												<div u="thumb">
													<div class="i" style="background-image:url(' . SMC_URLPATH . '/img/raiting_icon.png)!important;"></div>													
												</div>
											</div>
										</div>
										<!-- ThumbnailNavigator Skin Begin -->
										<div u="thumbnavigator" class="jssort11" style="position: absolute; width: '.$w.'px; height: '. ($smc_height  - 30 ) . 'px; left:-'.$w.'px; top:10px;">
											<div u="slides" style="cursor: pointer;">
												<div u="prototype" class="p" style="position: absolute; width: 70px; height: 70px; top: 0; left: 0;">
													
													<thumbnailtemplate style=" width: 100%; height: 100%; border: none;position:absolute; top: 0; left: 0;">
													</thumbnailtemplate>
												</div>
											</div>
										</div>
										<!-- Thumbnail Item Skin End -->
									</div>
								
							</div>';		
		$d					=  array(	
										1, //open my location
										array(
												'text'	=> $html,
												'exec'	=> 'location_slideshow',
												'args'	=> 'account',
												'time'	=> ( getmicrotime()   - $start )
											  )
									);
		$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
		print $d_obj;
	}
	
	
	function myajax_submit() 
	{
		global $Soling_Metagame_Constructor;
		global $SolingMetagameProduction;
		global $UAM;
		global $start;
		$start				= getmicrotime()  ;
		$nonce = $_POST['nonce'];
		// проверяем nonce код, если проверка не пройдена прерываем обработку
		//if ( !wp_verify_nonce( $nonce, 'myajax-nonce' ) ) die ( 'Stop me!!');
		// обрабатываем данные и возвращаем		
		$params				= $_POST['params'];
		$lp_userdata		= get_user_meta(get_current_user_id(), "metagame_panel");	
		$location_id		= isset($lp_userdata[0]['cur_location']) ? $lp_userdata[0]['cur_location'] : 0;
		
		
		//проверяем права  пользователя.
		$rights				= array(
										"",					//	(1)		locations
										"",					//	(1)		courusel		
										"",					//	(1)		account	
										"",					//	(2)		send_direct_message	
										"",					//	(-)		delete_direct_message	
										"",					//	(-)		add_location	
										"",					//	(4)		new_dm	
										"",					//	(3)		new_post	
										"",					//	(5)		new_location	
										"",					//	(-)		choose_my_location
										"",					//	(7)		change_settings		
										"",					//	(8)		auth		
										"",					//	(-)		proba		
										"",					//	(9)		panel_params_admin	

										"",					//// (10)	default
									);
		
		
		//новое личное сообщение
		if(!empty($_POST['new_mes_title']))
		{
			//echo "send new direct message";
		}		
		switch($params[0])
		{
			case "-1":				
				$d					= array(	
												-1, //ping
												array(
														'time'	=>  ( getmicrotime()   - $start )
   													  )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case "locations":		
				$location_id			= $params[1] != "undefined" && isset($params[1]) ? (int)$params[1] : $location_id;
				$this->set_location($location_id, $params);
				break;
			case "courusel":
				$this->set_carousel($params);
				break;
			case "account":
				$this->set_account($params);
				break;	
			case "send_direct_message":
				global $Direct_Message_Menager;
				$html				= $Direct_Message_Menager->single_dm_text($params[1]);
				$d					= array(	
												2, //send_direct_message
												array(
														'text' => array($html, $params[2]),
														'time'	=>  ( getmicrotime()   - $start )
   													  )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case "delete_direct_message":
				if(get_current_user_id() == get_post_meta($params[1], "addressee", true))
				{
					//wp_delete_post($params[1]);
					update_post_meta($params[1], "enabled", 0);
				}
				//$this->set_account($params);	
				$d					= array(	
												12, //send_direct_message
												array(
														'text' => array($html, $params[2]),
														'time'	=>  ( getmicrotime()   - $start )
   													  )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case "add_location":
				
				break;
			case "new_dm":				
				global $Direct_Message_Menager;
				$Direct_Message_Menager->send_dm(
													array( 'post_title' => $params[1], 'post_content' => $params[2]),
													$params[3],
													3
												);			
				$d					=  array(	
												4, //send_direct_message
												array(
														'text'=>'new dm',
														'time'	=>  ( getmicrotime()   - $start )
   													  )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case "new_post":
				if($UAM->is_current_user_can_write($location_id))
				{
					$new_post_id	= wp_insert_post(array(
																			'post_name'			=> strip_tags( stripslashes($params[1]))
																			, 'post_status' 	=> 'publish'
																			, "post_title"		=> strip_tags( stripslashes($params[1]))
																			, "post_content"	=> strip_tags( stripslashes($params[2]))
																			, "post_type"		=> "post"
																			, 'tax_input'		 => array( SMC_LOCATION_NAME => array( $location_id) )
																			)
																	);
					wp_set_object_terms( $new_post_id, (int)$location_id, SMC_LOCATION_NAME );
					if($params[3])
					{
						switch($params[3])
						{
							case "slides":
								$nuu=0;
								foreach($params[4] as $slide)
								{
									update_post_meta($new_post_id, 'slide_'.$nuu, $slide);
									$nuu++;
								}
								update_post_meta($new_post_id, 'slides', $params[4]);
								break;
							case "video":
								$video_id	= $this->assistants->get_youtube_id_from_url($params[4]);
								update_post_meta($new_post_id, 'youtube_id', $video_id);								
								break;
							case "audio":								
								update_post_meta($new_post_id, 'audio', $params[4]);								
								break;
						}
					}
					$this->default_post_trumbail($new_post_id);
					$succs			= __("New post added succesfully. You can see  them when page reloaded", "smc") . "<BR>". __("Title").":<BR><h3>". $params[1] . "</h3><BR>" . __("Text").":<BR>" . $params[2];//'<div style="color:#FFF; font-family:Arial, Sans-serif; font-size:22px; margin:25px;">'.__("Successful").'</div>';
				}
				else
				{
					$succs			= '<div style="color:#FF0000; font-family:Arial, Sans-serif; font-size:22px; margin:25px;">'.__("Not successful").'</div>';
				}
				$d					=  array(	
												3, //send_direct_message
												array(
														'text'=> $succs,
														'time'	=>  ( getmicrotime()   - $start )
   													  )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;	
			case "new_location":
				$new_id				= SMC_Location::add_new_location(
																		$params[4],
																		$params[1],
																		$params[1],
																		$params[2],
																		$params[3]
																	);
				$d					=  array(	
												"new_location", //new location open
												array(
														'text' => $new_id ? $params[1] : "",
														//'exec'=>"open_metagame_panel",
														//'args'=>"locations",
														'time'	=>  ( getmicrotime()  - $start )
													  )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				//wp_redirect( home_url() ); 
				break;
			case 'choose_my_location':
				$this->choose_my_location($params);
				break;
			case "change_settings":
				update_user_meta( get_current_user_id(), 'iface_fill', "#".str_replace("#", "", $params[1]));
				$d					=  array(	
												7, //change settings
												array(
														'time'	=>  ( getmicrotime()  - $start )
													  )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case "auth":
				$creds = array();
				$creds['user_login'] = $params[1];
				$creds['user_password'] = $params[2];
				$creds['remember'] = true;
				$user = wp_signon( $creds, false );
				
				if ( is_wp_error( $user ) ) {
					$error_string = $user->get_error_message();
					$text			=  '<div id="message" class="error"><p>' . $error_string . '</p></div>';
				}
				else {
					$text			=  __("Access authorization. Please wait", 'smc');
				}
				$d					=  array(	
												8, //send_error
												array(
														'text'=>$text,
														//'exec'=>'open_metagame_panel',
														//'args'=>'panel',
														'time'	=>  ( getmicrotime()  - $start )
													  )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case "proba":				
				$this->get_proba($params);
				break;
			case "panel_params_admin":
				$label_pos			= json_encode($params[1]);
				$dot_poses			= $params[2];
				$dots				= array();
				if(count($dot_poses))
				{
					foreach($dot_poses as $dot_pos)
					{
						$tax			= SMC_Location::get_term_meta( $dot_pos['id'] );//get_option("taxonomy_".$dot_pos['id']);
						$tax['y_pos']	= $dot_pos['offset']['top'];
						$tax['x_pos']	= $dot_pos['offset']['left'];
						update_option("taxonomy_".$dot_pos['id'], $tax);
					}
				}
				$this->options['map_titles_coords']		= $label_pos;
				update_option(SMC_ID, $this->options);
				$d					= array(	
												9, //command id
												array(
														'text' => array($dot_poses),
														'exec' => '',
														'args' => "",
														'time'	=>  ( getmicrotime()  - $start )
													  )
									);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case "panel_params_admin_remove":				
				$this->options['map_titles_coords']		= null;
				update_option(SMC_ID, $this->options);
				break;
			case "new_location_owner_dm":
				$loc				= get_term_by("by", $params[3], SMC_LOCATION_NAME);
				$owners				= $this->location_has_owners($params[3]);
				if($owners)
				{	
					$users			= array();
					foreach($owners as $owner)
					{
						if($owner =='')	continue;
						$addressee_id	= $owner;
						$user			= get_userdata($owner);
						
						$new_post_id	= wp_insert_post(array(
																				'post_name'			=> $params[1]
																				, 'post_status' 	=> 'publish'
																				, "post_title"		=> sprintf(__("Message to owner of %s: ", "smc"), $loc->name) . $params[1]
																				, "post_content"	=> $params[2]
																				, "post_type"		=> "direct_message"
																				)
																		);
						add_post_meta($new_post_id,  'viewed', 0, true);
						add_post_meta($new_post_id,  'source_type', 3, true);
						add_post_meta($new_post_id,  'addressee', $owner, true);
						$users[]		= $user->display_name;
					}
					$html				= __("Success send messages to: ","smc"). implode( ", ", array_diff ($users, array("")) );
				}
				else
				{
					$html			= __("Location has not owners.","smc");
				}
				$d					= array(	
												"new_location_owner_dm",
												array(
														'text' => $html,
														'exec' => '',
														'args' => "",
														'time'	=>  ( getmicrotime()  - $start )
													  )
									);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case "new_master_dm":					
				$masters			= get_posts(
												array(
														'post_type'       => 'master',
														'post_status'     => 'publish'
													  )
												);
				$m					= "Masters: ";
				foreach($masters as $master)
				{
					$addressee_id	= get_post_meta($master->ID, "user", true);
					$new_post_id	= wp_insert_post(array(
																			'post_name'			=> $params[1]
																			, 'post_status' 	=> 'publish'
																			, "post_title"		=> __("to Master: ", "smc") . $params[1]
																			, "post_content"	=> $params[2]
																			, "post_type"		=> "direct_message"
																			)
																	);
					add_post_meta($new_post_id,  'viewed', 0, true);
					add_post_meta($new_post_id,  'source_type', 3, true);
					add_post_meta($new_post_id,  'addressee', $addressee_id, true);
					$m				.= $addressee_id . ", ";
				}
				$d					=  array(	
												11, 			//send_master_direct_message
												array(
														'text' 	=> $m,
														'time'	=> getmicrotime()  - $start												
   													  )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case "special_filtered_panel":
				$html				= "";
				$m					= apply_filters("smc_display_special_panel", $params[1]);
				$d					= array(	
												13, 			//
												array(
														'text' 	=> $m['text'],
														'exec' 	=> $m['exec'],
														'args'	=> $m['args'],
														'full'	=> $m,
														'ptp'	=> 0,
														'time'	=> getmicrotime()  - $start
   													  )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case 'open_location_content_widget':
				$lt_id		= $Soling_Metagame_Constructor->get_location_type_id($params[1]);
				$lt		= get_post($lt_id);
				$html3				.= "
					<div class='smc_red_rounded hint hint--left' data-hint='" . __("goto to Location", "smp") . "'  view_content='" . get_term_link((int)$params[1], SMC_LOCATION_NAME) . "' style='position:absolute; right:32px; top:17px;'>
						<i class='fa fa-play'></i> ". //__("Goto", "smc") . 
					"</div>";
				$title	= "<span class='lp_under_title_klapan'>".$lt->post_title . " " . $lt->picto . " </span>".$params[2] . $html3; 
				$your	= $this->cur_user_is_owner($params[1]);
				$own			= $your ? "<div class=smc-your-label style='right:0px;'>".__("It's your", "smc")."</div>" : "<div class='smc-notyour-label' style='right:0px; '>" . __("It's not your", "smc") . "</div>" . Assistants::get_call_to_owner_hinter($params[1], __("About Location ", "smc") .$params[2]);
				
				$ass	= SMC_Location::get_location_widget($params[1]);
				$html	= Assistants::get_switcher(
													apply_filters("open_location_content_widget",  $ass, $params)
												   ). $own;
				$d		= array(	
								$params[0], 
								array(
										'text'	=> array( $title, $html, $params[3], $params[1]),
										'time'	=> getmicrotime()  - $start
									 )
							);
				$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case 'open_location_widget':
				$lt_id	= $Soling_Metagame_Constructor->get_location_type_id($params[1]);
				$lt		= get_post($lt_id);
				$title	= "<span class='lp_under_title_klapan'>".$lt->post_title." " . $lt->picto . "</span> " . $params[2];
				$html	= $SolingMetagameProduction->get_widget_bussiness_form("", $params[1]);
				$d		= array(	
								$params[0], 
								array(
										'text'	=> array($title, $html, $params[3], $params[1]),
										'time'	=> getmicrotime()  - $start
									 )
							);
				$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case "goto_special_page":
				$d		= array(	
								$params[0], 
								array(
										'text'	=> get_permalink($params[1]),
										'time'	=> getmicrotime()  - $start
									 )
							);
				$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;	
			case "clear_log":
				if(current_user_can("administrator"))
				{
					Assistants::clear_log();
					$text			= __('Successful');
				}
				else
				{
					$text			= __("Only Administrator can clear Log!", "smc");
				}
				$d		= array(	
								$params[0], 
								array(
										'text'	=> $text,
										'time'	=> getmicrotime()  - $start
									 )
							);
				$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case "reload_log":
				ob_start();
				my_log_page();
				$text	= ob_get_contents();
				ob_end_clean();
				$d		= array(	
								$params[0], 
								array(
										'text'	=> $text,
										'time'	=> getmicrotime()  - $start
									 )
							);
				$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case "update_my_location":
				$loc_id	= $params[1];
				$loc	= SMC_Location::get_instance($loc_id);
				$data	= $params[2];
				$text	= SMC_Location::update_my_location($loc_id, $data);
				$content		=  sprintf(
					__("Change owners of %s. New owners are: %s, %s, %s", "smc"), 
					"<b>".$loc->name."</b> ",
					"<b>".get_userdata((int)$data['owner_1'])->display_name."</b> ", 
					"<b>".get_userdata((int)$data['owner_2'])->display_name."</b> ",
					"<b>".get_userdata((int)$data['owner_3'])->display_name."</b> "
				);
				SMC_Location::add_log((int)$loc_id, $content, CHANGE_LOCATION_MEMBERS, get_current_user_id());
				$d		= array(	
								$params[0], 
								array(
										'text'	=> $text, //"Change parameters of ".$loc_id ."<br>". Assistants::echo_me($data),
										'time'	=> getmicrotime()  - $start
									 )
							);
				$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case "smc_register_user":
				$user_name			= $params[1];
				$user_pass			= $params[2];
				$e_mail				= $params[3];				
				$user_id 			= username_exists( $user_name );
				$u_email			= get_user_by('email', $e_mail);
				$mess				= "<h1>".__("Insert new Player", "smc")."</h2>";
				if ( !$user_id && !$u_email) 
				{
					$new_user_id	= wp_create_user($user_name, $user_pass, $e_mail);
					if( ! is_wp_error( $new_user_id ) ) 
					{					
						wp_update_user( array( 'ID' => $new_user_id, 'role' => 'Player' ) );
						$mess	.= sprintf(__('New player %s successfull inserting.<p>Now you mast log in site by your account name and password.</p>', "smc"), $user_name);
					}
					else
					{
						$mess	= $user_id->get_error_message();
					}
				}
				else if($u_email)
					$mess			.= __("This user's email is occupied." , 'smc');  
				else
					$mess			.= __("This user's name is occupied." , 'smc');  
				
				$mess				=  apply_filters("smc_register_action", $mess, $new_user_id, $datas);
				$d		= array(	
									$params[0], 
									array(
											'a_alert'	=> $mess,
											'time'		=> getmicrotime()  - $start
									 )
							);
				$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
				print $d_obj;
				break;
			case 'paged':
					$funct				= $params[1];
					$page_id			= $params[2];	
					$cont_id			= $params[3];
					$html				= function_exists($funct) ? $funct( $page_id, $cont_id ) : "no";
					$d					= array(	
													$params[0], 
													array(
															"content"	=> $html,
															'cont'		=> $cont_id,
															'time'		=> ( getmicrotime()  - $start )														
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
			case 'refresh_location_display':					
					require_once(SMC_REAL_PATH.'shortcodes/location_display.php');
					$term_id			= $params[1];
					$step				= (int)$params[2];
					$location 			= SMC_Location::get_instance($term_id);
					$term_meta 			= SMC_Location::get_term_meta($term_id);
					$location_type		= get_post($term_meta['location_type']);
					$tbl0				= get_tbl0($step, $location, $term_meta, $location_type);
					$tbl1				= get_tbl1($step, $location, $term_meta, $location_type);
					$tbl11				= get_tbl11($step, $location, $term_meta, $location_type);
					$tbl20				= get_tbl20($step, $location, $term_meta, $location_type);
					$d					= array(	
													$params[0], 
													array(
															//"text"		=> draw_location_display($params[1]),
															"tbl0"		=> $tbl0,
															"tbl1"		=> $tbl1,
															"tbl11"		=> $tbl11,
															"tbl20"		=> $tbl20,
															'time'		=> ( getmicrotime()  - $start )														
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
			case "wp_logout":
					wp_logout();
					$d					= array(	
													$params[0], 
													array(
															'time'		=> ( getmicrotime()  - $start )														
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
			default:
				do_action("smc_myajax_submit", $params);
				break;
		}
		
		// Не забываем выходить
		exit;
	}
	
	
	
	function add_location_interface($height = -1)
	{		
		global $is_panel_hide_current, $smc_height;
		switch(true)
		{
			case $this->options['panel_only_front']:
				if(!is_front_page())
					$is_panel_hide_current = true;
				break;
		}
		if($is_panel_hide_current) return;
		if($height) 
			$smc_height = $height;
			
		require_once(SMC_REAL_PATH."tpl/Main_panel_front.php");
	}
	
	function my_action_callback()
	{
		$nonce = $_POST['nonce'];
		// проверяем nonce код, если проверка не пройдена прерываем обработку
		if ( !wp_verify_nonce( $nonce, 'zmcnweurtgznlxcawd' ) ) die ( 'Stop!!!!');
			
		// обрабатываем данные и возвращаем
		$params				= $_POST['params'];				
		switch($params[0])
		{
			case "load_location_map":
				$d					=  array(	
												2,
												array(
														'text' 		=> 0,
														'time'	=>  ( getmicrotime()  - $start )
													 )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));				
				print $d_obj;
				break;
			case "save_map_data":
				update_option("taxonomy_map_".$params[2], stripslashes($params[1]));
				$d					=  array(	
												3,
												array(
														'text' => $params[2],
														'time'	=>  ( getmicrotime()  - $start )
													 )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));				
				print $d_obj;
				break;
			case "save_main_map_data":
				//$this->options['main_map_data']	= stripslashes($params[1]);
				$this->save_main_map_svg_data(stripslashes($params[1]));				
				$d					=  array(	
												3,
												array(
														'exec' => '',
														'args' => stripslashes($params[1]),
														'time'	=>  ( getmicrotime()  - $start )
													 )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));				
				print $d_obj;
				break;
			case "save_map_admin":
				global $SMC_Map;
				require(SMC_REAL_PATH.'class/SMC_Map.php');
				$SMC_Map		= new SMC_Map;
				$SMC_Map->set_map_data(stripslashes($params[1]));
				$d					=  array(	
												'save_map_admin',
												array(
														'text' => 'ura!',
														'a_alert' => stripslashes($params[1]),
														'time'	=>  ( getmicrotime()  - $start )
													 )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));				
				print $d_obj;
				break;
			case "remove_setting":
				delete_option(stripslashes($params[1]));
				$d					=  array(	
												'remove_setting',
												array(
														'text' => stripslashes($params[1]),
													 )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));				
				print $d_obj;
				break;
			case "change_loc_owners":
				SMC_Location::update_taxonomy_custom_meta(
															(int)$params[4], 
															array(
																	"owner1"=>(int)$params[1], 
																	"owner2"=>(int)$params[2], 
																	"owner3"=>(int)$params[3]
																  )
														 );
				$loc			= SMC_Location::get_instance((int)$params[4]);
				$content		=  sprintf(
					__("Change owners of %s. New owners are: %s, %s, %s", "smc"), 
					$loc->name,
					"<b>".get_userdata((int)$params[1])->display_name."</b>", 
					"<b>".get_userdata((int)$params[2])->display_name."</b>",
					"<b>".get_userdata((int)$params[3])->display_name."</b>" 
				);
				SMC_Location::add_log((int)$params[4], $content, CHANGE_LOCATION_MEMBERS, get_current_user_id());
				$d					=  array(	
												'change_loc_owners',
												array(
														'text' => array( "".
																			get_userdata($params[1])->display_name."<BR>".
																			get_userdata($params[2])->display_name."<BR>". 
																			get_userdata($params[3])->display_name. 
																		"", $params[4],$params[1], $params[2], $params[3] )
													 )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));				
				print $d_obj;
				break;
			case "upload_media":
				$d					=  array(	
												$params[0],
												array(
														'text' => "upload successfull."
													 )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));				
				print $d_obj;
				break;
			case "pre_install_loctypes":
				require_once(SMC_REAL_PATH.'tpl/register_hook.php');
				$types 				= "
				<h2>".__("Install new default Location Types", "smc")."</h2>".get_loctype_dialog()."<div class='button-primary smc-large-button' id='insdeloty'>".__("Install")."</div>";
				$d					=  array(	
												$params[0],
												array(
														'text' => $types
													 )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));				
				print $d_obj;
				break;
			case "install_loctypes":
				require_once(SMC_REAL_PATH.'tpl/register_hook.php');
				$col				= install_loctypes($params[1]);
				$d					=  array(	
												$params[0],
												array(
														'text' => $col
													 )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));				
				print $d_obj;
				break;
			case "pre_default_menu":
				$html				= Assistants::add_default_menu();
				
				$d					= array(	
												$params[0],
												array(
														'text' => $html
													 )
											);
				$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));				
				print $d_obj;
				break;
			case "refresh_post_table":
					$post_id				= $params[1];
					$post_type				= $params[2];
					$items_per_page			= $params[3];
					//$origin					= $post_type::get_instance($post_id);
					//$metas					= $origin->doubled();					
					Assistants::doubled_post($post_id);					
					$new_id				= 
					$d					= array(	
													$params[0], 
													array(
															'text'		=> "",
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
		}
		do_action("smc_myajax_admin_submit", $params);
		exit;
	}
	
	
	// SMC FILTERS
	function smc_load_panel_screen($html, $currenting_url)
	{
		/*
		if(is_single())
		{
			if($tr_if		= get_post_meta( 1, '_thumbnail_id', true ))
				$trr		= $tr_if;
			else
				$trr		= "";
				
			return "open_metagame_panel('special_filtered_panel', '" . 
						"<div class=lp_opn_mtgm_pnl $trr><center class=lp_opn_mtgm_title><div class=smc_special_title>" . 
						get_the_title(). "</div><div id=undertitle></div></center></div>')";
		}
		*/
		$prfx		= get_bloginfo("url"). "/?page_id=";
		switch($currenting_url)
		{
			case $prfx . $this->options['location_page_id']:
				$trum_img	= SMC_URLPATH."img/earth.jpg"; 
				return "open_metagame_panel('special_filtered_panel', '" . 
					"<div class=lp_opn_mtgm_pnl style=background-image:url($trum_img);><center class=lp_opn_mtgm_title><div class=smc_special_title>" . 
					__("My Locations", "smc") . "</div><div id=undertitle></div></center></div>')";
			
			case $prfx . $this->options['masters_page_id']:
				$trum_img	= SMC_URLPATH."img/masters.jpg"; 
					return "open_metagame_panel('special_filtered_panel', '" . 
					"<div class=lp_opn_mtgm_pnl style=background-image:url($trum_img);><center class=lp_opn_mtgm_title><div class=smc_special_title>" . 
					__("Masters", "smc") . "</div><div id=undertitle></div></center></div>')";
				return "open_metagame_panel('special_filtered_panel', '" . __("Masters", "smc") . "')";	
			case $prfx . $this->options['prsonal_page_id']:
				$trum_img	= SMC_URLPATH."img/direct_messages.jpg"; 
					return "open_metagame_panel('special_filtered_panel', '" . 
					"<div class=lp_opn_mtgm_pnl style=background-image:url($trum_img);><center class=lp_opn_mtgm_title><div class=smc_special_title>" . 
					__("Direct Messages", "smc") . "</div><div id=undertitle><div class=smc_button_alert target_name=new_message_win >".
					__("add new Direct Message", "smc").
					"</div><div class=smc_button_alert target_name=master_dm_win>".
					__("Call to Master", "smc").
					"</div></div></center></div>')";
				return "open_metagame_panel('special_filtered_panel', '" . __("Direct Messages", "smc") . "')";			
			default:
				return $html;
		}
		//return "open_metagame_panel('special_filtered_panel', '" . $currenting_url . "')";
	}
	
	
	
	
	//========================================
	//
	// INIT SETTINS FOR ACTIVATION PLUGIN
	//
	//========================================
	function init_settings()
	{
		global $wpdb;
		init_ermak_textdomain();
		$wpdb->query(
				"CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "ermak_log` (
				  `ID` BIGINT( 20 ) UNSIGNED NOT NULL AUTO_INCREMENT,
				  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				  `who` varchar(32) DEFAULT NULL,
				  `data` varchar(1024) DEFAULT NULL,
				  PRIMARY KEY (`id`),
				  KEY `ts` (`ts`) 
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
		);
		$new_dir	= ABSPATH."video";
		@mkdir($new_dir, 0777);	
		//wp_delete_post(1);
		SMC_Location::install();
		add_role( "Player", __("Player", "smc") , array(
															'edit_published_posts'		=> 1,
															'upload_files'				=> 1,
															'publish_posts'				=> 1,
															'delete_published_posts'	=> 1,
															'edit_posts'				=> 1,
															'delete_posts'				=> 1,
															'read'						=> 1
														)
				);
		add_role( "Master", __("Master", "smc") , array(
															'moderate_comments'			=> 1,
															'edit_others_posts'			=> 1,
															'edit_pages'				=> 1,
															'edit_others_pages'			=> 1,
															'edit_published_pages'		=> 1,
															'publish_pages'				=> 1,
															'delete_pages'				=> 1,
															'delete_others_pages'		=> 1,
															'delete_published_pages'	=> 1,
															'delete_others_posts'		=> 1,
															'delete_private_posts'		=> 1,
															'edit_private_posts'		=> 1,
															'read_private_posts'		=> 1,
															'delete_private_pages'		=> 1,
															'edit_private_pages'		=> 1,
															'read_private_pages'		=> 1,
															'unfiltered_html'			=> 1,
															'list_users'				=> 1,
															'manage_options'			=> 1,
															'promote_users'				=> 1,
															'remove_users'				=> 1,
															'edit_dashboard'			=> 1,
															'create_users'				=> 1,
															'edit_users'				=> 1,
															'delete_users'				=> 1,
															
															'edit_published_posts'		=> 1,
															'upload_files'				=> 1,
															'publish_posts'				=> 1,
															'delete_published_posts'	=> 1,
															'edit_posts'				=> 1,
															'delete_posts'				=> 1,
															'read'						=> 1
														)
				);
		$options						= array();
		$options['location_meta']		= array(
													'location_type',
													'hiding_type',
													'map_type',
													'owner1',
													'owner2',
													'owner3',
													'creator',
													'x_pos',
													'y_pos',
													'members',
													'meta_data_array'
											    );
		//$options['project_name']		= get_bloginfo("name");
		$options['title_background']	= "#C0D197";
		$options['main_map_color']		= '#153763';
		$options['project_pic']			= "";
		$options['admin_optimyze']		= false;
		$options['hide_panel']			= true;
		$options['use_panel']			= true;
		$options['smc_height']			= 330;
		$options['use_location_menu']	= true;
		$options['use_courusel']		= true;
		$options['account_panel']		= false;
		$options['show_quick_menu']		= true;
		$options['location_change_info']= true;
		$options['courusel_slides']		= 7;
		$options['fx_index']			= 0;
		$options['public_content']		= array( 'image'=>1, 'youtube'=>1, 'audio'=>1 );
		$options['use_exec']			= false;
		$options['default_lt_pic']		= "<i class='fa fa-globe'></i>";
		$options['no_fixed_and_no_closed']	= true;
		$options['nav_menu_height_klapan']	= 0;
		$options['klapan_bckgrnd1']		= "ametist_pattern3.jpg";
		$options["main_map_name"]		= get_bloginfo('name')=="" ? __("Metagame","smc") : get_bloginfo('name');
		$options["map_type"]			= 0;				// main screen on Location Panel. Type of map (0 - row-list, 1 - svg, 2 - column-list)
		$options["map_titles_coords"]	= 0;				// JSON for client's jQuery. Coordinates for Location's titles in svg map.
		$options["display_update_period"]	= 5000;
		$options['location_panel_pars']	= array(
													 '#E73F50',
													 "#008B8B",
													 "#B22222",
													 "#DAA520",
													 "#228B22",
												);
		$options['fills']				= array(
													array("#660000","#990000"),
													array("#006421","#009933"),
													array("#004C72","#006699"),
													array("#AD8B03","#CBA303"),
													array("#464545","#666666"),
													array("#B80505","#FF0000"),
													array("#3F0000","#660000"),
													array("#414D05","#5A6908"),
													array("#774E05","#A16B0A"),
													array("#05775A","#0AB88C"),
												);
		$options['iface_fill']			=  '#E73F50';
		$options['master_pade_id'];
		$options['default_featured_title']		=	"Карусель медиа-постов";
		$options['default_featured_text']		=	"Включите в свое сообщение ссылку на ролик youtube, историю из фото или аудио-ролик, и твое сообщение попадёт сюда.";
		
		// about project page
		$my_post = array(
							  'post_title'   		=> __("About the project", "smc"),
							  'post_type' 			=> 'page',
							  'post_content' 		=> "",
							  'post_status'  		=> 'publish',
							  'comment_status'		=> 'closed',
							);
		$about										= wp_insert_post( $my_post );
		$options['about_the_project_page_id']		= $about;
		
		// log page
		$my_post = array(
							  'post_title'   		=> __("Logs", "smc"),
							  'post_type' 			=> 'page',
							  'post_content' 		=> "[my_log_page]",
							  'post_status'  		=> 'publish',
							  'comment_status'		=> 'closed',
							);
		$my_log_page								= wp_insert_post( $my_post );
		$options['my_log_page_id']					= $my_log_page;
		
		// Location display
		$my_post = array(
							  'post_title'   		=> __("Location display", "smc"),
							  'post_type' 			=> 'page',
							  'post_content' 		=> "[location_display]",
							  'post_status'  		=> 'publish',
							  'comment_status'		=> 'closed',
							);
		$my_log_page								= wp_insert_post( $my_post );
		$options['location_display']				= $my_log_page;
		
		// Location display
		$my_post = array(
							  'post_title'   		=> __("Personal smartphone", "smc"),
							  'post_type' 			=> 'page',
							  'post_content' 		=> "[personal_smartphone]",
							  'post_status'  		=> 'publish',
							  'comment_status'		=> 'closed',
							);
		$personal_smartphone						= wp_insert_post( $my_post );
		$options['personal_smartphone']				= $personal_smartphone;
		
		// rules and rights page
		$my_post = array(
							  'post_title'   		=> __("Rules and rights", "smc"),
							  'post_type' 			=> 'page',
							  'post_content' 		=> "",
							  'post_status'  		=> 'publish',
							  'comment_status'		=> 'closed',
							);
		$rules										= wp_insert_post( $my_post );
		$options['rules_page_id']					= $rules;
		
		// init location page
		$my_post = array(
							  'post_title'   		=> __("My Locations", "smc"),
							  'post_type' 			=> 'page',
							  'post_content' 		=> "[smc_locations_page]",
							  'post_status'  		=> 'publish',
							  'comment_status'		=> 'closed',
							);
		$location_page_id							= wp_insert_post( $my_post );
		$options['location_page_id']				= $location_page_id;
		$options['masters_page_id']					= Master_Menager::install();
		update_option(SMC_ID, $options);
		update_option('install_ermak', 1);
		
		$hash				= get_option(SMC_HASH);
		if(!isset($hash) || $hash=="")
		{
			$my_name			= md5(get_bloginfo('url'));	
			update_option(SMC_HASH, $my_name);
		}
		Assistants::init_main_mg_menu();		
		return;
	}
	
	
	//=======================================
	//
	//	UNINSLALL PLUGIN
	//
	//=======================================
	static function smc_deactivation()
	{	
		$op					= get_option(SMC_ID);
		$txt				= "
		<style>
			stt{color:red;}
		</style>
		<h2>Result of deactivation Ermak </h2>
		<h4>Date: " . date_i18n( __( 'M j, Y @ G:i' )) . "</h4>
		<ul>";
		$txt			.= wp_delete_post( $op["location_page_id"], true) ? "<li><stt>My Locations page (" . $op["location_page_id"] . ") is not deleted.</stt></li>" : "<li>My Locations page delete successfully!.</li>";
		$txt			.= wp_delete_post( $op["my_log_page_id"], true) ? "<li><stt>Log page (" . $op["my_log_page_id"] . ") is not deleted.</stt></li>" : "<li>Log page delete successfully!.</li>";
		$txt			.= wp_delete_post( $op["about_the_project_page_id"], true) ? "<li><stt>About Project page (" . $op["about_the_project_page_id"] . ") is not deleted.</stt></li>" : "<li>About Project page delete successfully!.</li>";
		$txt			.= wp_delete_post( $op["rules_page_id"], true) ? "<li><stt>Rules page (" . $op["rules_page_id"] . ") is not deleted.</stt></li>" : "<li>Rulespage delete successfully!.</li>";
		$txt			.= wp_delete_post( $op["masters_page_id"], true) ? "<li><stt>Masters page (" . $op["masters_page_id"] . ") is not deleted.</stt></li>" : "<li>Masters page delete successfully!.</li>";
		$txt			.= wp_delete_post( $op["prsonal_page_id"], true) ? "<li><stt>Messages page (" . $op["prsonal_page_id"] . ") is not deleted.</stt></li>" : "<li>Messages page delete successfully!.</li>";
		$txt			.= wp_delete_post( $op["location_display"], true) ? "<li><stt>Location display page (" . $op["location_display"] . ") is not deleted.</stt></li>" : "<li>Location display delete successfully!.</li>";
		$txt			.= wp_delete_post( $op["personal_smartphone"], true) ? "<li><stt>personal smartphone page (" . $op["personal_smartphone"] . ") is not deleted.</stt></li>" : "<li>Location display delete successfully!.</li>";
		$txt			.= "</ul>";
		$otl			= get_post(1);
		if($otl)
		{
			$otl->post_content.= $txt;
			wp_update_post($otl);
		}
		remove_role( 'Player' );
		remove_role( 'Master' );
		delete_option(SMC_ID);
		delete_option('install_ermak');
	}
	
	function clear_all()
	{
		//delete Location_type posts
		$posts = get_posts(array(
			
			'post_type'       => 'location_type',
			'post_status'     => 'publish'
		));
		foreach($posts as $post)
		{ 			
			wp_delete_post( $post->ID, true );
		}
		wp_reset_postdata();
		
		// delete Locations
		$locations = get_terms(SMC_LOCATION_NAME);
		echo "delete locations ".count($locations);
		foreach( $locations as $location )
		{
			//echo $location->term_id;
			wp_delete_term($location->term_id, SMC_LOCATION_NAME);
		}
		// delete Direct Messages
		$posts = get_posts(
							array(
			
									'post_type'       => array('master','direct_message','Instruction'),
									'post_status'     => 'publish'
								)
						);
		foreach($posts as $post)
		{ 			
			wp_delete_post( $post->ID, true );
		}
		wp_reset_postdata();
		
		// delete options
		//delete_option(SMC_ID);	
	}
	//=======================================
	//
	//	ADD CSS STYLE
	//
	//=======================================
	function ermak_body_script()
	{
		global $post;
		wp_register_script('empty', plugins_url("/js/empty.js", __FILE__ ), array());
		wp_enqueue_script('empty');	
		?><script type='text/javascript' src='<?php echo SMC_URLPATH ?>js/jquery.fullscreen-min.js'></script><?php
		if($post->ID 	!= $Soling_Metagame_Constructor->options['location_display'])
		{
			echo "
			<link rel='stylesheet' id='empty'  href='".SMC_URLPATH ."css/empty.css' type='text/css' media='all' />
			<link rel='manifest' href='/manifest.json'> 
			<script type='text/javascript' src='".SMC_URLPATH ."js/empty.js'></script>
			";
			
		}
	}

	function add_frons_js_script()
	{	
		
		//jQuery		
		//wp_register_script('jquery_min1', "http://yandex.st/jquery/1.5.0/jquery.min.js", array());
		//wp_register_script('jquery_min1', plugins_url("/js/jquery-2.1.0.min.js", __FILE__ ), array());
		wp_register_script('jquery_min1', plugins_url("/js/jquery-1.4.4.js", __FILE__ ), array());
		//wp_register_script('jquery_min1', plugins_url("/js/jquery-1.7.2.min.js", __FILE__ ), array());
		wp_enqueue_script('jquery_min1');	
		
		//css
		wp_register_style('lp-css', plugins_url( '/css/ermak_front.css', __FILE__ ), array());
		wp_enqueue_style('lp-css');
		
		//Font Awesome
		// Register the style like this for a plugin:
		wp_register_style( 
							'Awesome-style', 
							plugins_url( "/css/font-awesome.min.css" , __FILE__ ), 
						  array()
						  );
		
		// For either a plugin or a theme, you can then enqueue the style:
		wp_enqueue_style( 'Awesome-style' );
		
		//slider
		wp_register_script('slider', plugins_url( '/js/ion.rangeSlider.min.js', __FILE__ ), array());
		wp_enqueue_script('slider');
		wp_register_style( 'rangeSlider_css', plugins_url( '/css/ion.rangeSlider.css', __FILE__ ), array());
		wp_enqueue_style( 'rangeSlider_css' );	
		
		//jquery.printElement.min		
		wp_register_script('lp-print', plugins_url( '/js/html5-3.6-respond-1.1.0.min.js', __FILE__ ), array());
		wp_enqueue_script('lp-print');	
		
		//jQuery UI
		wp_register_script('jquery_ui', plugins_url("/js/jquery-ui.js", __FILE__ ), array());
		wp_enqueue_script('jquery_ui');
		
		/*jQuery UI
		wp_register_script('draggable', plugins_url("/js/jquery.ui.draggable.js", __FILE__ ), array());
		wp_enqueue_script('draggable');
		wp_register_script('core', plugins_url("/js/jquery.ui.core.js", __FILE__ ), array());
		wp_enqueue_script('core');
		wp_register_script('mouse', plugins_url("/js/jquery.ui.mouse.js", __FILE__ ), array());
		wp_enqueue_script('mouse');
		wp_register_script('widget', plugins_url("/js/jquery.ui.widget.js", __FILE__ ), array());
		wp_enqueue_script('widget');
		*/
		
		// 
		wp_register_style( 'normalize', plugins_url( '/css/normalize.min.css', __FILE__ ), array());
		wp_enqueue_style( 'normalize' );
		wp_register_script('chosen', plugins_url( '/js/chosen.jquery.js', __FILE__ ), array());
		wp_enqueue_script('chosen');	
				
		wp_register_script('arcticmodal', plugins_url( '/js/jquery.arcticmodal-0.3.min.js', __FILE__ ), array());
		wp_enqueue_script('arcticmodal');	
				
		wp_register_script('jssor', plugins_url( '/js/jssor/jssor.core.js', __FILE__ ), array());
		wp_enqueue_script('jssor');
				
		wp_register_script('jssor-utils', plugins_url( '/js/jssor/jssor.utils.js', __FILE__ ), array());
		wp_enqueue_script('jssor-utils');
				
		wp_register_script('jssor-slider', plugins_url( '/js/jssor/jssor.slider.js', __FILE__ ), array());
		wp_enqueue_script('jssor-slider');
		
		wp_register_script('youtube', 'https://www.youtube.com/iframe_api', array());
		wp_enqueue_script('youtube');
				
		wp_register_script('tinycolorpicker', plugins_url( '/js/jquery.tinycolorpicker.js', __FILE__ ), array());
		wp_enqueue_script('tinycolorpicker');
		
		// svg-library
		wp_register_script('svg', plugins_url( '/js/svg.js', __FILE__ ), array());
		wp_enqueue_script('svg');
		
		wp_register_script('svgpatch', plugins_url( '/js/svg.patch-trans.js', __FILE__ ), array());
		wp_enqueue_script('svgpatch');
		
		wp_register_script('svgparser', plugins_url( '/js/svg.parser.min.js', __FILE__ ), array());
		wp_enqueue_script('svgparser');
		
		wp_register_script('svgimport', plugins_url( '/js/svg.import.min.js', __FILE__ ), array());
		wp_enqueue_script('svgimport');
		
		wp_register_script('rangeslider', plugins_url( '/js/rangeslider.min.js', __FILE__ ), array());
		wp_enqueue_script('rangeslider');
		
		wp_register_script('fullscreen', plugins_url( '/js/jquery.fullscreen-min.js', __FILE__ ), array());
		wp_enqueue_script('fullscreen');
		
		//dfrontend		
		wp_register_script('ermak_front', plugins_url( '/js/ermak_front.js', __FILE__ ), array());
		wp_enqueue_script('ermak_front');
		
				
		wp_register_script('jscolor', plugins_url( '/js/jscolor.js', __FILE__ ), array());
		wp_enqueue_script('jscolor');
		
		//ajax
		wp_localize_script( 'jquery', 'myajax', array(
															'url' => admin_url('admin-ajax.php'),
															'nonce' => wp_create_nonce('myajax-nonce')
													   ) );
	}

	function gg_scripts_add()
	{
		//return;
		wp_enqueue_script('dys-script', 'http://metaversity.ru/js/iframe.js');
		
		wp_register_script('datetimepicker', plugins_url( '/js/jquery.datetimepicker.js', __FILE__ ), array());
		wp_enqueue_script('datetimepicker');
		
		//jQuery		
		//wp_register_script('jquery_min1', plugins_url("/js/jquery-1.4.4.js", __FILE__ ), array());
		//wp_enqueue_script('jquery_min1');
		
		
		//chosen
		wp_register_script('chosen', plugins_url( '/js/chosen.jquery.js', __FILE__ ), array());
		wp_enqueue_script('chosen');			
				
		wp_register_script('jscolor', plugins_url( '/js/jscolor.js', __FILE__ ), array());
		wp_enqueue_script('jscolor');
		
		//slider
		wp_register_script('slider', plugins_url( '/js/ion.rangeSlider.min.js', __FILE__ ), array());
		wp_enqueue_script('slider');
		
		//js
		wp_register_script('smc-js', plugins_url( '/js/ermak_admin.js', __FILE__ ), array());
		wp_enqueue_script('smc-js');
		//ajax
		wp_localize_script( 'jquery', 'myajax1', array(
															'url' => admin_url('admin-ajax.php'),
															'nonce' => wp_create_nonce('zmcnweurtgznlxcawd')
													   ) );
		// svg-library
		wp_register_script('svg', plugins_url( '/js/svg.min.js', __FILE__ ), array());
		wp_enqueue_script('svg');
		
		wp_register_script('svgparser', plugins_url( '/js/svg.parser.min.js', __FILE__ ), array());
		wp_enqueue_script('svgparser');
		
		wp_register_script('svgimport', plugins_url( '/js/svg.import.min.js', __FILE__ ), array());
		wp_enqueue_script('svgimport');
		
		
		wp_register_script('arcticmodal', plugins_url( '/js/jquery.arcticmodal-0.3.min.js', __FILE__ ), array());
		wp_enqueue_script('arcticmodal');
		
		wp_enqueue_media();
	}
	
	
	function gg_styles_with_the_lot()
	{
		// Register the style like this for a plugin:
		wp_register_style( 'rangeSlider', plugins_url( '/css/ion.rangeSlider.css', __FILE__ ), array());
		wp_enqueue_style( 'rangeSlider' );			
		
		
		
		// Register the style like this for a plugin:
		wp_register_style( 'normalize', plugins_url( '/css/normalize.min.css', __FILE__ ), array());
		wp_enqueue_style( 'normalize' );
		
		// Register the style like this for a plugin:
		wp_register_style( 'custom-style', plugins_url( '/css/ermak_admin.css', __FILE__ ), array());
		wp_enqueue_style( 'custom-style' );
		
		
		//Font Awesome
		// Register the style like this for a plugin:
		wp_register_style( 
							'Awesome-style', 
							plugins_url( "/css/font-awesome.min.css" , __FILE__ ), 
						  array()
						  );
		
		// For either a plugin or a theme, you can then enqueue the style:
		wp_enqueue_style( 'Awesome-style' );
		
		
	}
	
	function user_register($user_id)
	{
		update_user_meta($user_id, "iface_fill", "#00FF00");
	}
	
	// return inerface color
	
	
	function get_iface_color()
	{
		$fill 			= is_user_logged_in() ? get_user_meta( get_current_user_id(), 'iface_fill', true) : $this->options['iface_fill'];
		 return 		$fill ? $fill : $this->options['iface_fill'];		
	}
	function hook_css()
	{
		global $smc_height, $voc, $is_mobile;
		global $user_iface_color;
		$user_iface_color			= $this->get_iface_color();		
		if($this->options['use_panel'])
		{
			echo "<style>
			body
			{
				#display:none;
				opacity:0;
			}
			</style>";
		}
		echo "
			<script>
				if( screen.width <= 480 )
				{
					jQuery('body').hide();
				}
				var is_mobile	= " . (int)$is_mobile . ";
				var ermakurl	= '". SMC_URLPATH. "';
			</script>
			<style>
			.lp-wrap-open 
			{ 
				height:".$smc_height."px; 
			} 
			.lp-scroll-content
			{
				height:".($smc_height-40)."px!important;
			}			
			.lp-external-color
			{
				color:" . $user_iface_color . "!important;
			}
			.lp-external-background, .lp-location-btn-directives
			{
				background:" . $user_iface_color . "!important;
			}
			.trapezium:active, .trapezium:active:before, .trapezium:active:after {
				color:#FFF;
				background:" . $user_iface_color . ";
			}
			._lp-select-button:before {
				border-right:1px solid " . $user_iface_color . ";
				transform: skew(-15deg);
				-webkit-transform: skew(-15deg);
				-moz-transform: skew(-15deg);
				-o-transform: skew(-15deg);
				left: 18px;
				border-bottom: 1px solid " . $user_iface_color . ";
			}
			._lp-select-button:after {
				border-left:1px solid " . $user_iface_color . ";
				transform: skew(15deg);
				-webkit-transform: skew(15deg);
				-moz-transform: skew(15deg);
				-o-transform: skew(15deg);
				right: 18px;
				left: auto;
				border-bottom: 1px solid " . $user_iface_color . ";
			}
			.lp-location-button:active
			{
				background:" . $user_iface_color . ";
			}
			.lp-location-exec-button a
			{
				font-family:'Ubuntu Condensed', Arial, Sans-serif;
				font-size:13px;
				text-transform: uppercase;
				background: " . $user_iface_color . ";
				padding:4px 25px;
				min-width:60px;
				color:black;
				-webkit-border-radius: 20px 20px 20px 20px;
				border-radius: 20px 20px 20px 20px;
				opacity:0.9;
				clear:right;
			}
			.lp-open-close-button:active
			{
				color:" . $user_iface_color . "!important;
				background:#FFF;
			}
			.lp-open-select-button
			{
				background:" . $user_iface_color . "; 
				color:#FFF!important;
			}
			.lp-block-height
			{
				height:".($smc_height - 65)."px!important;
			}
			.lp-personal-message-body a
			{
				color:" . $user_iface_color . "!important;
			}
			.lp-personal-message-body a:hover
			{
				background:" . $user_iface_color . "!important;
				color:#FFF!important;
			}
			.lp-link
			{
				color:" . $user_iface_color . ";
				
			}
			.lp-link:hover
			{
				color:#000;
			}
			.lp-link:active
			{
				color:#555;
			}
			#main
			{
				background:#FFF;
				padding:7px;
				margin:4px;
			}
			
			.smp-pr-title:hover
			{
				color:". $user_iface_color.";
				background:#FFF;
				border-bottom:1px solid #AAA;
				#width:190px;
				#padding:10px 11px 10px 10px;
			}
			.smp-pr-title:active
			{
				background:". $user_iface_color.";
				color:white;
			}
			.lp-head-2 a:hover
			{
				color:". $user_iface_color."!important;
			}
			.lp_bg_color_hover:hover
			{
				background:". $user_iface_color.";
				color:white;
			}
			.lp_bg_color_hover:active
			{
				background:black;
				color:white;
			}
			.lp_zoom_pnl_btn
			{
				background:". $user_iface_color.";
			}
			.lp_bg
			{
				background:". $user_iface_color."!important;
			}
			.lp-border-color
			{
				border-color: ". $user_iface_color."!important;
			}
			.lp-left-border-color
			{
				border-left: 11px solid ". $user_iface_color.";
			}
			.lp_clapan_raze1
			{
				background-color:". $user_iface_color.";
			}
			.smc_button:hover
			{
				background:". $user_iface_color.";
				color:white;
			}
			#zoom_slider_cont .irs-line-left, 
			#zoom_slider_cont .irs-line-mid, 
			#zoom_slider_cont .irs-line-right,
			#zoom_slider_cont .irs-line 
			{
				background:". $user_iface_color.";
			}
			.star, .star-empty
			{
				color:". $user_iface_color.";
			}
			". 
				apply_filters("smc_front_head_css", "").
			"</style>
			<script  type='text/javascript'>".
				apply_filters("smc_front_head_style", "").
			"</script>
			";
			if($this->options['use_ermak_ico'])
				echo "<link rel='shortcut icon' href='".SMC_URLPATH."img/ermak1.ico'>";
	}
	function init_achive_types()
	{
		global $achivment_types;
		$loc								=  new SMC_Achivment;
		$loc->init( array( "par_type"=>  SMC_LOCATION_NAME, 'capability_type'=>'taxonomy') );
		$achivment_types[__('Location', "smc")]		= $loc;
	}
	function svg_upload($params)
	{
		/**/
		if(!function_exists('wp_handle_upload'))
		{
			require_once(ABSPATH . 'wp-admin/includes/file.php');
		}		
		$upload_overrides = array('test_form' => false);
		foreach($_FILES as $file)
		{
			$ID 	= media_handle_sideload($file, $upload_overrides);
			$src 	= wp_get_attachment_url( $ID );
		}
		print json_encode( "<img src='".$src."'>" );
		exit();
		
	}
	
}

$Soling_Metagame_Constructor	= new Soling_Metagame_Constructor();
SMC_Location_Type::init();
SMC_Location::init();

$Assistants						= $Soling_Metagame_Constructor->assistants;

add_filter( 'plugin_row_meta', 'fb_get_plugin_string', 10, 4 );
function fb_get_plugin_string( $plugin_meta, $plugin_file, $plugin_data, $status ) 
{
	return $plugin_meta;
}
function getmicrotime() 
{ 
    list($usec, $sec) = explode(" ", microtime()); 
    return round(((float)$usec + (float)$sec)*1000); 
} 

// FILTERS:
//	smc_login_widget_list_element
//	smc_location_type_meta
//	smc_personal_button
//	smc_add_grandchildren_element_to_map
//	smc_load_panel_screen
//	smc_display_special_panel
// 	smc_map_external_0
//
//	ACTIONS:
//	smc_add_grandchildren_data_to_map
//	smc_myajax_submit
?>
