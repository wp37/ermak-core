<?php
/*
	
	
*/

class SMC_Login_Picto extends WP_Widget 
{
		
/*  Constructor
/* ------------------------------------ */
	function SMC_Login_Picto() 
	{
		parent::__construct( false, __('Ermak Login Widget Picto', 'smc'), array('description' => __('Player Cabinet', 'smc'), 'classname' => 'smc_widget_login_picto') );;	
		//add_action( 'init',				array($this, 'redirect_login_page'));
		add_filter( 'authenticate', 		array($this, 'verify_username_password'), 1, 3);
		add_action( 'wp_login_failed',		array($this, 'login_failed' ));
		add_filter("smc_front_head_style",	array($this, "smc_add_vocabulary"), 2);
		//add_action( 'wp_logout',		array($this, 'logout_page'));
	}
	
	function get_list()
	{
		return  $this->get_default_list();
	}
	function get_default_list()
	{
		global $Soling_Metagame_Constructor;
		$options	= $Soling_Metagame_Constructor->options;
		$aaaa		= $options['prsonal_page_id'];
		$bbbb		= $options['location_page_id'];									
		$arr		= apply_filters("smc_login_widget_list", 
			array(
				"dm"			=> array(
					"title"		=> __("Direct Messages", "smc"),
					"picto"		=> SMC_URLPATH . "icon/talk_ico.png",
					"url"		=> get_permalink($aaaa)
					),
				"locations"		=> array(
					"title"		=> __("My Locations", "smc"),
					"picto"		=> SMC_URLPATH . "icon/location_ico.png",
					"url"		=> get_permalink($bbbb)
					)
				)
		);
		$arr["logout"]		= array(
				"title"		=> __("Log out"),
				"picto"		=> SMC_URLPATH . "icon/logout_ico.png",
				"url"		=> esc_url( wp_logout_url() )
		);
		return $arr;
	}
	function redirect_login_page() 
	{  
		$login_page  	= home_url( '/' );  
		$page_viewed 	= basename($_SERVER['REQUEST_URI']);  
		$this->name 	= __('Ermak Login Widget Picto', 'smc');
		$this->widget_options['description'] 	= __('Player Cabinet', 'smc');
		if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {  
			wp_redirect($login_page);  
			exit;  
		}  
	}  
	function login_failed() {  
		$login_page  = home_url( '/' );  
		wp_redirect( $login_page . '?login=failed' );  
		exit;  
	}  
	function logout_page() {  
		$login_page  = home_url( '/' );  
		wp_redirect( $login_page );  
		exit;  
	}  
	function verify_username_password( $user, $username, $password ) {  
		$login_page  = home_url( '/' );  
		if( $username == "" || $password == "" ) {  
			wp_redirect( $login_page . "?login=empty" );  
			exit;  
		}  
	}  

/*  Widget
/* ------------------------------------ */
	public function widget($args, $instance) 
	{
		global $post;
		global $UAM;
		global $SolingMetagameProduction, $Soling_Metagame_Constructor;
		global $is_smc_login_widget, $add_local_avatars;
		$is_smc_login_widget = true;
		$mess		= "";
		$mess_color	= "#FF0000";
		
		
		if($_POST['smp_register1'])
		{
			$user_name		= $_POST['smp-username-reg'];
			$user_pass		= $_POST['smp-password-reg'];
			$e_mail			= $_POST['smp-email-reg'];
			$user_id 		= username_exists( $user_name );
			$u_email		= get_user_by('email', $e_mail);
			if ( !$user_id && !$u_email) 
			{
				$new_user_id= wp_create_user($user_name, $user_pass, $e_mail);
				wp_update_user( array( 'ID' => $new_user_id, 'role' => 'Player' ) );
				//$_POST['smp_login'] = "1";
			}
			else if($u_email)
			{
				$mess			= '<p class="login-msg">'.__("This user's email is occupied." , 'smc').'</p>';  
			}
			else
			{
				$mess			= '<p class="login-msg">'.__("This user's name is occupied." , 'smc').'</p>';  
			}
			if(is_wp_error($new_user_id))
			{
				$mess		=  $new_user_id->get_error_message();
			}			
			do_action("smc_register_action", $new_user_id);
		}
		
		if($_POST['smp_login'])
		{
			$log			= wp_login($_POST['smp-username-login'], $_POST['smp-password-login']);
		}
		
		$login  = (isset($_GET['login']) ) ? $_GET['login'] : 0;
		if ( $login === "failed" ) {  
			$mess	= __('Invalid username and/or password', 'smc');  
		} elseif ( $login === "empty" ) {  
			$mess	= '';//__('Username and/or Password is empty', 'smc');  
		} elseif ( $login === "false" ) {  
			$mess	= __('You are logged out', 'smc');  
		}
		else
		{
			$mess	= '';//<p class="login-msg"> '.__('Success', 'smc').'</p>';  
			$mess_color = "#555500";
		}
		
		
		if($_POST['chn'])
		{
			$u_data			= array( 
										"ID"			=> get_current_user_id() ,
										//"user_login"	=> $user->user_login
									);
			if($_POST['user_display_name'] != "")
			{
				$u_data['display_name']		= $_POST['user_display_name'];
				if($_POST['user_pass']!='')
				{
					$u_data['user_pass']	= $_POST['user_pass'];	
				}
				$ud				= wp_update_user($u_data);
				if(is_wp_error($ud))
				{
					$mess		.= $ud->get_error_message();
				}
				else
				{
					$mess_color = "#555500";
					$mess		.= "Success!";
				}
			}
			else
			{
				$mess	= __('Invalid username.', 'smc');  
			}
		}
		$user		= wp_get_current_user();
		$vis				= $mess =="" ? "none" : "block";	
		extract( $args );
		$instance['title'] ? NULL : $instance['title'] = '';
		$title = apply_filters('widget_title',$instance['title']);
		$output = $before_widget."\n";
		if($title)
			$output .= $before_title.$title.$after_title;
		ob_start();
		$avatar		= get_avatar( get_current_user_id() , 70);
		$options	= $Soling_Metagame_Constructor->options;
		$aaaa		= $options['prsonal_page_id'];
		$bbbb		= $options['location_page_id'];
		$pname		= $options['project_name'];
		$win_login_title	= __("Login", "smc") . ': <span> '.get_bloginfo("name").'</span>';
		$win_register_title	= __('Register', 'smc') . ': <span> '.get_bloginfo("name").'</span>';
		?>
		<?php 
			//echo "<div style=' background:".$mess_color."; color:white; padding-top:8px; padding-bottom:4px;  text-align:center; margin:5px 0; display:".$vis."'>".$mess."</div>";
			if($mess != ""){
		?>
		<script>
			//alert('<?php echo  $mess ?>');
			add_ermak_message( "<?php echo $mess ?>" );
		</script>
		<?php } ?>
		<div id="lp-widget-avatar" class='lp-widget-avatar'>												
				<?php echo $avatar ?>
			<div id="lp_avatar_dialog">
				<form method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<td style="vertical-align:top; width:140px; height:140px;">
								<?php
								if(is_plugin_active('add-local-avatar/avatars.php')){
								?>
								<div>	
									<?php echo $add_local_avatars->avatar_uploader_table($user, PRESENTATION_SIZE );?>
								</div>
								<?php } ?>
							</td>
							<td>
								<h2><?php _e("User parameters", "smc"); ?></h2>
								<div class="lp-abzaz lp-full_width">
									<label for="user_display_name"><?php _e("Login name", "smc")?></label><BR>
									<input name="user_display_name" value="<?php echo $user->display_name; ?>"/>
								</div>
								<div class="lp-abzaz lp-full_width">
									<label for="user_pass"><?php _e("Insert your new password", "smc")?></label><BR>
									<input name="user_pass" value=""/>
								</div>
								<div class="lp-abzaz lp-full_width">
									<label for="user_pass11"><?php _e("Repeat new password", "smc")?></label><BR>
									<input name="user_pass11" value=""/>
								</div>
								<!--div class="lp-abzaz lp-full_width">
								<label for="color"><?php _e("Color", "smc");?></label><br>
								<input class="color" name="color" id="color" value="<?php echo get_user_meta( get_current_user_id(), 'iface_fill', true) ?>"/>
								</div-->
								<div class="lp-abzaz lp-full_width">
									<input type="submit" name="chn" value="<?php _e("Change")?>"/>
								</div>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
		<table BORDER="0" cellspacing="0" cellpadding="0" class="smc-login-table" style="vertical-align: top!important; text-align:left!important;">
			<tr>
				
				<td valign="top" class="smc-login-table">
					<div style="disply:inline-block; position:relative; width:100%;">		
						<div style="display:<?php echo is_user_logged_in() ? "block" : "none"; ?>; margin-bottom:20px;">
							<?php echo apply_filters("before_smc_logine_name", "",  true) ;?>
							<span class="lp-avatar-name">
								<?php  echo $user->display_name; ?>
							</span>
							<span class="vertical-align:middle;">
								<input style="
									width:10px; 
									height:10px; 
									margin-left: 10px;
									display: inline-block;
									position: relative;
									cursor:pointer;" 
								class="color" name="color" id="user_color" value="<?php echo get_user_meta( get_current_user_id(), 'iface_fill', true) ?>"/>
							</span>
							<?php echo apply_filters("after_smc_logine_name", "",  true) ;?>
						</div>		
						<div style="display:<?php echo is_user_logged_in() ? "none" : "block"; ?>; margin-bottom:20px;">
							<?php echo apply_filters("before_smc_logine_name", "",  false) ;?>
							<span class="lp-avatar-name">
								<?php  _e("Hello", "smc"); ?>
							</span>
							<?php echo apply_filters("after_smc_logine_name", "",  false) ;?>
						</div>
					</div>		
					
					<?php 
					$visible_button = is_array($instance["visible_button"]) ? $instance["visible_button"] : array();
					if($instance['is_illustrated'])
					{
					?>
						<div style="display:<?php echo is_user_logged_in() ? "block" : "none"; ?>">
							<div class="ermak_login_picto">						
								<?php								
								foreach($this->get_list() as $key=>$lst)
								{
									if(!$visible_button[$key] && isset($visible_button[$key]) || !apply_filters("smc_as_user_can", true, LOGIN_WIDGET_REASON, array("key"=>$key, "value"=>$lst ))) continue;
									?>
									<div class="hint hint--top  page_icon" data-hint="<?php echo $lst['title']; ?>" href="<?php echo $lst['url'] ?>">
										<img src="<?php echo $lst['picto']; ?>">								
									</div>
									<?php
								}
								?>
							</div>
						</div>
						<div style="display:<?php echo is_user_logged_in() ? "none" : "block"; ?>">
								
							<span><i class="fa fa-angle-right"></i></span> <span id="register_btn" class="smc-alert lp-link lp-widget-login" target_name="login_form" title='<?php echo $win_login_title; ?>'> <?php _e('Login', 'smc'); ?></span>
							</br>
						<?php if($instance['is_register']) { ?>
							<span><i class="fa fa-angle-right"></i></span> <span id="register_btm" class="smc-alert lp-link lp-widget-login" target_name="register-form" params="min-height:200px;" title='<?php echo $win_register_title; ?>'> <?php _e('Register', 'smc'); ?></span>
						<?php } ?>
						</div>
						<?php 
					}
					else
					{
					?>
						<div class="lp-widget-login">
							<div style="display:<?php echo is_user_logged_in() ? "block" : "none"; ?>">								
								<?php
								foreach($this->get_list() as $key=>$lst)
								{
									if(!$visible_button[$key] && isset($visible_button[$key]) || !apply_filters("smc_as_user_can", true, LOGIN_WIDGET_REASON, array("key"=>$key, "value"=>$lst ))) continue;
									?>
									<span><i class="fa fa-angle-right"></i></span> 
									<a href="<?php echo $lst['url']; ?>"> <?php echo $lst['title']; ?></a><br>
									<?php
								}
								?>
							</div>
							<div style="display:<?php echo is_user_logged_in() ? "none" : "block"; ?>">
								
								<span><i class="fa fa-angle-right"></i></span> <span id="register_btn" class="smc-alert lp-link lp-widget-login" target_name="login_form" title='<?php echo $win_login_title; ?>'> <?php _e('Login', 'smc'); ?></span>
								</br>
							<?php if($instance['is_register']) { ?>
								<span><i class="fa fa-angle-right"></i></span> <span id="register_btm" class="smc-alert lp-link lp-widget-login" target_name="register-form" params="min-height:200px;" title='<?php echo $win_register_title; ?>'> <?php _e('Register', 'smc'); ?></span>
							<?php } ?>
							</div>
						</div>	
					<?php } ?>					
				</td>
			</tr>
		</table>
		<script>
			(function($)
			{
				$(".page_icon").each(function( n, obj)
				{
					//console.log(obj);
					if($(obj).attr("href") == window.location.href)
					{
						//$(obj).css({"background":"#121212"});
						$(obj).addClass("login_picto_select");
					}
					else
					{
						$(obj).removeClass("login_picto_select");
					};
				})
			})(jQuery);
		</script>
		
						<?php echo apply_filters("smc_login_form",
						'<div  id="login_form" style="display:none"> <!-- Login -->
							<div id="login-form">
								'.
								wp_login_form( array(
											'echo' => false,
											'redirect' => site_url( $_SERVER['REQUEST_URI'] ), 
											'form_id' => 'loginform',
											'label_username' => __( 'Username' ),
											'label_password' => __( 'Password' ),
											'label_remember' => __( 'Remember Me' ),
											'label_log_in' => __( 'Log In' ),
											'id_username' => 'user_login',
											'id_password' => 'user_pass',
											'id_remember' => 'rememberme',
											'id_submit' => 'wp-submit',
											'remember' => true,
											'value_username' => NULL,
											'value_remember' => false 
									) ).
							'</div>
						</div><!-- /Login -->');
						echo $instance['is_register'] ?	apply_filters("smc_register_form", Assistants::get_register_form() ) : ""; ?>
		

		<?php
		$output .= ob_get_clean();
		$output .= $after_widget."\n";
		echo $output;
	}
	
		
/*  Widget update
/* ------------------------------------ */
	public function update($new,$old) 
	{
		$instance = $old;
		$instance['title'] 				= strip_tags($new['title']);	
		$instance['is_illustrated'] 	= $new['is_illustrated']	== 'on' ? 1 : 0;	
		$instance['is_register'] 		= $new['is_register']		== 'on' ? 1 : 0;	
		$instance['visible_button'] 	= array();
		foreach($this->get_list() as $key=>$lst)
		{
			$instance['visible_button'][$key]  = $new[$key] == "on" ? 1 : 0;
		}
		insertLog("update_widget", $instance['visible_button']);
		return $instance;
	}

/*  Widget form
/* ------------------------------------ */
	public function form($instance) 
	{
		// Default widget settings
		$defaults = array(
			'title' 			=> '',		
			'is_illustrated' 	=> '',		
			'is_register' 		=> '',		
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
	?>		
		<div class="alx-options-posts">
			<p>
				<board_title><?php _e('Title'); ?></board_title>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance["title"]); ?>" />
			</p>
			<hr>
			<board_title><?php _e("Visible of buttons", "smc"); ?></board_title>
			<p>
				<?php
				$visible_button = is_array($instance["visible_button"]) ? $instance["visible_button"] : array();
				foreach($this->get_list() as $key=>$lst)
				{
					$check		= $visible_button[$key] || !isset($visible_button[$key]);
					?>
					<p>
						<input type="checkbox" class="css-checkbox" id="<?php echo $this->get_field_id($key); ?>" name="<?php echo $this->get_field_name($key); ?>" <?php checked(1, $check, 1); ?> />
						<label for="<?php echo $this->get_field_id($key); ?>" class="css-label"><?php echo $lst['title'] ?></label>
					</p>
					<?php
				}				
				?>
			</p>
			<hr>
			<board_title><?php _e("Settings"); ?></board_title>
			<p>
				<input class="css-checkbox" type="checkbox"  id="<?php echo $this->get_field_id('is_illustrated'); ?>" name="<?php echo $this->get_field_name('is_illustrated'); ?>" <?php checked( $instance["is_illustrated"], 1); ?> />
				<label class="css-label" for="<?php echo $this->get_field_id('is_illustrated'); ?>"><?php _e("Is illustrated", "smc");?></label>
			</p>		
			<p>
				<input class="css-checkbox" type="checkbox"  id="<?php echo $this->get_field_id('is_register'); ?>" name="<?php echo $this->get_field_name('is_register'); ?>" <?php checked( $instance["is_register"], 1); ?> />
				<label class="css-label" for="<?php echo $this->get_field_id('is_register'); ?>"><?php _e("Add Register form", "smc");?></label>
			</p>		
			<hr>		 
		</div>
	<?php
	}
	
	function smc_add_vocabulary($text)
	{
		return $text.
		"	vocabulary['field_username_empty']='". __("field username not may be empty!", "smc") . "';
			vocabulary['field_pass_empty']='". __("field password not may be empty!", "smc") . "';
			vocabulary['field_email_empty']='". __("field e-mail not may be empty!", "smc") . "';
		";			
	}

}


/*  Register widget
/* ------------------------------------ */
	function smc_register_widget_picto() { 
		register_widget( 'SMC_Login_Picto' );
	}
	add_action( 'widgets_init', 'smc_register_widget_picto' );
	
	
?>