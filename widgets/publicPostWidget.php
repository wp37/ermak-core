<?php
	/*
	Plugin Name: Public Post Widget
	
	License: GNU General Public License v2.0
	License URI: http://www.gnu.org/licenses/gpl-2.0.html
	
	Copyright: (c) 2014 genagl@gmail.com
	
		@package genagl
		@version 1.0
	*/

	class PublicPostWidget extends WP_Widget 
	{

		/*  Constructor
		/* ------------------------------------ */
		function PublicPostWidget() 
		{
			parent::__construct( 'public_post_widget', __('Metagame Public Post Widget', "smc"), array('description' => 'public post form', 'classname' => 'public_post_widget') );;	
		}
		
		/*  Widget
		/* ------------------------------------ */
		public function widget($args, $instance) 
		{
			global $post;
			extract( $args );
			$instance['title'] ? NULL : $instance['title'] = '';
			$title = apply_filters('widget_title',$instance['title']);
			$output = $before_widget."\n";
			if($title)
				$output .= $before_title.$title.$after_title;
			else
				$output .= $before_title. $instance['title'].$after_title;
			ob_start();	
			if(is_category() || is_tax())
			{
				if (! is_user_logged_in() ) 
				{ 
					echo "Авторизуйтесь, пожалуйста"; 
					$output .= ob_get_clean();
					$output .= $after_widget."\n";
					echo $output;
					return;
				}
			}
			else
			{
				return;
			}
			?>
				<style type="text/css">
					.botton_
					{
						display: block;
						background: #fcfcfc; /* Old browsers */
						background: -moz-linear-gradient(top,  #fcfcfc 0%, #e2e2e2 100%); /* FF3.6+ */
						background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fcfcfc), color-stop(100%,#e2e2e2)); /* Chrome,Safari4+ */
						background: -webkit-linear-gradient(top,  #fcfcfc 0%,#e2e2e2 100%); /* Chrome10+,Safari5.1+ */
						background: -o-linear-gradient(top,  #fcfcfc 0%,#e2e2e2 100%); /* Opera 11.10+ */
						background: -ms-linear-gradient(top,  #fcfcfc 0%,#e2e2e2 100%); /* IE10+ */
						background: linear-gradient(to bottom,  #fcfcfc 0%,#e2e2e2 100%); /* W3C */
						filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfcfc', endColorstr='#e2e2e2',GradientType=0 ); /* IE6-9 */
						-webkit-border-radius: 2px 2px 2px 2px;
						border-radius: 2px 2px 2px 2px;
						border: 1px solid #CCC;
						padding: 20px 45px;
						color: #7B7B7B;
						font-size: 0.875em;
						font-family: inherit;
						text-decoration: none;
						text-shadow: 0 1px 0 #FFFFFF;
						text-align: center;
						box-shadow: inset 0 1px 0 #FFFFFF,0 1px 2px rgba(0,0,0,0.1);
					}
					.botton_:hover
					{
						background: #fcfcfc; /* Old browsers */
						background: -moz-linear-gradient(top,  #fcfcfc 0%, #ececec 100%); /* FF3.6+ */
						background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fcfcfc), color-stop(100%,#ececec)); /* Chrome,Safari4+ */
						background: -webkit-linear-gradient(top,  #fcfcfc 0%,#ececec 100%); /* Chrome10+,Safari5.1+ */
						background: -o-linear-gradient(top,  #fcfcfc 0%,#ececec 100%); /* Opera 11.10+ */
						background: -ms-linear-gradient(top,  #fcfcfc 0%,#ececec 100%); /* IE10+ */
						background: linear-gradient(to bottom,  #fcfcfc 0%,#ececec 100%); /* W3C */
						filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfcfc', endColorstr='#ececec',GradientType=0 ); /* IE6-9 */

					}
					.botton_:active {	
						background: #fcfcfc; /* Old browsers */
						background: -moz-linear-gradient(bottom,  #fcfcfc 0%, #ececec 100%); /* FF3.6+ */
						background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fcfcfc), color-stop(100%,#ececec)); /* Chrome,Safari4+ */
						background: -webkit-linear-gradient(bottom,  #fcfcfc 0%,#ececec 100%); /* Chrome10+,Safari5.1+ */
						background: -o-linear-gradient(bottom,  #fcfcfc 0%,#ececec 100%); /* Opera 11.10+ */
						background: -ms-linear-gradient(bottom,  #fcfcfc 0%,#ececec 100%); /* IE10+ */
						background: linear-gradient(to top,  #fcfcfc 0%,#ececec 100%); /* W3C */
						filter: progid:DXImageTransform.Microsoft.gradient(endColorstr='#ececec', startColorstr='#fcfcfc', GradientType=0 ); /* IE6-9 */
						.box-shadow(none);
						
					}
					.lp_new_post_metaz
					{
						display:none;
					}
					.lp_comment_z
					{
						padding:10px 0;
						font-size:11px;
						opacity:0.75;
					}
					.lp-input-z
					{
						width:100%;
					}
				</style>
				<form name="new_post1" method="post"  enctype="multipart/form-data" id="new_post1">
					<div style="margin:0; display:inline-block; height:100%; width:100%;">											
						<div class="" style="">
							<div>
								<input type="text" name="new_post_title1" id="new_post_title1" placeholder="<?php _e("Title");?>" class="lp-black-input" />
							</div>
							<div>
								<textarea rows="4" id="new_post_text1" name="new_post_text1" maxlengt="140"  placeholder="<?php _e("Text")?>" class="lp-black-input" style="height:135px;"></textarea>
							</div>
						</div>
						<div class="" style="">							
							<div>
								<div style="display:inline-block; clear:left; width:100%; background:transparent;">
									<div class="lp-button-sector">
										 <a href="javascript:void(0);" onclick="newPostShowHide1(1);" class="lp-open-close-button  hint hint--top" data-hint="<?php  _e("slideshow", "smc")?>"><i class="fa fa-folder fa-2x"></i></a><br/>
									</div>	
									<div class="lp-button-sector">
										 <a href="javascript:void(0);" onclick="newPostShowHide1(2);" class="lp-open-close-button  hint hint--top" data-hint="<?php  _e("video", "smc");?>"><i class="fa fa-youtube-square fa-2x"></i></a><br/>
									</div>	
									<div class="lp-button-sector">
										 <a href="javascript:void(0);" onclick="newPostShowHide1(3);" class="lp-open-close-button  hint hint--top" data-hint="<?php  _e("audio", "smc")?>"><i class="fa fa-music fa-2x"></i> </a><br/>
									</div>	

								</div>															
								<div class="lp_new_post_metaz" id="nnn1">
									<div class="lp_comment_z"><?php _e("add urls of pictures", "smc") ?></div>
									<input type="text" name="slide11" placeholder="<?php  echo sprintf(__('put url for %s slide', 'smc'), 1) ?>" class="lp-input-z"/>
									
									<input type="text" name="slide21" placeholder="<?php echo sprintf(__('put url for %s slide', 'smc'), 2) ?>" class="lp-input-z"/>
									
									<input type="text" name="slide31" placeholder="<?php echo  sprintf(__('put url for %s slide', 'smc'), 3) ?>" class="lp-input-z"/>
									
									<input type="text" name="slide41" placeholder="<?php echo sprintf(__('put url for %s slide', 'smc'), 4) ?>" class="lp-input-z"/>
									
									<input type="file" multiple name="upload_files[]" id="upload_files1" class="lp-input-z"/>
								</div>															
								<div class="lp_new_post_metaz" id="nnn2">
									<div class="lp_comment_z"><?php _e('put youtube url', 'smc')?></div>
									<input type="text" name="youtube1" placeholder="<?php  _e('put youtube url', 'smc') ?>" class="lp-input-z"/>
									
								</div>															
								<div class="lp_new_post_metaz" id="nnn3">
									<div class="lp_comment_z"><?php  _e('add audio', 'smc') ?></div>
									<input type="file" name="audio_file1" class="lp-input-z"/>
									
								</div>														
								<div class="lp_new_post_metaz" id="nnn4">
									<div class="lp_comment_z">ATTACHNMENT</div>
								</div>
							</div>
							
							<center style="padding-top:20px;">
								
									<a href="javascript:void(0);" onclick="create_new_post();" class="botton" style="border:none; padding:15px;">
										<?php _e("Create Post", "smc")?>
									</a>
							</center>
						</div>											
					</div>
				</form>
			<?php
			
			$output .= ob_get_clean();
			$output .= $after_widget."\n";
			echo $output;
		}
		/*  Widget update
		/* ------------------------------------ */
		public function update($new,$old) {
			$instance = $old;
			$instance['title'] = strip_tags($new['title']);
			return $instance;
		}
		
		/*  Widget form
		/* ------------------------------------ */
		public function form($instance) {
			// Default widget settings
			$defaults = array(
				'title' 			=> __("Add New Post","smc"),			
			);
			$instance = wp_parse_args( (array) $instance, $defaults );
			?>

			<style>
			.widget .widget-inside .alx-options-posts .postform { width: 100%; }
			.widget .widget-inside .alx-options-posts p { margin-bottom: 0.3em; }
			.widget .widget-inside .alx-options-posts hr { border: none; border-bottom: 1px dotted #ddd; margin: 1em 0; }
			.widget .widget-inside .alx-options-posts h4 { margin-bottom:0.665em; }
			</style>
			
			<div class="alx-options-posts">
				<p>
					<label for="<?php echo $instance['title']; ?>">Заголовок:</label>
					<input class="widefat" id="<?php $instance['title']; ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance["title"]); ?>" />
				</p>		
				<hr>
			</div>
			<?php
		}
		
		function add_new_post(
					$post_title,				//string
					$post_content,				//string
					$taxonomies_id	= array() 	//array
				 )
		{
			$post = array(
			  'comment_status' 	=> 'open',
			  'ping_status'    	=> 'closed',
			  'post_content'   	=> $post_content,
			  'post_name'      	=> 'Post_Name',
			  'post_status'    	=> 'publish',
			  'post_title'    	=> $post_title,
			  'post_type'      	=> 'post',
			  'tags_input' 		=> $tags_id,
			  'tax_input'      	=> $taxonomies_id
			);
			$post_id = wp_insert_post( $post, $wp_error);			
			return $post_id;
		}
	}
	/*  Register widget
	/* ------------------------------------ */
	function register_public_post_widget() { 
		register_widget( 'PublicPostWidget' );
	}
	add_action( 'widgets_init', 'register_public_post_widget' );
?>